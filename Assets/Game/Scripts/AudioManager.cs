﻿using System;
using UnityEngine;

public class AudioManager : MonoBehaviour
{
    public bool PlaySounds = true;
    public bool PlayMusic = true;
    public Sound Music_Warhound;
    public Sound Music_Boss;
    public Sound Music_Gameover;
    public Sound Music_End;
    public Sound Blip;
    public Sound Bullet;
    public Sound Bullet2;
    public Sound EnemyDeath;
    public Sound EnemyDeath2;
    public Sound EnemyDeath3;
    public Sound Grenade;
    public Sound Grenade2;
    public Sound Grenade3;
    public Sound Powerupcollect;
    public Sound Powerupcollect2;
    public Sound Powerupcollect3;
    public Sound Powerupcollect4;
    private static AudioManager instance;

    public enum WarhoundSounds
    {
        music_warhound,
        music_boss,
        music_gameover,
        music_end,
        blip,
        bullet,
        bullet2,
        enemyDeath,
        enemyDeath2,
        enemyDeath3,
        grenade,
        grenade2,
        grenade3,
        powerupcollect,
        powerupcollect2,
        powerupcollect3,
        powerupcollect4,
    }

    private bool IsMusic(WarhoundSounds s)
    {
        return s == WarhoundSounds.music_boss || s == WarhoundSounds.music_end
            || s == WarhoundSounds.music_gameover || s == WarhoundSounds.music_warhound;
    }

    public void Play(WarhoundSounds warhoundSound)
    {
        var sound = GetWarhoundSound(warhoundSound);
        if (sound != null)
        {
            if ((IsMusic(warhoundSound) && PlayMusic) || (!IsMusic(warhoundSound) && PlaySounds))
            {
                sound.Source.Play();
            }
        }
    }

    public void Stop(WarhoundSounds warhoundSound)
    {
        var sound = GetWarhoundSound(warhoundSound);
        if (sound != null)
        {
            sound.Source.Stop();
        }
    }

    private Sound GetWarhoundSound(WarhoundSounds warhoundSound)
    {
        switch (warhoundSound)
        {
            case WarhoundSounds.music_warhound:
                return Music_Warhound;
            case WarhoundSounds.music_boss:
                return Music_Boss;
            case WarhoundSounds.music_gameover:
                return Music_Gameover;
            case WarhoundSounds.music_end:
                return Music_End;            
            case WarhoundSounds.blip:
                return Blip;
            case WarhoundSounds.bullet:
                return Bullet;
            case WarhoundSounds.bullet2:
                return Bullet2;
            case WarhoundSounds.enemyDeath:
                return EnemyDeath;
            case WarhoundSounds.enemyDeath2:
                return EnemyDeath2;
            case WarhoundSounds.enemyDeath3:
                return EnemyDeath3;
            case WarhoundSounds.grenade:
                return Grenade;
            case WarhoundSounds.grenade2:
                return Grenade2;
            case WarhoundSounds.grenade3:
                return Grenade3;
            case WarhoundSounds.powerupcollect:
                return Powerupcollect;
            case WarhoundSounds.powerupcollect2:
                return Powerupcollect2;
            case WarhoundSounds.powerupcollect3:
                return Powerupcollect3;
            case WarhoundSounds.powerupcollect4:
                return Powerupcollect4;
            default:
                return null;
        }
    }

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else
        {
            Destroy(gameObject);
            return;
        }

        var values = Enum.GetValues(typeof(WarhoundSounds));

        foreach (WarhoundSounds enumValue in values)
        {
            Sound s = GetWarhoundSound(enumValue);
            s.Name = s.Clip.name;
            s.Source = gameObject.AddComponent<AudioSource>();
            s.Source.clip = s.Clip;
            s.Source.volume = s.Volume;
            s.Source.pitch = s.Pitch;
            s.Source.loop = s.Loop;
        }
    }
}