﻿using Assets.Game.Scripts.Map;
using Assets.Game.Scripts.PlayerScripts;
using UnityEngine;

namespace Assets.Game.Scripts.EnemyScripts
{
    public class Brain : Enemy
    {                
        public GameObject BrainDialogPrefab;
        private const float DialogDuration = 4f;
        private const float ProjectileSpeed = GridManager.TileRadius * 1.6f; // Constant movement speed of bullets
        private GameObject brainDialogInstance;        
        private float dialogWaitPeriodStartTime = 1f;
        private float radius;
        private float rotateSpeed;
        private Vector2 center;
        private float angle;
        private BrainState privateBrainState;
        private int explodeThreshold = 10;
        private float timeOfLastStateChange;
        private float animationDuration;
        private float timeOfLastShot;        
        private bool lastShotWasCircle;
        private bool destinationReached;
        private bool spawnedOnce;
        private float timeOfLastSnipe;
        private int movementIndex;

        private enum BrainState
        {
            Moving, Shooting, Sniping, Spawning, Swelling, Bursting, Pincer
        }

        private BrainState State
        {
            get
            {
                return privateBrainState;
            }

            set
            {
                privateBrainState = value;
                timeOfLastStateChange = Time.time;
            }
        }

        public override void Destroy()
        {
            audioManager.Play(AudioManager.WarhoundSounds.grenade3);

            CancelInvoke();
            Hp = 0;
            spriteRenderer.material = defaultMaterial;
            ObjectPooler.Instance.GetPooledObjectAndMoveTo(ObjectPooler.ObjectType.Explosion, gameObject.transform.position);
            ObjectPooler.Instance.GetPooledObjectAndMoveTo(ObjectPooler.ObjectType.Explosion, gameObject.transform.position + new Vector3(-GridManager.TileDiameter, -GridManager.TileDiameter, 0));
            ObjectPooler.Instance.GetPooledObjectAndMoveTo(ObjectPooler.ObjectType.Explosion, gameObject.transform.position + new Vector3(GridManager.TileDiameter, GridManager.TileDiameter, 0));
            ObjectPooler.Instance.GetPooledObjectAndMoveTo(ObjectPooler.ObjectType.Explosion, gameObject.transform.position + new Vector3(-GridManager.TileDiameter, GridManager.TileDiameter, 0));
            ObjectPooler.Instance.GetPooledObjectAndMoveTo(ObjectPooler.ObjectType.Explosion, gameObject.transform.position + new Vector3(GridManager.TileDiameter, -GridManager.TileDiameter, 0));
            gameObject.SetActive(false);
        }

        protected new void OnTriggerEnter2D(Collider2D collider)
        {
            if (Time.time > dialogWaitPeriodStartTime + DialogDuration)
            {
                Bullet projectile = collider.gameObject.GetComponent<Bullet>();
                if (projectile != null && !collider.gameObject.GetComponent<EnemyBullet>())
                {
                    if (projectile.Hp > 0)
                    {
                        int tempProjectileHp = projectile.Hp;
                        projectile.Hp -= Hp;
                        Hp -= tempProjectileHp;
                        gameManager.BossHealth = Hp;
                        if (projectile.Hp <= 0)
                        {
                            projectile.Hp = 0;
                            projectile.DestroyBullet();
                        }
                    }

                    StartCoroutine("FlashSprite");
                    if (Hp <= 0)
                    {
                        Destroy();
                    }
                }
            }
        }

        protected new void OnEnable()
        {
            dialogWaitPeriodStartTime = Time.time;
            brainDialogInstance = GameObject.Instantiate(BrainDialogPrefab, gameObject.transform.position + new Vector3(GridManager.TileRadius * 2, GridManager.TileRadius * 7, 0f), Quaternion.identity);
            GameObject.Destroy(brainDialogInstance, DialogDuration);
            maxHp = 55;
            Hp = maxHp;
            gameManager.BossHealth = Hp;
            gameManager.BossHealthMax = Hp;
            gameManager.IsBossActive = true;
            base.OnEnable();
        }

        private void Start()
        {
            State = BrainState.Moving;
            animator.SetBool("IsShooting", false);            
            moveSpeed = GridManager.TileRadius * 7.7f;
            radius = GridManager.TileRadius * 2;
            rotateSpeed = 2f;
        }

        private void FlyInCircles()
        {
            angle += rotateSpeed * Time.deltaTime;
            var offset = new Vector2(Mathf.Sin(angle), Mathf.Cos(angle)) * radius;
            if (Vector2.Distance(transform.position, center + offset) > 0.05f)
            {
                transform.position = Vector2.MoveTowards(transform.position, center + offset, moveSpeed / 2 * Time.deltaTime);
            }
            else
            {
                transform.position = center + offset;
            }
        }

        private void ShootCircle()
        {
            if (Time.time > timeOfLastShot + 0.7f)
            {
                lastShotWasCircle = true;
                timeOfLastShot = Time.time;
                ShootBullet(new Vector2(0f, 0.0f), new Vector3(5f, 5f, 0f) * ProjectileSpeed);
                ShootBullet(new Vector2(0f, 0.0f), new Vector3(0f, 10f, 0f) * ProjectileSpeed);
                ShootBullet(new Vector2(0f, 0.0f), new Vector3(5f, -5f, 0f) * ProjectileSpeed);
                ShootBullet(new Vector2(0f, 0.0f), new Vector3(0f, -10f, 0f) * ProjectileSpeed);
                ShootBullet(new Vector2(0f, 0.0f), new Vector3(-5f, -5f, 0f) * ProjectileSpeed);
                ShootBullet(new Vector2(0f, 0.0f), new Vector3(-10f, 0f, 0f) * ProjectileSpeed);
                ShootBullet(new Vector2(0f, 0.0f), new Vector3(-5f, 5f, 0f) * ProjectileSpeed);
                ShootBullet(new Vector2(0f, 0.0f), new Vector3(10f, 0f, 0f) * ProjectileSpeed);
            }
        }

        private void ShootOffCircle()
        {
            if (Time.time > timeOfLastShot + 0.7f)
            {
                lastShotWasCircle = false;
                timeOfLastShot = Time.time;
                ShootBullet(new Vector2(0f, 0.0f), new Vector3(7.5f, 2.5f, 0f) * ProjectileSpeed);
                ShootBullet(new Vector2(0f, 0.0f), new Vector3(2.5f, 7.5f, 0f) * ProjectileSpeed);
                ShootBullet(new Vector2(0f, 0.0f), new Vector3(7.5f, -2.5f, 0f) * ProjectileSpeed);
                ShootBullet(new Vector2(0f, 0.0f), new Vector3(2.5f, -7.5f, 0f) * ProjectileSpeed);
                ShootBullet(new Vector2(0f, 0.0f), new Vector3(-7.5f, -2.5f, 0f) * ProjectileSpeed);
                ShootBullet(new Vector2(0f, 0.0f), new Vector3(-7.5f, 2.5f, 0f) * ProjectileSpeed);
                ShootBullet(new Vector2(0f, 0.0f), new Vector3(-2.5f, 7.5f, 0f) * ProjectileSpeed);
                ShootBullet(new Vector2(0f, 0.0f), new Vector3(-2.5f, -7.5f, 0f) * ProjectileSpeed);
            }
        }

        private void UpdateMovement()
        {
            if (!destinationReached)
            {
                Vector2 targetWorldCoord = Vector2.zero;

                switch (movementIndex)
                {
                    case 0:
                        targetWorldCoord = gridManager.GetWorldCoordinatesFromNodeCoordinates(new Vector2(8, 8));
                        break;
                    case 1:
                        targetWorldCoord = gridManager.GetWorldCoordinatesFromNodeCoordinates(new Vector2(13, 8));
                        break;
                    case 2:
                        targetWorldCoord = gridManager.GetWorldCoordinatesFromNodeCoordinates(new Vector2(8, 13));
                        break;
                    case 3:
                        targetWorldCoord = gridManager.GetWorldCoordinatesFromNodeCoordinates(new Vector2(1, 8));
                        break;
                    case 4:
                        targetWorldCoord = gridManager.GetWorldCoordinatesFromNodeCoordinates(new Vector2(8, 1));
                        break;
                }

                if (Vector2.Distance(transform.position, targetWorldCoord) > 0.05f)
                {
                    transform.position = Vector2.MoveTowards(transform.position, targetWorldCoord, moveSpeed * Time.deltaTime);
                }
                else
                {
                    destinationReached = true;
                }
            }
        }

        private void FixedUpdate()
        {
            if (Time.time > dialogWaitPeriodStartTime + DialogDuration)
            {
                switch (State)
                {
                    case BrainState.Moving:
                        UpdateMovement();
                        if (Time.time > timeOfLastStateChange + 1.5f)
                        {
                            State = BrainState.Shooting;
                            animator.SetBool("IsShooting", true);
                            center = transform.position;
                        }

                        break;
                    case BrainState.Shooting:
                        FlyInCircles();
                        if (!lastShotWasCircle)
                        {
                            ShootCircle();
                        }
                        else
                        {
                            ShootOffCircle();
                        }

                        if (Hp <= explodeThreshold)
                        {
                            State = BrainState.Swelling;
                            animator.SetBool("IsShooting", false);
                            animator.SetBool("IsSwelling", true);
                        }
                        else if (Time.time > timeOfLastStateChange + 4f)
                        {
                            destinationReached = false;
                            animator.SetBool("IsShooting", false);
                            switch (Random.Range(0, 3))
                            {
                                case 0:
                                    // Randomized move
                                    movementIndex = Random.Range(0, 4);
                                    State = BrainState.Moving;
                                    break;
                                case 1:
                                    // Move to spawn position
                                    movementIndex = 0;
                                    State = BrainState.Spawning;
                                    break;
                                case 2:
                                    // Snipe
                                    State = BrainState.Sniping;
                                    break;
                            }
                        }

                        break;
                    case BrainState.Sniping:
                        FlyInCircles();
                        if (Time.time < timeOfLastStateChange + 5f)
                        {
                            if (Time.time > timeOfLastSnipe + 0.3f)
                            {
                                ShootBullet(transform.position, ((player.transform.position - transform.position).normalized * Random.Range(7, 8) * ProjectileSpeed) + new Vector3(Random.Range(-1f, 1f), Random.Range(-1f, 1f)));
                                timeOfLastSnipe = Time.time;
                            }
                        }
                        else if (Time.time > timeOfLastStateChange + 5f)
                        {
                            StopCoroutine("Snipe");
                            State = BrainState.Moving;
                        }

                        break;
                    case BrainState.Spawning:
                        if (destinationReached && !gameManager.GetPlayerComponent().FreezeMotion)
                        {
                            if (Time.time < timeOfLastStateChange + 4 && !spawnedOnce)
                            {
                                // Spawn
                                animator.SetBool("IsShooting", true);
                                SpawnRandomEnemy();
                                spawnedOnce = true;
                            }
                            else if (Time.time > timeOfLastStateChange + 3 && spawnedOnce)
                            {
                                // Go back to moving
                                animator.SetBool("IsShooting", false);
                                State = BrainState.Moving;
                                spawnedOnce = false;
                            }
                        }
                        else
                        {
                            // Move toward center of map
                            UpdateMovement();
                        }

                        break;
                    case BrainState.Swelling:
                        animationDuration = animator.GetCurrentAnimatorStateInfo(0).length;
                        if (Time.time > timeOfLastStateChange + animationDuration)
                        {
                            animator.SetBool("IsBursting", true);
                            State = BrainState.Bursting;
                        }

                        break;
                    case BrainState.Bursting:
                        animationDuration = animator.GetCurrentAnimatorStateInfo(0).length;
                        if (Time.time > timeOfLastStateChange + animationDuration)
                        {
                            animator.SetBool("IsBurst", true);
                            State = BrainState.Pincer;
                            moveSpeed = 85f;
                        }

                        break;
                    case BrainState.Pincer:
                        UpdateMoveTowardsPlayer();
                        break;
                }
            }
        }

        private void UpdateMoveTowardsPlayer()
        {
            transform.position = Vector2.MoveTowards(transform.position, player.transform.position, moveSpeed * Time.deltaTime);
        }

        private void SpawnRandomEnemy()
        {
            ObjectPooler.ObjectType typeToSpawn = ObjectPooler.ObjectType.Robot;
            switch (Random.Range(0, 3))
            {
                case 0:
                    typeToSpawn = ObjectPooler.ObjectType.Bat;
                    break;
                case 1:
                    typeToSpawn = ObjectPooler.ObjectType.Eyeball;
                    break;
            }

            ObjectPooler.Instance.GetPooledObjectAndMoveTo(typeToSpawn, gridManager.GetWorldCoordinatesFromNodeCoordinates(new Vector2(6, 7)));
            ObjectPooler.Instance.GetPooledObjectAndMoveTo(typeToSpawn, gridManager.GetWorldCoordinatesFromNodeCoordinates(new Vector2(8, 7)));
            ObjectPooler.Instance.GetPooledObjectAndMoveTo(typeToSpawn, gridManager.GetWorldCoordinatesFromNodeCoordinates(new Vector2(8, 9)));
            ObjectPooler.Instance.GetPooledObjectAndMoveTo(typeToSpawn, gridManager.GetWorldCoordinatesFromNodeCoordinates(new Vector2(6, 9)));
        }
    }
}