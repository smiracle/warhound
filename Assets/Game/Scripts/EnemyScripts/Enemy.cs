﻿using System.Collections;
using Assets.Game.Scripts.Map;
using Assets.Game.Scripts.PlayerScripts;
using UnityEngine;

namespace Assets.Game.Scripts.EnemyScripts
{
    public class Enemy : MonoBehaviour
    {
        public static int EnemyCount = 0;
        [ShowOnly]
        public bool EnrouteToWaypoint;        
        [HideInInspector]
        public Vector2 MoveTo;
        [ShowOnly]
        public int Hp;
        [ShowOnly]
        protected int maxHp;
        protected Transform playerTransform;
        [ShowOnly]
        protected float moveSpeed = GridManager.TileRadius * 3.8f;
        protected GridManager gridManager;
        protected GameManager gameManager;
        protected Animator animator;
        protected Player player;
        protected Vector2[] path;
        protected Vector2 currentWaypoint;        
        protected int patrolIndex = 0;
        protected SpriteRenderer spriteRenderer;
        protected Material defaultMaterial;
        protected Material whiteMaterial;
        protected bool didRecalculatePath;        
        protected AudioManager audioManager;
        private int targetIndex;

        public virtual void Destroy()
        {
            switch (Random.Range(0, 3))
            {
                case 0:
                    audioManager.Play(AudioManager.WarhoundSounds.enemyDeath);
                    break;
                case 1:
                    audioManager.Play(AudioManager.WarhoundSounds.enemyDeath2);
                    break;
                case 2:
                    audioManager.Play(AudioManager.WarhoundSounds.enemyDeath3);
                    break;
            }

            CancelInvoke();
            Hp = 0;
            ObjectPooler.Instance.GetPooledObjectAndMoveTo(ObjectPooler.ObjectType.Explosion, gameObject.transform.position);

            // Spawn a random powerup
            gridManager.RandomChancePowerupSpawnAtWorldCoordinate(gameObject.transform.position);
            gameObject.SetActive(false);
        }

        public void OnPathFound(Vector2[] newPath, bool pathSuccessful)
        {
            // Object pooling means that sometimes the enemy will be inactive, have to account for that in this conditional
            if (pathSuccessful && gameObject.activeSelf)
            {
                path = newPath;
                targetIndex = 0;
                StopCoroutine("FollowPath");
                if (path.Length > 0)
                {
                    StartCoroutine("FollowPath");
                }
            }
        }

        public void OnDrawGizmos()
        {
            if (path != null)
            {
                for (int i = targetIndex; i < path.Length; i++)
                {
                    Gizmos.color = Color.black;
                    Gizmos.DrawCube(path[i], Vector3.one * (GridManager.TileRadius / 10));

                    if (i == targetIndex)
                    {
                        Gizmos.DrawLine(transform.position, path[i]);
                    }
                    else
                    {
                        Gizmos.DrawLine(path[i - 1], path[i]);
                    }
                }
            }
        }

        protected void Awake()
        {
            EnemyCount++;
            animator = GetComponent<Animator>();
            gameManager = GameObject.FindObjectOfType<GameManager>();
            defaultMaterial = new Material(Shader.Find("Sprites/Default"));
            whiteMaterial = Resources.Load("WhiteMaterial") as Material;
            spriteRenderer = GetComponent<SpriteRenderer>();
            gridManager = GameObject.FindObjectOfType<GridManager>();
            audioManager = GameObject.FindObjectOfType<AudioManager>();
        }

        protected IEnumerator FlashSprite()
        {
            spriteRenderer.material = whiteMaterial;
            yield return new WaitForSeconds(0.1f);
            spriteRenderer.material = defaultMaterial;
        }

        protected void ShootBullet(Vector2 bulletOrigin, Vector3 moveDirection)
        {
            GameObject bullet = ObjectPooler.Instance.GetPooledObjectAndMoveTo(ObjectPooler.ObjectType.EnemyBullet, transform.position);
            bullet.transform.position = transform.position;
            bullet.GetComponent<EnemyBullet>().SetMoveDirection(moveDirection);
            if (!player.FreezeMotion)
            {
                audioManager.Play(AudioManager.WarhoundSounds.bullet2);
            }
        }

        protected void OnTriggerEnter2D(Collider2D collider)
        {
            Bullet projectile = collider.gameObject.GetComponent<Bullet>();
            if (projectile != null && !collider.gameObject.GetComponent<EnemyBullet>())
            {
                if (projectile.Hp > 0)
                {
                    int tempProjectileHp = projectile.Hp;
                    projectile.Hp -= Hp;
                    Hp -= tempProjectileHp;
                    if (projectile.Hp <= 0)
                    {
                        projectile.Hp = 0;
                        projectile.DestroyBullet();
                    }
                }

                if (gameObject.activeInHierarchy)
                {
                    StartCoroutine("FlashSprite");
                }

                if (Hp <= 0)
                {
                    Destroy();
                }
            }
        }

        protected void OnEnable()
        {
            Hp = maxHp;
            gameManager.EnemySpawnCount += 1;
            player = gameManager.GetPlayerComponent();
            player.PreviousNode = new Node(false, new Vector2(1, 1), 1, 1, 1);
            spriteRenderer.material = defaultMaterial;
            playerTransform = gameManager.GetPlayerGameObject().transform;
        }

        protected void OnDisable()
        {
            gameManager.EnemyKillCount += 1;
            patrolIndex = 0;
        }

        /// <summary>
        /// Should be called every frame
        /// </summary>
        protected void RecalculatePathIfNeeded()
        {            
            if (player.HasEnemyDestinationChanged && !didRecalculatePath)
            {
                didRecalculatePath = true;
                player.NumEnemiesThatStillNeedToRecalculatePaths--;

                // Recalculate path
                PathRequestManager.RequestPathAndMove(transform.position, playerTransform.position, OnPathFound);
            }

            if (player.DoneRecalculatingAllPaths)
            {
                didRecalculatePath = false;
            }
        }

        private void Start()
        {
            animator = GetComponent<Animator>();
        }

        private IEnumerator FollowPath()
        {
            currentWaypoint = path[0];

            while (true)
            {
                if (new Vector2(transform.position.x, transform.position.y) == currentWaypoint)
                {
                    targetIndex++;
                    if (targetIndex >= path.Length)
                    {
                        EnrouteToWaypoint = false;
                        yield break;
                    }

                    currentWaypoint = path[targetIndex];
                }

                transform.position = Vector2.MoveTowards(transform.position, currentWaypoint, moveSpeed * Time.deltaTime);
                yield return null;
            }
        }
    }
}