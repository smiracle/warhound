﻿using Assets.Game.Scripts.Map;
using UnityEngine;

namespace Assets.Game.Scripts.EnemyScripts
{
    public class Eyeball : Enemy
    {
        private Rigidbody2D rigidbody2d;

        private void Start()
        {
            rigidbody2d = GetComponent<Rigidbody2D>();
            maxHp = 2;
            Hp = maxHp;
            moveSpeed = GridManager.TileRadius * 5f;
        }

        private void FixedUpdate()
        {
            if (IsGroundObjectBetweenEnemyAndPlayer())
            {
                rigidbody2d.velocity = rigidbody2d.velocity * 0.97f;
            }
            else
            {
                // Fly toward player at constant speed with a slight arc
                Vector2 velocity = new Vector2(transform.position.x - player.transform.position.x, transform.position.y - player.transform.position.y).normalized * moveSpeed;
                rigidbody2d.velocity = -velocity;
            }
        }

        private bool IsGroundObjectBetweenEnemyAndPlayer()
        {            
            Vector3 fromPosition = transform.position;
            Vector3 toPosition = player.transform.position;
            Vector3 direction = toPosition - fromPosition;

            if (Physics2D.Linecast(player.transform.position, gameObject.transform.position, 1 << 8))
            {
                Debug.DrawRay(transform.position, direction, Color.green);
                return true;
            }

            return false;
        }
    }
}