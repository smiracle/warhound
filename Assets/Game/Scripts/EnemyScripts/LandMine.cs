﻿namespace Assets.Game.Scripts.EnemyScripts
{
    public class LandMine : Enemy
    {
        protected new void OnDisable()
        {
            base.OnDisable();
            CancelInvoke();
        }

        private void Start()
        {
            maxHp = 10;
            Hp = maxHp;
        }
    }
}