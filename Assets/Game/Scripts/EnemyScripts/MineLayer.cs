﻿using System.Collections.Generic;
using Assets.Game.Scripts.Map;
using UnityEngine;

namespace Assets.Game.Scripts.EnemyScripts
{
    public class MineLayer : Enemy
    {
        private int updateNum = 0;
        private int randomUpdateNum = 1;

        protected new void OnDisable()
        {
            base.OnDisable();
            CancelInvoke();
        }

        protected new void OnEnable()
        {
            base.OnEnable();
            MoveTo = GetRandomUnblockedCenterWorldCoordinate();
        }

        private Vector2 GetRandomUnblockedCenterWorldCoordinate()
        {
            List<Node> unblockedCenterNodes = gridManager.GetUnblockedCenterNodes();
            return unblockedCenterNodes[Random.Range(0, unblockedCenterNodes.Count)].WorldPosition;
        }

        private void Start()
        {
            maxHp = 2;
            Hp = maxHp;
            moveSpeed = GridManager.TileRadius * 6.5f;
        }

        private void FixedUpdate()
        {
            updateNum++;
            if (updateNum % randomUpdateNum == 0)
            {
                UpdatePatrol();
                randomUpdateNum = Random.Range(5, 15);
                updateNum = 0;
            }
        }

        private void UpdatePatrol()
        {
            PathRequestManager.RequestPathAndMove(transform.position, MoveTo, OnPathFound);
            if (Vector2.Distance(transform.position, MoveTo) < GridManager.TileRadius)
            {
                ObjectPooler.Instance.GetPooledObjectAndMoveTo(ObjectPooler.ObjectType.LandMine, transform.position);
                patrolIndex++;
                Destroy();
            }
        }
    }
}