﻿using System.Collections.Generic;
using System.Linq;
using Assets.Game.Scripts.Map;
using Assets.Game.Scripts.PlayerScripts;
using UnityEngine;

namespace Assets.Game.Scripts.EnemyScripts
{
    public class ObjectPooler : MonoBehaviour
    {
        public static ObjectPooler Instance;
        public List<GameObject> Prefabs;
        private GameObject enemyParent;
        private GameObject bulletParent;
        private GameObject enemyBulletParent;
        private GameObject explosionParent;
        private GameObject powerupParent;
        private Dictionary<ObjectType, List<GameObject>> pooledObjectsByType = new Dictionary<ObjectType, List<GameObject>>();
        private ObjectType[] droppablePowerups = new ObjectType[]
        {
            ObjectType.Powerup_MachineGun,
            ObjectType.Powerup_Shotgun,
            ObjectType.Powerup_Circle,
            ObjectType.Powerup_Grenade,
            ObjectType.Powerup_Helmet,
            ObjectType.Powerup_Medal,
            ObjectType.Powerup_Dollar
        };

        public enum ObjectType
        {
            Robot,
            Roboboss,
            Roboboss2,
            Bullet,
            EnemyBullet,
            Explosion,
            MineLayer,
            LandMine,
            Robodog,
            Spider,
            Roboshield,
            Bat,
            Eyeball,
            Brain,
            Crystal,
            Powerup_MachineGun,
            Powerup_Shotgun,
            Powerup_Circle,
            Powerup_Grenade,
            Powerup_Helmet,
            Powerup_Medal,
            Powerup_Dollar
        }

        public void DeactivateAllEnemiesAffectedByGrenadeFromBoard()
        {
            SetAllInactive(ObjectPooler.ObjectType.Bat);
            SetAllInactive(ObjectPooler.ObjectType.Eyeball);
            SetAllInactive(ObjectPooler.ObjectType.LandMine);
            SetAllInactive(ObjectPooler.ObjectType.MineLayer);
            SetAllInactive(ObjectPooler.ObjectType.Robot);
            SetAllInactive(ObjectPooler.ObjectType.Robodog);
            SetAllInactive(ObjectPooler.ObjectType.Spider);
            SetAllInactive(ObjectPooler.ObjectType.Roboshield);
        }

        public void DeactivateAllEnemiesAndPowerupsForPlayerHitEvent()
        {
            SetAllInactive(ObjectPooler.ObjectType.Bat);
            SetAllInactive(ObjectPooler.ObjectType.Eyeball);
            SetAllInactive(ObjectPooler.ObjectType.LandMine);
            SetAllInactive(ObjectPooler.ObjectType.MineLayer);
            SetAllInactive(ObjectPooler.ObjectType.Robot);
            SetAllInactive(ObjectPooler.ObjectType.Robodog);
            SetAllInactive(ObjectPooler.ObjectType.Spider);
            SetAllInactive(ObjectPooler.ObjectType.Roboshield);
            SetAllInactive(ObjectPooler.ObjectType.Powerup_MachineGun);
            SetAllInactive(ObjectPooler.ObjectType.Powerup_Shotgun);
            SetAllInactive(ObjectPooler.ObjectType.Powerup_Grenade);
            SetAllInactive(ObjectPooler.ObjectType.Powerup_Helmet);
            SetAllInactive(ObjectPooler.ObjectType.Powerup_Circle);
            SetAllInactive(ObjectPooler.ObjectType.Powerup_Medal);
            SetAllInactive(ObjectPooler.ObjectType.Powerup_Dollar);
        }

        public void DeactivateAllEnemiesBossesAndPowerupsForGameOverEvent()
        {
            SetAllInactive(ObjectPooler.ObjectType.Bat);
            SetAllInactive(ObjectPooler.ObjectType.Eyeball);
            SetAllInactive(ObjectPooler.ObjectType.LandMine);
            SetAllInactive(ObjectPooler.ObjectType.MineLayer);
            SetAllInactive(ObjectPooler.ObjectType.Robot);
            SetAllInactive(ObjectPooler.ObjectType.Robodog);
            SetAllInactive(ObjectPooler.ObjectType.Spider);
            SetAllInactive(ObjectPooler.ObjectType.Roboboss);
            SetAllInactive(ObjectPooler.ObjectType.Roboboss2);
            SetAllInactive(ObjectPooler.ObjectType.Brain);
            SetAllInactive(ObjectPooler.ObjectType.Roboshield);
            SetAllInactive(ObjectPooler.ObjectType.Powerup_MachineGun);
            SetAllInactive(ObjectPooler.ObjectType.Powerup_Shotgun);
            SetAllInactive(ObjectPooler.ObjectType.Powerup_Grenade);
            SetAllInactive(ObjectPooler.ObjectType.Powerup_Helmet);
            SetAllInactive(ObjectPooler.ObjectType.Powerup_Circle);
            SetAllInactive(ObjectPooler.ObjectType.Powerup_Medal);
            SetAllInactive(ObjectPooler.ObjectType.Powerup_Dollar);
        }

        public void DeactivateAllPowerupsAndEnemiesForBoardScroll()
        {
            SetAllInactive(ObjectPooler.ObjectType.Bat);
            SetAllInactive(ObjectPooler.ObjectType.Eyeball);
            SetAllInactive(ObjectPooler.ObjectType.LandMine);
            SetAllInactive(ObjectPooler.ObjectType.MineLayer);
            SetAllInactive(ObjectPooler.ObjectType.Robot);
            SetAllInactive(ObjectPooler.ObjectType.Robodog);
            SetAllInactive(ObjectPooler.ObjectType.Roboboss);
            SetAllInactive(ObjectPooler.ObjectType.Roboboss2);
            SetAllInactive(ObjectPooler.ObjectType.Spider);
            SetAllInactive(ObjectPooler.ObjectType.Roboshield);
            SetAllInactive(ObjectPooler.ObjectType.Powerup_MachineGun);
            SetAllInactive(ObjectPooler.ObjectType.Powerup_Shotgun);
            SetAllInactive(ObjectPooler.ObjectType.Powerup_Grenade);
            SetAllInactive(ObjectPooler.ObjectType.Powerup_Helmet);
            SetAllInactive(ObjectPooler.ObjectType.Powerup_Circle);
            SetAllInactive(ObjectPooler.ObjectType.Powerup_Medal);
            SetAllInactive(ObjectPooler.ObjectType.Powerup_Dollar);
        }

        public GameObject GetPooledObjectAndMoveTo(ObjectType objectType, Vector2 moveToWorldCoord)
        {
            for (int i = 0; i < pooledObjectsByType[objectType].Count; i++)
            {
                if (!pooledObjectsByType[objectType][i].activeInHierarchy)
                {
                    pooledObjectsByType[objectType][i].SetActive(true);
                    pooledObjectsByType[objectType][i].transform.position = moveToWorldCoord;
                    var enemyComponent = pooledObjectsByType[objectType][i].GetComponent<Enemy>();
                    if (enemyComponent != null)
                    {
                        enemyComponent.EnrouteToWaypoint = false; // Reset explicit waypoint pathfinding
                    }

                    return pooledObjectsByType[objectType][i];
                }
            }

            GameObject obj = CreateAndReturnNewObject(objectType);
            obj.transform.position = moveToWorldCoord;
            obj.SetActive(true);
            return obj;
        }

        public ObjectType[] GetDroppablePowerupsArray()
        {
            return droppablePowerups;
        }

        private void PoolObjectMultipleTimes(ObjectType objectType, int count)
        {
            for (int i = 0; i < count; i++)
            {
                var newObject = CreateAndReturnNewObject(objectType);
                newObject.SetActive(false);
            }
        }

        private void Start()
        {
            enemyParent = new GameObject("EnemyParent");
            bulletParent = new GameObject("BulletParent");
            enemyBulletParent = new GameObject("EnemyBulletParent");
            explosionParent = new GameObject("ExplosionParent");
            powerupParent = new GameObject("PowerupParent");
            pooledObjectsByType.Add(ObjectType.Bullet, new List<GameObject>());
            pooledObjectsByType.Add(ObjectType.EnemyBullet, new List<GameObject>());
            pooledObjectsByType.Add(ObjectType.Explosion, new List<GameObject>());
            pooledObjectsByType.Add(ObjectType.LandMine, new List<GameObject>());
            pooledObjectsByType.Add(ObjectType.MineLayer, new List<GameObject>());
            pooledObjectsByType.Add(ObjectType.Robodog, new List<GameObject>());
            pooledObjectsByType.Add(ObjectType.Bat, new List<GameObject>());
            pooledObjectsByType.Add(ObjectType.Eyeball, new List<GameObject>());
            pooledObjectsByType.Add(ObjectType.Roboboss, new List<GameObject>());
            pooledObjectsByType.Add(ObjectType.Roboboss2, new List<GameObject>());
            pooledObjectsByType.Add(ObjectType.Robot, new List<GameObject>());
            pooledObjectsByType.Add(ObjectType.Spider, new List<GameObject>());
            pooledObjectsByType.Add(ObjectType.Roboshield, new List<GameObject>());
            pooledObjectsByType.Add(ObjectType.Brain, new List<GameObject>());
            pooledObjectsByType.Add(ObjectType.Crystal, new List<GameObject>());
            pooledObjectsByType.Add(ObjectType.Powerup_Circle, new List<GameObject>());
            pooledObjectsByType.Add(ObjectType.Powerup_Grenade, new List<GameObject>());
            pooledObjectsByType.Add(ObjectType.Powerup_Helmet, new List<GameObject>());
            pooledObjectsByType.Add(ObjectType.Powerup_MachineGun, new List<GameObject>());
            pooledObjectsByType.Add(ObjectType.Powerup_Shotgun, new List<GameObject>());
            pooledObjectsByType.Add(ObjectType.Powerup_Medal, new List<GameObject>());
            pooledObjectsByType.Add(ObjectType.Powerup_Dollar, new List<GameObject>());

            // Set initial pooling amounts here
            PoolObjectMultipleTimes(ObjectType.Bullet, 20);
            PoolObjectMultipleTimes(ObjectType.Explosion, 20);
        }

        private void Awake()
        {
            Instance = this;
        }

        private void SetAllInactive(ObjectType objectType)
        {
            foreach (GameObject pooledObject in pooledObjectsByType[objectType])
            {
                pooledObject.SetActive(false);
                var enemyComponent = pooledObject.GetComponent<Enemy>();
                if (enemyComponent != null)
                {
                    enemyComponent.EnrouteToWaypoint = false; // Reset explicit waypoint pathfinding
                }
            }
        }

        private GameObject GetParent(ObjectType objectType)
        {
            switch (objectType)
            {
                case ObjectType.Bullet:
                    return bulletParent;
                case ObjectType.EnemyBullet:
                    return enemyBulletParent;
                case ObjectType.Explosion:
                    return explosionParent;
                case ObjectType.Roboboss:
                case ObjectType.Roboboss2:
                case ObjectType.LandMine:
                case ObjectType.MineLayer:
                case ObjectType.Robodog:
                case ObjectType.Bat:
                case ObjectType.Eyeball:
                case ObjectType.Robot:
                case ObjectType.Spider:
                case ObjectType.Roboshield:
                case ObjectType.Brain:
                case ObjectType.Crystal:
                    return enemyParent;
                case ObjectType.Powerup_Circle:
                case ObjectType.Powerup_Grenade:
                case ObjectType.Powerup_Helmet:
                case ObjectType.Powerup_MachineGun:
                case ObjectType.Powerup_Shotgun:
                case ObjectType.Powerup_Medal:
                case ObjectType.Powerup_Dollar:
                    return powerupParent;
                default:
                    throw new System.Exception("Parent object not found for type " + objectType);
            }
        }

        private GameObject GetPrefab(ObjectType objectType)
        {
            switch (objectType)
            {
                case ObjectType.Bullet:
                    return Prefabs.Where(x => x.GetComponent<Bullet>()).First();
                case ObjectType.EnemyBullet:
                    return Prefabs.Where(x => x.GetComponent<EnemyBullet>()).First();
                case ObjectType.Explosion:
                    return Prefabs.Where(x => x.GetComponent<Explosion>()).First();
                case ObjectType.Bat:
                    return Prefabs.Where(x => x.GetComponent<Bat>()).First();
                case ObjectType.Eyeball:
                    return Prefabs.Where(x => x.GetComponent<Eyeball>()).First();
                case ObjectType.LandMine:
                    return Prefabs.Where(x => x.GetComponent<LandMine>()).First();
                case ObjectType.MineLayer:
                    return Prefabs.Where(x => x.GetComponent<MineLayer>()).First();
                case ObjectType.Roboboss:
                    return Prefabs.Where(x => x.GetComponent<Roboboss>()).First();
                case ObjectType.Roboboss2:
                    return Prefabs.Where(x => x.GetComponent<Roboboss2>()).First();
                case ObjectType.Robot:
                    return Prefabs.Where(x => x.GetComponent<Robot>()).First();
                case ObjectType.Robodog:
                    return Prefabs.Where(x => x.GetComponent<Robodog>()).First();
                case ObjectType.Spider:
                    return Prefabs.Where(x => x.GetComponent<Spider>()).First();
                case ObjectType.Roboshield:
                    return Prefabs.Where(x => x.GetComponent<Roboshield>()).First();
                case ObjectType.Brain:
                    return Prefabs.Where(x => x.GetComponent<Brain>()).First();
                case ObjectType.Powerup_Circle:
                    return Prefabs.Where(x => x.tag == "Powerup_Circle").First();
                case ObjectType.Powerup_Grenade:
                    return Prefabs.Where(x => x.tag == "Powerup_Grenade").First();
                case ObjectType.Powerup_Helmet:
                    return Prefabs.Where(x => x.tag == "Powerup_Helmet").First();
                case ObjectType.Powerup_MachineGun:
                    return Prefabs.Where(x => x.tag == "Powerup_MachineGun").First();
                case ObjectType.Powerup_Shotgun:
                    return Prefabs.Where(x => x.tag == "Powerup_Shotgun").First();
                case ObjectType.Powerup_Medal:
                    return Prefabs.Where(x => x.tag == "Powerup_Medal").First();
                case ObjectType.Powerup_Dollar:
                    return Prefabs.Where(x => x.tag == "Powerup_Dollar").First();
                default:
                    throw new System.Exception("Object pool prefab not found for type " + objectType);
            }
        }
        
        private GameObject CreateAndReturnNewObject(ObjectType objectType)
        {
            // Grow the pool automatically
            GameObject obj = (GameObject)Instantiate(GetPrefab(objectType));
            obj.transform.parent = GetParent(objectType).transform;
            pooledObjectsByType[objectType].Add(obj);

            var enemyComponentOfNewObject = obj.GetComponent<Enemy>();
            if (enemyComponentOfNewObject != null)
            {
                enemyComponentOfNewObject.EnrouteToWaypoint = false; // Reset explicit waypoint pathfinding
            }

            return obj;
        }        
    }
}
