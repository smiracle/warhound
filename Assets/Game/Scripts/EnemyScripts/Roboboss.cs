﻿using Assets.Game.Scripts.Map;
using Assets.Game.Scripts.PlayerScripts;
using UnityEngine;

namespace Assets.Game.Scripts.EnemyScripts
{
    public class Roboboss : Enemy
    {
        public GameObject RobobossDialogPrefab;
        private const float DialogDuration = 4f;
        private int behaviorIndex;
        private GameObject robobossDialogInstance;
        private float dialogWaitPeriodStartTime = 1f;
        private float projectileSpeed = 12.6f; // Constant movement speed of bullets
        private float timeOfLastMove = 0f;
        private bool allowShooting;
        private float timeOfLastShot;

        public override void Destroy()
        {
            switch (Random.Range(0, 3))
            {
                case 0:
                    audioManager.Play(AudioManager.WarhoundSounds.enemyDeath);
                    break;
                case 1:
                    audioManager.Play(AudioManager.WarhoundSounds.enemyDeath2);
                    break;
                case 2:
                    audioManager.Play(AudioManager.WarhoundSounds.enemyDeath3);
                    break;
            }

            CancelInvoke();
            Hp = 0;
            spriteRenderer.material = defaultMaterial;
            ObjectPooler.Instance.GetPooledObjectAndMoveTo(ObjectPooler.ObjectType.Explosion, gameObject.transform.position);
            ObjectPooler.Instance.GetPooledObjectAndMoveTo(ObjectPooler.ObjectType.Explosion, gameObject.transform.position + new Vector3(-GridManager.TileDiameter, -GridManager.TileDiameter, 0));
            ObjectPooler.Instance.GetPooledObjectAndMoveTo(ObjectPooler.ObjectType.Explosion, gameObject.transform.position + new Vector3(GridManager.TileDiameter, GridManager.TileDiameter, 0));
            ObjectPooler.Instance.GetPooledObjectAndMoveTo(ObjectPooler.ObjectType.Explosion, gameObject.transform.position + new Vector3(-GridManager.TileDiameter, GridManager.TileDiameter, 0));
            ObjectPooler.Instance.GetPooledObjectAndMoveTo(ObjectPooler.ObjectType.Explosion, gameObject.transform.position + new Vector3(GridManager.TileDiameter, -GridManager.TileDiameter, 0));
            ObjectPooler.Instance.GetPooledObjectAndMoveTo(ObjectPooler.ObjectType.Powerup_Helmet, gameObject.transform.position);
            gameObject.SetActive(false);
        }        

        protected new void OnEnable()
        {
            dialogWaitPeriodStartTime = Time.time;
            robobossDialogInstance = GameObject.Instantiate(RobobossDialogPrefab, gameObject.transform.position + new Vector3(GridManager.TileRadius * 2, -GridManager.TileRadius * 4, 0f), Quaternion.identity);
            GameObject.Destroy(robobossDialogInstance, DialogDuration);
            Hp = 25;
            maxHp = Hp;
            gameManager.BossHealth = Hp;
            gameManager.BossHealthMax = Hp;
            gameManager.IsBossActive = true;
            base.OnEnable();
        }

        protected new void OnDisable()
        {
            base.OnDisable();
        }

        protected new void OnTriggerEnter2D(Collider2D collider)
        {
            Bullet projectile = collider.gameObject.GetComponent<Bullet>();
            if (projectile != null && !collider.gameObject.GetComponent<EnemyBullet>())
            {
                if (projectile.Hp > 0)
                {
                    int tempProjectileHp = projectile.Hp;
                    projectile.Hp -= Hp;
                    Hp -= tempProjectileHp;
                    gameManager.BossHealth = Hp;
                    if (projectile.Hp <= 0)
                    {
                        projectile.Hp = 0;
                        projectile.DestroyBullet();
                    }
                }

                StartCoroutine("FlashSprite");
                if (Hp <= 0)
                {
                    Destroy();
                }
            }
        }

        private void Start()
        {
            moveSpeed = GridManager.TileRadius * 7.2f;            
        }

        private void Update()
        {
            if (Time.time > dialogWaitPeriodStartTime + DialogDuration)
            {
                UpdatePatrol();
            }

            UpdateShooting();
        }

        private void UpdateShooting()
        {
            if (allowShooting)
            {
                if (Time.time > timeOfLastShot + 0.35f)
                {
                    animator.SetFloat("DirectionY", 1f);
                    timeOfLastShot = Time.time;
                    ShootBullet(new Vector2(0f, 1.0f), new Vector3(Random.Range(-2.5f, 7f), 10f, 0f) * projectileSpeed);
                }
            }
            else
            {
                animator.SetFloat("DirectionY", -1f);
            }
        }

        private void UpdatePatrol()
        {
            animator.SetBool("IsMoving", allowShooting);
            if (!EnrouteToWaypoint)
            {
                switch (behaviorIndex)
                {
                    case 0:
                        if (Time.time > timeOfLastMove + 1f)
                        {
                            allowShooting = false;
                        }

                        if (Time.time > timeOfLastMove + 3f)
                        {
                            timeOfLastMove = Time.time;
                            behaviorIndex = Random.Range(1, 6);
                        }

                        break;
                    case 1:
                        // Slight left
                        if (Time.time > timeOfLastMove + 2f)
                        {
                            timeOfLastMove = Time.time;
                            behaviorIndex = 7;
                        }
                        else
                        {
                            allowShooting = true;
                            PathRequestManager.RequestPathAndMove(transform.position, gridManager.GetWorldCoordinatesFromNodeCoordinates(new Vector2(9, 13)), OnPathFound);
                        }

                        break;
                    case 2:
                        // left
                        if (Time.time > timeOfLastMove + 2f)
                        {
                            timeOfLastMove = Time.time;
                            behaviorIndex = 7;
                        }
                        else
                        {
                            allowShooting = true;
                            PathRequestManager.RequestPathAndMove(transform.position, gridManager.GetWorldCoordinatesFromNodeCoordinates(new Vector2(3, 13)), OnPathFound);
                        }

                        break;
                    case 3:
                        // Far left
                        if (Time.time > timeOfLastMove + 2f)
                        {
                            timeOfLastMove = Time.time;
                            behaviorIndex = 7;
                        }
                        else
                        {
                            allowShooting = true;
                            PathRequestManager.RequestPathAndMove(transform.position, gridManager.GetWorldCoordinatesFromNodeCoordinates(new Vector2(1, 13)), OnPathFound);
                        }

                        break;
                    case 4:
                        // Walk to slight right
                        if (Time.time > timeOfLastMove + 2f)
                        {
                            timeOfLastMove = Time.time;
                            behaviorIndex = 7;
                        }
                        else
                        {
                            allowShooting = true;
                            PathRequestManager.RequestPathAndMove(transform.position, gridManager.GetWorldCoordinatesFromNodeCoordinates(new Vector2(9, 13)), OnPathFound);
                        }

                        break;
                    case 5:
                        // Right
                        if (Time.time > timeOfLastMove + 2f)
                        {
                            timeOfLastMove = Time.time;
                            behaviorIndex = 7;
                        }
                        else
                        {
                            allowShooting = true;
                            PathRequestManager.RequestPathAndMove(transform.position, gridManager.GetWorldCoordinatesFromNodeCoordinates(new Vector2(11, 13)), OnPathFound);
                        }

                        break;
                    case 6:
                        // Far right
                        if (Time.time > timeOfLastMove + 2f)
                        {
                            timeOfLastMove = Time.time;
                            behaviorIndex = 7;
                        }
                        else
                        {
                            allowShooting = true;
                            PathRequestManager.RequestPathAndMove(transform.position, gridManager.GetWorldCoordinatesFromNodeCoordinates(new Vector2(13, 13)), OnPathFound);
                        }

                        break;
                    case 7:
                        // Walk to middle
                        allowShooting = true;
                        timeOfLastMove = Time.time;
                        PathRequestManager.RequestPathAndMove(transform.position, gridManager.GetWorldCoordinatesFromNodeCoordinates(new Vector2(7, 13)), OnPathFound);
                        behaviorIndex = 0;
                        break;
                }
            }
        }
    }
}