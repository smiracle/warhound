﻿using Assets.Game.Scripts.Map;

namespace Assets.Game.Scripts.EnemyScripts
{
    public class Robodog : Enemy
    {
        private void Start()
        {
            moveSpeed = GridManager.TileRadius * 5f;
            maxHp = 1;
            Hp = maxHp;
        }

        private void FixedUpdate()
        {
            RecalculatePathIfNeeded();
        }
    }
}