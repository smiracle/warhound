﻿namespace Assets.Game.Scripts.EnemyScripts
{
    public class Roboshield : Enemy
    {
        private void Start()
        {
            moveSpeed = 40f;
            maxHp = 5;
            Hp = maxHp;
        }

        private void FixedUpdate()
        {
            RecalculatePathIfNeeded();
        }
    }
}