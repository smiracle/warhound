﻿namespace Assets.Game.Scripts.EnemyScripts
{
    public class Robot : Enemy
    {
        private void Start()
        {
            maxHp = 1;
            Hp = maxHp;
        }

        private void FixedUpdate()
        {
            RecalculatePathIfNeeded();
        }
    }
}