﻿using Assets.Game.Scripts.Map;

namespace Assets.Game.Scripts.EnemyScripts
{
    public class Spider : Enemy
    {
        private void Start()
        {
            moveSpeed = GridManager.TileRadius * 4.5f;
            maxHp = 2;
            Hp = maxHp;
        }

        private void FixedUpdate()
        {
            RecalculatePathIfNeeded();
        }
    }
}