﻿using UnityEngine;

/// <summary>
/// Draws a useful reference grid in the editor in Unity.
/// </summary>
public class DrawGizmoGrid : MonoBehaviour
{
    // universal grid scale
    public float GridScale = 1f;

    // extents of the grid
    public int MinX = -0;
    public int MinY = 0;
    public int MaxX = 16;
    public int MaxY = 16;

    // nudges the whole grid rel
    public Vector3 GridOffset = Vector3.zero;

    // is this an XY or an XZ grid?
    public bool TopDownGrid = true;

    // choose a colour for the gizmos
    public int GizmoMajorLines = 5;
    public Color GizmoLineColor = new Color(0.4f, 0.4f, 0.3f, 1f);

    // rename + centre the gameobject upon first time dragging the script into the editor. 
    private void Reset()
    {
        if (name == "GameObject")
        {
            name = "~~ GIZMO GRID ~~";
        }

        transform.position = Vector3.zero;
    }

    // Draw the grid
    private void OnDrawGizmos()
    {
        // orient to the gameobject, so you can rotate the grid independently if desired
        Gizmos.matrix = transform.localToWorldMatrix;

        // set colours
        Color dimColor = new Color(GizmoLineColor.r, GizmoLineColor.g, GizmoLineColor.b, 0.25f * GizmoLineColor.a);
        Color brightColor = Color.Lerp(Color.white, GizmoLineColor, 0.75f);

        // draw the horizontal lines
        for (int x = MinX; x < MaxX + 1; x++)
        {
            // find major lines
            Gizmos.color = x % GizmoMajorLines == 0 ? GizmoLineColor : dimColor;
            if (x == 0)
            {
                Gizmos.color = brightColor;
            }

            Vector3 pos1 = new Vector3(x, MinY, 0) * GridScale;
            Vector3 pos2 = new Vector3(x, MaxY, 0) * GridScale;

            // convert to topdown/overhead units if necessary
            if (TopDownGrid)
            {
                pos1 = new Vector3(pos1.x, 0, pos1.y);
                pos2 = new Vector3(pos2.x, 0, pos2.y);
            }

            Gizmos.DrawLine(GridOffset + pos1, GridOffset + pos2);
        }

        // draw the vertical lines
        for (int y = MinY; y < MaxY + 1; y++)
        {
            // find major lines
            Gizmos.color = y % GizmoMajorLines == 0 ? GizmoLineColor : dimColor;
            if (y == 0)
            {
                Gizmos.color = brightColor;
            }

            Vector3 pos1 = new Vector3(MinX, y, 0) * GridScale;
            Vector3 pos2 = new Vector3(MaxX, y, 0) * GridScale;

            // convert to topdown/overhead units if necessary
            if (TopDownGrid)
            {
                pos1 = new Vector3(pos1.x, 0, pos1.y);
                pos2 = new Vector3(pos2.x, 0, pos2.y);
            }

            Gizmos.DrawLine(GridOffset + pos1, GridOffset + pos2);
        }
    }
}