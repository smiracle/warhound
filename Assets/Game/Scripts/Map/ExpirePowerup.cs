﻿using System.Collections;
using UnityEngine;

public class ExpirePowerup : MonoBehaviour
{
    private const float Duration = 7f;
    private const float FlashSpeed = 0.1f;
    private float startTime;
    private SpriteRenderer spriteRenderer;
    private bool isFlashing;

    private void Start()
    {
        startTime = Time.time;        
        spriteRenderer = GetComponent<SpriteRenderer>();
    }

    private void OnEnable()
    {
        startTime = Time.time;
    }

    private void OnDisable()
    {
        StopAllCoroutines();
    }

    private void FixedUpdate()
    {
        if (Time.time > Duration + startTime)
        {
            gameObject.SetActive(false);
        }
        else if (Time.time > Duration - 3 + startTime && !isFlashing)
        {
            isFlashing = true;
            StartCoroutine(Flash(FlashSpeed));
        }
    }

    private IEnumerator Flash(float x)
    {
        while (true)
        {
            spriteRenderer.enabled = false;
            yield return new WaitForSeconds(x);
            spriteRenderer.enabled = true;
            yield return new WaitForSeconds(x);
        }
    }
}
