﻿using System.Collections;
using UnityEngine;

namespace Assets.Game.Scripts.Map
{
    public class Explosion : MonoBehaviour
    {
        private float delay = 0.0f;
        private float animationDuration;

        public IEnumerator DelayedDestruction()
        {
            yield return new WaitForSeconds(animationDuration + delay);
            gameObject.SetActive(false);
            yield break;
        }

        private void Awake()
        {
            animationDuration = GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).length;
        }

        private void OnEnable()
        {
            StartCoroutine("DelayedDestruction");
        }
    }
}
