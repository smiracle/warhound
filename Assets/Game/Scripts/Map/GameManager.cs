﻿using System.Collections;
using Assets.Game.Scripts.EnemyScripts;
using Assets.Game.Scripts.PlayerScripts;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Game.Scripts.Map
{
    public class GameManager : MonoBehaviour
    {
        public const float GameAreaSize = 167f;
        public const float SpawnDelayAfterDeathAndScroll = 3f;
        public const float GameAreaOffset = 29.9f;
        [ShowOnly]
        public bool AllowBoardScroll;
        [HideInInspector]
        public bool HasPlayerReachedBoundary;
        [HideInInspector]
        public GameObject CurrentBoard;
        [HideInInspector]
        public GameObject NextBoard;
        [HideInInspector]
        public ProgressionManager ProgressionManager;
        [HideInInspector]
        public int[] BootCosts = { 7, 14, 21 };
        [HideInInspector]
        public int[] GunCosts = { 10, 20, 30 };
        [HideInInspector]
        public int[] AmmoCosts = { 15, 30, 45 };
        public GameObject EndingCarPrefab;
        [HideInInspector]
        public GameObject EndingCarInstance;
        public GameObject EndingDialogPrefab;
        [HideInInspector]
        public GameObject EndingDialogInstance;
        public GameObject GameTitleText;
        [HideInInspector]
        public float TimeOfLastDeathOrBoardSetup;
        [ShowOnly]
        public bool IsBossActive;
        [ShowOnly]
        public float BossHealth;
        [ShowOnly]
        public float BossHealthMax;
        [ShowOnly]
        public float TimeRemaining;
        [ShowOnly]
        public float TimeRemainingMax;
        [ShowOnly]
        public bool IgnoreTimeIncreases;
        [ShowOnly]
        public int EnemyKillCount;
        [ShowOnly]
        public int EnemySpawnCount;
        public GameObject PlayerPrefab;
        public Text NumLivesText;
        public Text NumDollarsText;
        public Text TimeRemainingText;
        public Image TimerImage;
        public Image MachineGunImage;
        public Image CircleImage;
        public Image ShotgunImage;
        public Image GrenadeImage;
        public Image MedalImage;
        public GameObject GameTitle;
        public GameObject GameTitleBlinkText;
        public GameObject GameInterface;
        public GameObject PowerupBox;
        public GameObject BlinkingArrowPrefab;
        public GameObject MerchantPrefab;
        public GameObject MerchantMatPrefab;
        public GameObject ClockIcon;
        public GameObject HeartIcon;
        public GameObject PassivePowerupIconParent;
        public Image PassiveBootImage;
        public Image PassiveGunImage;
        public Image PassiveAmmoImage;
        public GameObject LeftJoystickContainerPrefab;
        public GameObject RightJoystickContainerPrefab;
        public GameObject[] BootPowerupPrefabArray;
        public GameObject[] GunPowerupPrefabArray;
        public GameObject[] AmmoPowerupPrefabArray;
        [HideInInspector]
        public GameObject BootPowerupInstance;
        [HideInInspector]
        public GameObject GunPowerupInstance;
        [HideInInspector]
        public GameObject AmmoPowerupInstance;
        public GameObject PowerupCostParent;
        public Text BootCostText;
        public Text GunCostText;
        public Text AmmoCostText;
        [HideInInspector]
        public TilePooler TilePooler;
        [HideInInspector]
        public GameObject BlinkingArrowInstance;
        [HideInInspector]
        public GameObject MerchantInstance;
        [HideInInspector]
        public GameObject MerchantMatInstance;
        private VirtualJoystickControl rightJoystickContainer;
        private VirtualJoystickControl leftJoystickContainer;
        private Vector3 playerScrollDestination;
        private Vector3 currentBoardScrollDestination;
        private Vector3 nextBoardScrollDestination;
        private GameObject player;
        private Player playerComponent;
        private GridManager gridManager;
        private Image storedPowerupIcon;
        private RectTransform canvasRectTransform;
        private RectTransform leftJoystickRectTransform;
        private RectTransform rightJoystickRectTransform;
        private Canvas canvas;
        private float joystickScreenSpace = (Screen.width - Screen.height) / 2;
        private GridManager grid;
        private bool boardScrollInProgress;
        private float timerImageMaxWidth;
        private bool titleBlinkOn;

        public int Progression
        {
            get
            {
                return ProgressionManager.Progression;
            }

            set
            {
                ProgressionManager.Progression = value;
            }
        }

        public Player GetPlayerComponent()
        {
            return playerComponent;
        }

        public GameObject GetPlayerGameObject()
        {
            return player;
        }

        public VirtualJoystickControl GetLeftJoystick()
        {
            return leftJoystickContainer;
        }

        public VirtualJoystickControl GetRightJoystick()
        {
            return rightJoystickContainer;
        }

        public void StartGame()
        {
            if (player != null)
            {
                GameObject.Destroy(player);
            }

            if (CurrentBoard != null)
            {
                GameObject.Destroy(CurrentBoard);
            }

            player = GameObject.Instantiate(PlayerPrefab);
            playerComponent = player.GetComponent<Player>();
            playerComponent.FreezeMotion = false;
            player.SetActive(false);
            player.transform.position = new Vector3(GameAreaOffset, GameAreaOffset, 0);
            playerComponent.NumLives = 3;
            playerComponent.NumDollars = 0;
            playerComponent.GunLevel = 0;
            playerComponent.AmmoLevel = 0;
            playerComponent.BootLevel = 0;
            playerComponent.ShootingRate = Player.BaseShootingRate;

            PassiveBootImage.sprite = BootPowerupPrefabArray[0].GetComponent<SpriteRenderer>().sprite;
            PassiveBootImage.enabled = false;
            PassiveGunImage.sprite = GunPowerupPrefabArray[0].GetComponent<SpriteRenderer>().sprite;
            PassiveGunImage.enabled = false;
            PassiveAmmoImage.sprite = AmmoPowerupPrefabArray[0].GetComponent<SpriteRenderer>().sprite;
            PassiveAmmoImage.enabled = false;
            HeartIcon.SetActive(false);
            ClockIcon.SetActive(true);
        }

        public void ShowTitle()
        {
            GameTitle.SetActive(true);
            StartCoroutine("BlinkTitle");
        }

        public void HideTitle()
        {
            GameTitle.SetActive(false);
            StopCoroutine("BlinkTitle");
        }

        public void DestroyMerchantAndMatAndPassivePowerups()
        {
            GameObject.Destroy(MerchantInstance);
            GameObject.Destroy(MerchantMatInstance);
            GameObject.Destroy(BootPowerupInstance);
            GameObject.Destroy(GunPowerupInstance);
            GameObject.Destroy(AmmoPowerupInstance);
            PowerupCostParent.SetActive(false);
        }

        public void PrepareBoardsForScrolling()
        {
            Vector2 scrollDestinationInRelationToStart = new Vector2(0, 350f);
            playerComponent.FreezeMotion = true;
            DestroyMerchantAndMatAndPassivePowerups(); // Destroy merchant if he's there
            ObjectPooler.Instance.DeactivateAllPowerupsAndEnemiesForBoardScroll();
            player.GetComponent<BoxCollider2D>().enabled = false;
            player.GetComponent<Rigidbody2D>().simulated = false;
            CurrentBoard = GameObject.Find("Board" + ProgressionManager.ActiveBoardNumber.ToString());
            gridManager.BoardSetup(ProgressionManager.ActiveBoardNumber + 1, true);
            NextBoard = GameObject.Find("Board" + (ProgressionManager.ActiveBoardNumber + 1).ToString());
            ProgressionManager.ActiveBoardNumber += 1;
            playerScrollDestination = new Vector3(player.transform.position.x + scrollDestinationInRelationToStart.x, player.transform.position.y + scrollDestinationInRelationToStart.y, player.transform.position.z);
            currentBoardScrollDestination = new Vector3(CurrentBoard.transform.position.x + scrollDestinationInRelationToStart.x, CurrentBoard.transform.position.y + scrollDestinationInRelationToStart.y, CurrentBoard.transform.position.z);
            nextBoardScrollDestination = new Vector3(NextBoard.transform.position.x + scrollDestinationInRelationToStart.x, NextBoard.transform.position.y + scrollDestinationInRelationToStart.y, NextBoard.transform.position.z);
        }

        public void SetStoredPowerupIcon(StoredPowerups storedPowerup)
        {
            DestroyStoredPowerup();

            switch (storedPowerup)
            {
                case StoredPowerups.CIRCLE_SHOT:
                    storedPowerupIcon = Instantiate(CircleImage, PowerupBox.transform);
                    OffsetStoredPowerupIcon(storedPowerupIcon);
                    break;
                case StoredPowerups.MACHINE_GUN:
                    storedPowerupIcon = Instantiate(MachineGunImage, PowerupBox.transform);
                    OffsetStoredPowerupIcon(storedPowerupIcon);
                    break;
                case StoredPowerups.SHOTGUN:
                    storedPowerupIcon = Instantiate(ShotgunImage, PowerupBox.transform);
                    OffsetStoredPowerupIcon(storedPowerupIcon);
                    break;
                case StoredPowerups.GRENADE:
                    storedPowerupIcon = Instantiate(GrenadeImage, PowerupBox.transform);
                    OffsetStoredPowerupIcon(storedPowerupIcon);
                    break;
                case StoredPowerups.MEDAL:
                    storedPowerupIcon = Instantiate(MedalImage, PowerupBox.transform);
                    OffsetStoredPowerupIcon(storedPowerupIcon);
                    break;
                case StoredPowerups.NONE:
                    DestroyStoredPowerup();
                    break;
            }
        }

        public void DeactivateCurrentBoard()
        {
            if (CurrentBoard != null)
            {
                TilePooler.DeactivateAllTilesForBoard(CurrentBoard.transform);
                GameObject.Destroy(CurrentBoard);
                CurrentBoard = null;
            }
        }

        public void GameOver()
        {
            GameObject.FindObjectOfType<ObjectPooler>().DeactivateAllEnemiesBossesAndPowerupsForGameOverEvent();
            DeactivateCurrentBoard();
            ProgressionManager.Progression = 0;
            ProgressionManager.ActiveBoardNumber = 1;
        }

        private IEnumerator BlinkTitle()
        {
            while (true)
            {
                if (titleBlinkOn)
                {
                    titleBlinkOn = !titleBlinkOn;
                    GameTitleBlinkText.SetActive(false);
                    yield return new WaitForSeconds(0.5f);
                }
                else
                {
                    titleBlinkOn = !titleBlinkOn;
                    GameTitleBlinkText.SetActive(true);
                    yield return new WaitForSeconds(0.5f);
                }
            }
        }

        private void Awake()
        {
            canvas = GameObject.FindObjectOfType<Canvas>();
            canvasRectTransform = GameObject.FindObjectOfType<Canvas>().GetComponent<RectTransform>();
            CreateAndRepositionJoysticks();
            TilePooler = GetComponent<TilePooler>();
            gridManager = GameObject.FindObjectOfType<GridManager>();
            timerImageMaxWidth = TimerImage.GetComponent<RectTransform>().rect.width;
            grid = GameObject.FindObjectOfType<GridManager>();
            ProgressionManager = new ProgressionManager(this, grid);
        }

        private void Start()
        {
            ProgressionManager.ActiveBoardNumber = 1;
            ProgressionManager.Progression = 0;
        }

        private void CreateAndRepositionJoysticks()
        {
            // Create joystick controls
            leftJoystickContainer = GameObject.Instantiate(LeftJoystickContainerPrefab, canvas.transform).GetComponent<VirtualJoystickControl>();
            leftJoystickContainer.name = "LeftJoystickContainer";
            rightJoystickContainer = GameObject.Instantiate(RightJoystickContainerPrefab, canvas.transform).GetComponent<VirtualJoystickControl>();
            rightJoystickContainer.name = "RightJoystickContainer";

            // Set transform variables
            leftJoystickRectTransform = leftJoystickContainer.GetComponent<RectTransform>();
            rightJoystickRectTransform = rightJoystickContainer.GetComponent<RectTransform>();

            // Resize
            leftJoystickRectTransform.sizeDelta = new Vector2(joystickScreenSpace, joystickScreenSpace);
            rightJoystickRectTransform.sizeDelta = new Vector2(joystickScreenSpace, joystickScreenSpace);

            // Resize inner joysticks
            leftJoystickRectTransform.GetChild(0).GetComponent<RectTransform>().sizeDelta = new Vector2(joystickScreenSpace / 2, joystickScreenSpace / 2);
            rightJoystickRectTransform.GetChild(0).GetComponent<RectTransform>().sizeDelta = new Vector2(joystickScreenSpace / 2, joystickScreenSpace / 2);

            // Reposition
            //leftJoystickContainer.transform.position = new Vector2(0, (canvasRectTransform.rect.height - leftJoystickRectTransform.rect.height) / 2);
            //rightJoystickContainer.transform.position = new Vector2(canvasRectTransform.rect.width - rightJoystickRectTransform.rect.width, (canvasRectTransform.rect.height - rightJoystickRectTransform.rect.height) / 2);
            leftJoystickContainer.transform.position = new Vector2(0, (Screen.height - leftJoystickRectTransform.rect.height) / 2);
            rightJoystickContainer.transform.position = new Vector2(Screen.width - rightJoystickRectTransform.rect.width, (Screen.height - rightJoystickRectTransform.rect.height) / 2);
        }

        private void FixedUpdate()
        {
            // Count down if there is no active boss
            if (TimeRemaining != 0 && !playerComponent.FreezeMotion && !IsBossActive)
            {
                TimeRemaining = TimeRemaining - Time.deltaTime;
                if (TimeRemaining < 0)
                {
                    TimeRemaining = 0;
                }
                else if (TimeRemaining > TimeRemainingMax)
                {
                    TimeRemaining = TimeRemainingMax;
                }

                TimerImage.GetComponent<RectTransform>().sizeDelta = new Vector2(timerImageMaxWidth * (TimeRemaining / TimeRemainingMax), TimerImage.GetComponent<RectTransform>().sizeDelta.y);
                TimeRemainingText.text = ((int)TimeRemaining).ToString();
            }
            else if (IsBossActive && HeartIcon.activeInHierarchy && !ClockIcon.activeInHierarchy)
            {
                if (BossHealth <= 0)
                {
                    BossHealth = 0;
                    IsBossActive = false;
                }
                else if (BossHealth > BossHealthMax)
                {
                    BossHealth = BossHealthMax;
                }

                TimerImage.GetComponent<RectTransform>().sizeDelta = new Vector2(timerImageMaxWidth * (BossHealth / BossHealthMax), TimerImage.GetComponent<RectTransform>().sizeDelta.y);
            }

            if (HasPlayerReachedBoundary && !boardScrollInProgress)
            {
                // Scroll the board
                AllowBoardScroll = false;
                boardScrollInProgress = true;
                TimeOfLastDeathOrBoardSetup = Time.time;
                PrepareBoardsForScrolling();
            }
            else if (boardScrollInProgress)
            {
                ScrollBoards();
                return;
            }

            if (ProgressionManager != null)
            {
                ProgressionManager.CheckProgression();
            }
        }

        private void ScrollBoards()
        {
            float scrollMaxDelta = 7f;
            player.transform.position = Vector3.MoveTowards(player.transform.position, playerScrollDestination, scrollMaxDelta);
            CurrentBoard.transform.position = Vector3.MoveTowards(CurrentBoard.transform.position, currentBoardScrollDestination, scrollMaxDelta);
            NextBoard.transform.position = Vector3.MoveTowards(NextBoard.transform.position, nextBoardScrollDestination, scrollMaxDelta);
            GameObject.Destroy(BlinkingArrowInstance);

            if (Vector2.Distance(CurrentBoard.transform.position, currentBoardScrollDestination) < 0.01f)
            {
                // Done scrolling
                playerComponent.FreezeMotion = false;
                boardScrollInProgress = false;
                HasPlayerReachedBoundary = false;
                player.GetComponent<Rigidbody2D>().simulated = true;
                player.GetComponent<BoxCollider2D>().enabled = true;
                TilePooler.DeactivateAllTilesForBoard(CurrentBoard.transform);
                GameObject.Destroy(CurrentBoard);
                CurrentBoard = NextBoard;
                CurrentBoard.name = "Board" + ProgressionManager.ActiveBoardNumber;
                Progression = Progression + 1;
            }
        }

        private void OffsetStoredPowerupIcon(Image storedPowerupIcon)
        {
            storedPowerupIcon.transform.position = new Vector2(storedPowerupIcon.transform.position.x, storedPowerupIcon.transform.position.y);
        }

        private void DestroyStoredPowerup()
        {
            if (storedPowerupIcon != null)
            {
                Destroy(storedPowerupIcon.gameObject);
                storedPowerupIcon = null;
            }
        }
    }
}