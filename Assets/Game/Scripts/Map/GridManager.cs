﻿using System.Collections.Generic;
using System.Linq;
using Assets.Game.Scripts.EnemyScripts;
using UnityEngine;

namespace Assets.Game.Scripts.Map
{
    public class GridManager : MonoBehaviour
    {
        public const int GridSizeX = 16, GridSizeY = 16;
        public const float TileRadius = 11f; // The radius (half of width) of one square tile in Unity world coordinates        
        public const float TileDiameter = TileRadius * 2;
        public const float Offset = 136f;
        public bool DisplayGridGizmos = false;
        [HideInInspector]
        public Dictionary<int, int[,]> MapDictionary;

        private Vector2 gridWorldSize;
        private Dictionary<int, int> walkableRegionsDictionary = new Dictionary<int, int>();
        private Node[,] grid;
        private TilePooler tilePooler;                
        private GameObject mostRecentBoardHolder;

        private int[,] map1 =
        {
            { 6, 5, 5, 5,  5, 5, 3, 3,  3, 3, 5, 5,  5, 5, 5, 6 },
            { 6, 0, 0, 0,  0, 0, 0, 0,  0, 0, 0, 0,  0, 0, 0, 7 },
            { 6, 0, 0, 0,  0, 0, 0, 0,  0, 0, 0, 0,  0, 0, 0, 6 },
            { 6, 0, 0, 0,  0, 0, 0, 0,  0, 0, 0, 0,  0, 0, 0, 6 },
            { 6, 0, 0, 0,  0, 0, 0, 0,  0, 0, 0, 0,  0, 0, 0, 6 },
            { 5, 0, 0, 0,  0, 0, 0, 8,  9, 10, 0, 0,  0, 0, 0, 5 },
            { 3, 0, 0, 0,  0, 0, 0, 11, 12, 13, 0, 0,  0, 0, 0, 3 },
            { 3, 0, 0, 0,  0, 0, 0, 0,  0, 0, 0, 0,  0, 0, 0, 3 },
            { 3, 0, 0, 0,  0, 0, 0, 0,  0, 0, 0, 0,  0, 0, 0, 3 },
            { 3, 0, 0, 0,  0, 0, 0, 0,  0, 0, 0, 0,  0, 0, 0, 3 },
            { 4, 0, 0, 0,  0, 0, 0, 0,  0, 0, 0, 0,  0, 0, 0, 4 },
            { 6, 0, 0, 0,  0, 0, 0, 0,  0, 0, 0, 0,  0, 0, 0, 6 },
            { 7, 0, 0, 0,  0, 0, 0, 0,  0, 0, 0, 0,  0, 0, 0, 6 },
            { 6, 0, 0, 0,  0, 0, 0, 0,  0, 0, 0, 0,  0, 0, 0, 6 },
            { 6, 0, 0, 0,  0, 0, 0, 0,  0, 0, 0, 0,  0, 0, 0, 7 },
            { 6, 4, 4, 4,  4, 4, 3, 3,  3, 3, 4, 4,  4, 4, 4, 6 }
        };

        private int[,] map2 =
        {
            { 6, 5, 5, 5,  5, 5, 3, 3,  3, 3, 5, 5,  5, 5, 5, 7 },
            { 6, 0, 0, 0,  0, 0, 0, 0,  0, 0, 0, 0,  0, 0, 0, 6 },
            { 6, 0, 0, 0,  0, 0, 0, 0,  0, 0, 0, 0,  0, 0, 0, 6 },
            { 6, 0, 0, 0,  0, 0, 0, 0,  0, 0, 0, 0,  0, 0, 0, 6 },
            { 6, 0, 0, 1,  1, 0, 0, 0,  0, 0, 0, 1,  1, 0, 0, 6 },
            { 5, 0, 0, 1,  0, 0, 0, 0,  0, 0, 0, 0,  1, 0, 0, 5 },
            { 3, 0, 0, 0,  0, 0, 0, 0,  0, 0, 0, 0,  0, 0, 0, 3 },
            { 3, 0, 0, 0,  0, 0, 0, 0,  0, 0, 0, 0,  0, 0, 0, 3 },
            { 3, 0, 0, 0,  0, 0, 0, 0,  0, 0, 0, 0,  0, 0, 0, 3 },
            { 4, 0, 0, 0,  0, 0, 0, 0,  0, 0, 0, 0,  0, 0, 0, 4 },
            { 6, 0, 0, 0,  0, 0, 0, 0,  0, 0, 0, 0,  0, 0, 0, 6 },
            { 6, 0, 0, 1,  0, 0, 0, 0,  0, 0, 0, 0,  1, 0, 0, 6 },
            { 6, 0, 0, 1,  1, 0, 0, 0,  0, 0, 0, 1,  1, 0, 0, 6 },
            { 7, 0, 0, 0,  0, 0, 0, 0,  0, 0, 0, 0,  0, 0, 0, 6 },
            { 6, 0, 0, 0,  0, 0, 0, 0,  0, 0, 0, 0,  0, 0, 0, 6 },
            { 6, 4, 4, 4,  4, 4, 4, 3,  3, 3, 4, 4,  4, 4, 4, 6 }
        };

        private int[,] map3 =
        {
            { 6, 6, 5, 5,  5, 5, 3, 3,  3, 3, 5, 5,  5, 5, 6, 6 },
            { 6, 5, 0, 0,  0, 0, 0, 0,  0, 0, 0, 0,  0, 0, 5, 6 },
            { 6, 0, 0, 0,  0, 0, 0, 0,  0, 0, 0, 0,  0, 0, 0, 6 },
            { 6, 0, 0, 0,  0, 0, 0, 0,  0, 0, 0, 0,  0, 0, 0, 6 },
            { 6, 0, 0, 0,  0, 0, 0, 0,  1, 0, 0, 0,  0, 0, 0, 6 },
            { 5, 0, 0, 0,  0, 0, 0, 0,  0, 0, 0, 0,  0, 0, 0, 5 },
            { 3, 0, 0, 0,  0, 0, 0, 0,  0, 0, 0, 0,  0, 0, 0, 3 },
            { 3, 0, 0, 0,  1, 0, 0, 0,  0, 0, 0, 0,  0, 0, 0, 3 },
            { 3, 0, 0, 0,  0, 0, 0, 0,  0, 0, 0, 1,  0, 0, 0, 3 },
            { 3, 0, 0, 0,  0, 0, 0, 0,  0, 0, 0, 0,  0, 0, 0, 3 },
            { 4, 0, 0, 0,  0, 0, 0, 0,  0, 0, 0, 0,  0, 0, 0, 4 },
            { 6, 0, 0, 0,  0, 0, 0, 1,  0, 0, 0, 0,  0, 0, 0, 6 },
            { 6, 0, 0, 0,  0, 0, 0, 0,  0, 0, 0, 0,  0, 0, 0, 6 },
            { 6, 0, 0, 0,  0, 0, 0, 0,  0, 0, 0, 0,  0, 0, 0, 6 },
            { 6, 0, 0, 0,  0, 0, 0, 0,  0, 0, 0, 0,  0, 0, 0, 6 },
            { 5, 4, 4, 4,  4, 4, 3, 3,  3, 4, 4, 4,  4, 4, 4, 5 }
        };

        private int[,] map4 =
{
            { 6, 6, 5, 5,  5, 5, 3, 3,  3, 5, 5, 5,  5, 5, 6, 6 },
            { 6, 5, 0, 0,  0, 0, 0, 0,  0, 0, 0, 0,  0, 0, 5, 6 },
            { 6, 0, 0, 0,  0, 0, 0, 0,  0, 0, 0, 0,  0, 0, 0, 7 },
            { 6, 0, 0, 0,  0, 0, 0, 0,  0, 0, 0, 0,  0, 0, 0, 6 },
            { 6, 0, 0, 0,  1, 1, 1, 0,  0, 1, 1, 1,  0, 0, 0, 6 },
            { 5, 0, 0, 0,  1, 0, 0, 0,  0, 0, 0, 1,  0, 0, 0, 5 },
            { 3, 0, 0, 0,  1, 0, 0, 0,  0, 0, 0, 1,  0, 0, 0, 3 },
            { 3, 0, 0, 0,  1, 0, 0, 0,  0, 0, 0, 1,  0, 0, 0, 3 },
            { 3, 0, 0, 0,  0, 0, 0, 0,  0, 0, 0, 0,  0, 0, 0, 3 },
            { 3, 0, 0, 0,  0, 0, 0, 0,  0, 0, 0, 0,  0, 0, 0, 3 },
            { 4, 0, 0, 0,  1, 0, 0, 0,  0, 0, 0, 1,  0, 0, 0, 4 },
            { 6, 0, 0, 0,  1, 0, 0, 0,  0, 0, 0, 1,  0, 0, 0, 6 },
            { 6, 0, 0, 0,  1, 1, 1, 0,  0, 1, 1, 1,  0, 0, 0, 6 },
            { 6, 0, 0, 0,  0, 0, 0, 0,  0, 0, 0, 0,  0, 0, 0, 6 },
            { 7, 0, 0, 0,  0, 0, 0, 0,  0, 0, 0, 0,  0, 0, 0, 6 },
            { 5, 3, 3, 3,  3, 3, 3, 3,  3, 3, 3, 3,  3, 3, 3, 5 }
        };

        // Roboboss river map
        private int[,] map5 =
        {
            { 3, 3, 3, 3,  3, 3, 3, 3,  3, 3, 3, 3,  3, 3, 3, 3 },
            { 3, 0, 0, 0,  0, 0, 0, 0,  0, 0, 0, 0,  0, 0, 0, 3 },
            { 3, 0, 0, 0,  0, 0, 0, 0,  0, 0, 0, 0,  0, 0, 0, 3 },
            { 3, 0, 0, 0,  0, 0, 0, 0,  0, 0, 0, 0,  0, 0, 0, 3 },
            { 3, 0, 0, 0,  0, 0, 0, 1,  0, 0, 0, 0,  0, 0, 0, 3 },
            { 3, 0, 0, 0,  0, 0, 0, 0,  0, 0, 0, 0,  0, 0, 0, 3 },
            { 3, 0, 0, 0,  0, 0, 0, 0,  0, 0, 0, 0,  0, 0, 0, 3 },
            { 3, 0, 0, 0,  0, 0, 0, 0,  0, 0, 0, 0,  0, 0, 0, 3 },
            { 2, 2, 2, 2,  2, 2, 2, 2,  2, 2, 2, 2,  2, 2, 2, 2 },
            { 3, 0, 0, 0,  0, 0, 0, 0,  0, 0, 0, 0,  0, 0, 0, 3 },
            { 3, 0, 0, 0,  0, 0, 0, 0,  0, 0, 0, 0,  0, 0, 0, 3 },
            { 3, 0, 0, 0,  0, 0, 0, 0,  0, 0, 0, 0,  0, 0, 0, 3 },
            { 3, 0, 0, 0,  0, 0, 1, 1,  1, 0, 0, 0, 19, 19, 19, 3 },
            { 3, 0, 0, 0,  0, 0, 0, 0,  0, 0, 0, 0, 19, 16, 19, 3 },
            { 3, 0, 0, 0,  0, 0, 0, 0,  0, 0, 0, 0, 19, 19, 19, 3 },
            { 3, 3, 3, 3,  3, 3, 3, 3,  3, 3, 3, 3,  3, 3, 3, 3 }
        };

        // LVL 2-1 Sewer
        private int[,] map6 =
        {
            { 18, 18, 17, 18,  18, 18, 18, 18,  18, 18, 18, 18,  18, 18, 17, 18 },
            { 28, 28, 28, 28,  28, 28, 28, 28,  28, 28, 28, 28,  28, 28, 28, 28 },
            { 18, 20, 20, 20,  20, 20, 20, 20,  20, 20, 20, 20,  20, 20, 20, 18 },
            { 18, 19, 19, 19,  19, 19, 19, 19,  19, 19, 19, 18,  45, 18, 19, 18 },
            { 19, 19, 19, 19,  19, 19, 19, 19,  19, 19, 19, 18,  45, 18, 19, 19 },
            { 21, 21, 27, 19,  19, 19, 19, 19,  19, 19, 19, 19,  19, 26, 21, 21 },
            { 18, 28, 22, 19,  19, 19, 19, 19,  19, 19, 19, 19,  19, 23, 28, 18 },
            { 18, 28, 22, 19,  19, 19, 19, 19,  19, 19, 19, 19,  19, 23, 28, 18 },
            { 18, 28, 22, 19,  19, 19, 19, 19,  19, 19, 19, 19,  19, 23, 28, 18 },
            { 18, 28, 22, 19,  19, 19, 19, 19,  19, 19, 19, 19,  19, 23, 28, 18 },
            { 18, 28, 22, 19,  19, 19, 19, 19,  19, 19, 19, 19,  19, 23, 28, 18 },
            { 18, 28, 22, 19,  19, 19, 19, 19,  19, 19, 19, 19,  19, 23, 28, 18 },
            { 18, 28, 22, 19,  19, 19, 19, 19,  19, 19, 19, 19,  19, 23, 28, 18 },
            { 20, 20, 24, 19,  19, 19, 19, 19,  19, 19, 19, 19,  19, 25, 20, 20 },
            { 19, 19, 19, 19,  19, 19, 19, 19,  19, 19, 19, 19,  19, 19, 19, 19 },
            { 18, 18, 18, 18,  18, 18, 18, 19,  19, 18, 18, 18,  18, 18, 18, 18 },
        };

        // LVL 2-2 Sewer
        private int[,] map7 =
        {
            { 18, 18, 18, 18,  18, 18, 18, 19,  19, 18, 18, 18,  18, 18, 18, 18 },
            { 19, 19, 19, 19,  19, 19, 19, 19,  19, 19, 19, 19,  19, 19, 19, 19 },
            { 19, 19, 19, 19,  19, 19, 19, 19,  19, 19, 19, 19,  19, 19, 19, 19 },
            { 21, 21, 21, 21,  21, 21, 21, 21,  21, 21, 21, 21,  21, 21, 21, 21 },
            { 28, 28, 28, 28,  28, 28, 28, 28,  28, 28, 28, 28,  28, 28, 28, 28 },
            { 20, 20, 20, 20,  20, 20, 20, 20,  20, 20, 20, 20,  20, 20, 20, 20 },
            { 19, 19, 19, 19,  19, 19, 19, 19,  19, 19, 19, 19,  19, 19, 19, 19 },
            { 19, 19, 19, 19,  19, 19, 19, 19,  19, 19, 19, 19,  19, 19, 19, 19 },
            { 19, 19, 19, 19,  19, 19, 19, 19,  19, 19, 19, 19,  19, 19, 19, 19 },
            { 19, 19, 19, 19,  19, 19, 19, 19,  19, 19, 19, 19,  19, 19, 19, 19 },
            { 21, 21, 21, 21,  21, 21, 21, 21,  21, 21, 21, 21,  21, 21, 21, 21 },
            { 28, 28, 28, 28,  28, 28, 28, 28,  28, 28, 28, 28,  28, 28, 28, 28 },
            { 20, 20, 20, 20,  20, 20, 20, 20,  20, 20, 20, 20,  20, 20, 20, 20 },
            { 19, 19, 19, 19,  19, 19, 19, 19,  19, 19, 19, 19,  19, 19, 19, 19 },
            { 19, 19, 19, 19,  19, 19, 19, 19,  19, 19, 19, 19,  19, 19, 19, 19 },
            { 18, 18, 18, 18,  18, 19, 19, 19,  19, 19, 19, 18,  18, 18, 18, 18 },
        };

        // LVL 2-3 Sewer
        private int[,] map8 =
        {
            { 18, 18, 18, 18,  18, 19, 19, 19,  19, 19, 19, 18,  18, 18, 18, 18 },
            { 18, 19, 19, 19,  19, 19, 19, 19,  19, 19, 19, 19,  19, 19, 19, 18 },
            { 18, 19, 19, 19,  19, 19, 19, 19,  19, 19, 19, 19,  19, 19, 19, 18 },
            { 18, 19, 19, 19,  19, 19, 19, 19,  19, 19, 19, 19,  19, 19, 19, 18 },
            { 19, 19, 19, 19,  19, 19, 19, 19,  19, 19, 19, 19,  19, 19, 19, 19 },
            { 19, 19, 19, 19,  19, 19, 19, 19,  19, 19, 19, 19,  19, 19, 19, 19 },
            { 19, 19, 19, 18,  19, 19, 19, 19,  19, 19, 19, 19,  18, 19, 19, 19 },
            { 19, 19, 26, 17,  27, 19, 19, 19,  19, 19, 19, 26,  17, 27, 19, 19 },
            { 19, 19, 23, 28,  22, 19, 19, 19,  19, 19, 19, 23,  28, 22, 19, 19 },
            { 19, 19, 23, 28,  22, 19, 19, 19,  19, 19, 19, 23,  28, 22, 19, 19 },
            { 19, 19, 25, 20,  24, 19, 19, 19,  19, 19, 19, 25,  20, 24, 19, 19 },
            { 19, 19, 19, 19,  19, 19, 19, 19,  19, 19, 19, 19,  19, 19, 19, 19 },
            { 18, 19, 19, 19,  19, 19, 19, 19,  19, 19, 19, 19,  19, 19, 19, 18 },
            { 18, 19, 19, 19,  19, 19, 19, 19,  19, 19, 19, 19,  19, 19, 19, 18 },
            { 18, 19, 19, 19,  19, 19, 19, 19,  19, 19, 19, 19,  19, 19, 19, 18 },
            { 18, 18, 18, 18,  18, 19, 19, 19,  19, 19, 19, 18,  18, 18, 18, 18 },
        };

        // LVL 2-4 Sewer
        private int[,] map9 =
        {
            { 17, 17, 22, 19,  19, 19, 19, 19,  19, 19, 19, 19,  19, 23, 17, 17 },
            { 28, 28, 22, 19,  19, 19, 19, 19,  19, 19, 19, 19,  19, 23, 28, 28 },
            { 28, 28, 22, 19,  19, 19, 19, 19,  19, 19, 19, 19,  19, 23, 28, 28 },
            { 28, 28, 22, 19,  19, 19, 19, 19,  19, 19, 19, 19,  19, 23, 28, 28 },
            { 28, 28, 22, 19,  19, 19, 19, 19,  19, 19, 19, 19,  19, 23, 28, 28 },
            { 28, 28, 22, 19,  19, 19, 19, 19,  19, 19, 19, 19,  19, 23, 28, 28 },
            { 28, 28, 22, 19,  19, 19, 19, 19,  19, 19, 19, 19,  19, 23, 28, 28 },
            { 28, 28, 22, 19,  19, 19, 19, 19,  19, 19, 19, 19,  19, 23, 28, 28 },
            { 28, 28, 22, 19,  19, 19, 19, 19,  19, 19, 19, 19,  19, 23, 28, 28 },
            { 28, 28, 22, 19,  19, 19, 19, 19,  19, 19, 19, 19,  19, 23, 28, 28 },
            { 28, 28, 22, 19,  19, 19, 19, 19,  19, 19, 19, 19,  19, 23, 28, 28 },
            { 28, 28, 22, 19,  19, 19, 19, 19,  19, 19, 19, 19,  19, 23, 28, 28 },
            { 28, 28, 22, 19,  19, 19, 19, 19,  19, 19, 19, 19,  19, 23, 28, 28 },
            { 28, 28, 22, 19,  19, 19, 19, 19,  19, 19, 19, 19,  19, 23, 28, 28 },
            { 28, 28, 22, 19,  19, 19, 19, 19,  19, 19, 19, 19,  19, 23, 28, 28 },
            { 28, 28, 22, 19,  19, 19, 19, 19,  19, 19, 19, 19,  19, 23, 28, 28 },
        };

        // LVL 2-5 Sewer (Roboboss2)
        private int[,] map10 =
        {
            { 19, 19, 19, 19,  19, 19, 19, 19,  19, 19, 19, 19,  19, 19, 19, 19 },
            { 19, 19, 19, 19,  19, 19, 19, 19,  19, 19, 19, 19,  19, 19, 19, 19 },
            { 19, 19, 19, 19,  19, 19, 19, 19,  19, 19, 19, 19,  19, 19, 19, 19 },
            { 19, 19, 19, 19,  19, 19, 19, 19,  19, 19, 19, 19,  19, 19, 19, 19 },
            { 19, 19, 19, 19,  19, 19, 19, 18,  19, 19, 19, 19,  19, 19, 19, 19 },
            { 19, 19, 19, 19,  19, 19, 19, 19,  19, 19, 19, 19,  19, 19, 19, 19 },
            { 19, 19, 19, 19,  19, 19, 19, 19,  19, 19, 19, 19,  19, 19, 19, 19 },
            { 19, 19, 19, 19,  19, 19, 19, 19,  19, 19, 19, 19,  19, 19, 19, 19 },
            { 28, 28, 28, 28,  28, 28, 28, 28,  28, 28, 28, 28,  28, 28, 28, 28 },
            { 19, 19, 19, 19,  19, 19, 19, 19,  19, 19, 19, 19,  19, 19, 19, 19 },
            { 19, 19, 19, 19,  19, 19, 19, 19,  19, 19, 19, 19,  19, 19, 19, 19 },
            { 19, 19, 19, 19,  19, 19, 19, 19,  19, 19, 19, 19,  19, 19, 19, 19 },
            { 19, 19, 19, 19,  19, 19, 18, 18,  18, 19, 19, 19,  19, 19, 19, 19 },
            { 19, 19, 19, 19,  19, 19, 19, 19,  19, 19, 19, 19,  19, 16, 19, 19 },
            { 19, 19, 19, 19,  19, 19, 19, 19,  19, 19, 19, 19,  19, 19, 19, 19 },
            { 19, 19, 19, 19,  19, 19, 19, 19,  19, 19, 19, 19,  19, 19, 19, 19 },
        };

        // LVL 3-1 Lab
        private int[,] map11 =
        {
            { 41, 41, 41, 41,  29, 29, 29, 29,  29, 29, 29, 29,  41, 46, 41, 41 },
            { 41, 41, 41, 41,  29, 29, 29, 29,  29, 29, 29, 29,  41, 46, 41, 41 },
            { 41, 29, 29, 29,  29, 29, 29, 29,  29, 29, 29, 29,  29, 29, 29, 41 },
            { 41, 29, 29, 29,  29, 29, 29, 29,  29, 29, 29, 29,  29, 29, 29, 41 },
            { 29, 29, 29, 43,  29, 29, 29, 29,  29, 29, 29, 29,  43, 29, 29, 29 },
            { 29, 29, 29, 29,  29, 38, 38, 38,  38, 38, 38, 29,  29, 29, 29, 29 },
            { 29, 29, 29, 29,  29, 38, 40, 40,  40, 40, 38, 29,  29, 29, 29, 29 },
            { 29, 29, 29, 29,  29, 38, 47, 47,  47, 47, 38, 29,  29, 29, 29, 29 },
            { 29, 29, 29, 29,  29, 38, 47, 47,  47, 47, 38, 29,  29, 29, 29, 29 },
            { 29, 29, 29, 29,  29, 38, 47, 47,  47, 47, 38, 29,  29, 29, 29, 29 },
            { 29, 29, 29, 29,  29, 38, 38, 38,  38, 38, 38, 29,  29, 29, 29, 29 },
            { 29, 29, 29, 43,  29, 29, 36, 29,  29, 36, 29, 29,  43, 29, 29, 29 },
            { 41, 29, 29, 29,  29, 29, 36, 29,  29, 36, 29, 29,  29, 29, 29, 41 },
            { 41, 29, 29, 29,  29, 29, 36, 29,  29, 36, 29, 29,  29, 29, 29, 41 },
            { 41, 29, 29, 29,  29, 29, 36, 29,  29, 36, 29, 29,  29, 29, 29, 41 },
            { 41, 41, 41, 41,  29, 29, 36, 29,  29, 36, 29, 29,  41, 41, 41, 41 },
        };

        // LVL 3-2 Lab
        private int[,] map12 =
        {
            { 41, 41, 41, 41,  29, 29, 36, 29,  29, 36, 29, 29,  41, 41, 41, 41 },
            { 29, 29, 29, 29,  29, 29, 36, 29,  29, 36, 29, 29,  29, 29, 29, 29 },
            { 29, 29, 29, 29,  29, 29, 36, 31,  31, 36, 29, 29,  29, 29, 29, 29 },
            { 29, 38, 38, 38,  38, 38, 36, 31,  31, 36, 38, 38,  38, 38, 38, 29 },
            { 29, 38, 40, 40,  40, 38, 36, 29,  29, 36, 38, 40,  40, 40, 38, 29 },
            { 29, 38, 47, 47,  47, 38, 36, 29,  29, 36, 38, 47,  47, 47, 38, 29 },
            { 29, 38, 38, 38,  38, 38, 36, 29,  29, 36, 38, 38,  38, 38, 38, 29 },
            { 29, 43, 29, 29,  29, 29, 36, 29,  29, 36, 29, 29,  29, 29, 43, 29 },
            { 29, 43, 29, 29,  29, 29, 36, 29,  29, 36, 29, 29,  29, 29, 43, 29 },
            { 29, 38, 38, 38,  38, 38, 36, 29,  29, 36, 38, 38,  38, 38, 38, 29 },
            { 29, 38, 40, 40,  40, 38, 36, 29,  29, 36, 38, 40,  40, 40, 38, 29 },
            { 29, 38, 47, 47,  47, 38, 36, 29,  29, 36, 38, 47,  47, 47, 38, 29 },      
            { 29, 38, 38, 38,  38, 38, 36, 31,  31, 36, 38, 38,  38, 38, 38, 29 },
            { 29, 29, 29, 29,  29, 29, 36, 31,  31, 36, 29, 29,  29, 29, 29, 29 },
            { 29, 29, 29, 29,  29, 29, 36, 29,  29, 36, 29, 29,  29, 29, 29, 29 },
            { 41, 41, 41, 41,  41, 41, 36, 29,  29, 36, 41, 41,  41, 41, 41, 41 },
        };

        // LVL 3-3 Lab
        private int[,] map13 =
        {
            { 41, 41, 41, 41,  29, 29, 36, 29,  29, 36, 29, 29,  41, 41, 41, 41 },
            { 29, 50, 29, 50,  29, 29, 36, 29,  29, 36, 29, 29,  50, 29, 50, 29 },
            { 29, 29, 29, 29,  29, 29, 36, 29,  29, 36, 29, 29,  29, 29, 29, 29 },
            { 29, 50, 29, 29,  29, 29, 36, 29,  29, 36, 29, 29,  29, 29, 50, 29 },
            { 29, 29, 29, 29,  42, 42, 42, 29,  29, 42, 42, 42,  29, 29, 29, 29 },
            { 29, 29, 29, 29,  48, 29, 29, 29,  29, 29, 29, 48,  29, 29, 29, 29 },
            { 29, 50, 29, 29,  48, 29, 29, 29,  29, 29, 29, 48,  29, 29, 50, 29 },
            { 29, 29, 29, 29,  29, 29, 29, 29,  29, 29, 29, 29,  29, 29, 29, 29 },
            { 29, 29, 29, 29,  29, 29, 29, 29,  29, 29, 29, 29,  29, 29, 29, 29 },
            { 29, 50, 29, 29,  42, 29, 29, 29,  29, 29, 29, 42,  29, 29, 50, 29 },
            { 29, 29, 29, 29,  48, 29, 29, 29,  29, 29, 29, 48,  29, 29, 29, 29 },
            { 29, 29, 29, 29,  48, 42, 42, 29,  29, 42, 42, 48,  29, 29, 29, 29 },
            { 29, 50, 29, 29,  29, 29, 29, 29,  29, 29, 29, 29,  29, 29, 50, 29 },
            { 29, 29, 29, 29,  29, 29, 29, 29,  29, 29, 29, 29,  29, 29, 29, 29 },
            { 40, 40, 40, 40,  40, 40, 29, 29,  29, 29, 40, 40,  40, 40, 40, 40 },
            { 47, 47, 47, 47,  47, 47, 29, 29,  29, 29, 47, 47,  47, 47, 47, 47 },
        };

        // LVL 3-4 Lab
        private int[,] map14 =
        {
            { 47, 47, 47, 47,  29, 29, 29, 29,  29, 29, 29, 29,  47, 47, 47, 47 },
            { 47, 29, 29, 29,  29, 29, 29, 29,  29, 29, 29, 29,  29, 29, 29, 47 },
            { 47, 29, 49, 29,  29, 29, 29, 29,  29, 29, 29, 29,  29, 49, 29, 47 },
            { 47, 29, 29, 29,  29, 29, 29, 29,  29, 29, 29, 29,  29, 29, 29, 47 },
            { 29, 29, 29, 43,  29, 29, 29, 29,  29, 29, 29, 29,  43, 29, 29, 29 },
            { 29, 29, 29, 29,  29, 29, 29, 29,  29, 29, 29, 29,  29, 29, 29, 29 },
            { 29, 49, 29, 29,  43, 29, 29, 29,  29, 29, 29, 43,  29, 29, 49, 29 },
            { 29, 29, 29, 29,  29, 29, 29, 29,  29, 29, 29, 29,  29, 29, 29, 29 },
            { 29, 29, 29, 29,  29, 29, 29, 29,  29, 29, 29, 29,  29, 29, 29, 29 },
            { 29, 49, 29, 29,  43, 29, 29, 29,  29, 29, 29, 43,  29, 29, 49, 29 },
            { 29, 29, 29, 29,  29, 29, 29, 29,  29, 29, 29, 29,  29, 29, 29, 29 },
            { 29, 29, 29, 43,  29, 29, 29, 29,  29, 29, 29, 29,  43, 29, 29, 29 },
            { 40, 29, 29, 29,  29, 29, 29, 29,  29, 29, 29, 29,  29, 29, 29, 40 },
            { 47, 29, 49, 29,  29, 29, 29, 29,  29, 29, 29, 29,  29, 49, 29, 47 },
            { 47, 29, 29, 29,  29, 29, 29, 29,  29, 29, 29, 29,  29, 29, 29, 47 },
            { 47, 40, 40, 40,  29, 29, 29, 29,  29, 29, 29, 29,  40, 40, 40, 47 },
        };

        // LVL 3-5 Lab (Brain)
        private int[,] map15 =
        {
            { 31, 31, 31, 31,  43, 31, 31, 31,  31, 31, 31, 43,  31, 31, 31, 31 },
            { 31, 29, 29, 29,  29, 29, 29, 29,  29, 29, 29, 29,  29, 29, 29, 31 },
            { 31, 29, 29, 29,  29, 29, 29, 29,  29, 29, 29, 29,  29, 29, 29, 31 },
            { 31, 29, 29, 29,  29, 29, 29, 29,  29, 29, 29, 29,  29, 29, 29, 31 },
            { 43, 29, 29, 29,  29, 29, 29, 29,  29, 29, 29, 29,  29, 29, 29, 43 },
            { 31, 29, 29, 29,  29, 29, 29, 29,  29, 29, 29, 29,  29, 29, 29, 31 },
            { 31, 29, 29, 29,  29, 29, 29, 29,  29, 29, 29, 29,  29, 29, 29, 31 },
            { 31, 29, 29, 29,  29, 29, 29, 29,  29, 29, 29, 29,  29, 29, 29, 31 },
            { 31, 29, 29, 29,  29, 29, 29, 29,  29, 29, 29, 29,  29, 29, 29, 31 },
            { 31, 29, 29, 29,  29, 29, 29, 29,  29, 29, 29, 29,  29, 29, 29, 31 },
            { 31, 29, 29, 29,  29, 29, 29, 29,  29, 29, 29, 29,  29, 29, 29, 31 },
            { 43, 29, 29, 29,  29, 29, 29, 29,  29, 29, 29, 29,  29, 29, 29, 43 },
            { 31, 29, 29, 29,  29, 29, 29, 29,  29, 29, 29, 29,  29, 29, 29, 31 },
            { 31, 29, 29, 29,  29, 29, 29, 29,  29, 29, 29, 29,  29, 29, 29, 31 },
            { 31, 29, 29, 29,  29, 29, 29, 29,  29, 29, 29, 29,  29, 29, 29, 31 },
            { 31, 31, 31, 31,  43, 31, 31, 31,  31, 31, 31, 43,  31, 31, 31, 31 },
        };

        // Ending
        private int[,] map16 =
        {
            { 0, 0, 0, 0,  5, 4, 0, 0,  0, 0, 5, 0,  0, 0, 0, 4 },
            { 4, 0, 4, 0,  0, 5, 0, 0,  0, 0, 0, 0,  0, 0, 0, 5 },
            { 5, 0, 5, 0,  0, 0, 4, 0,  0, 0, 0, 0,  3, 0, 0, 0 },
            { 0, 4, 4, 0,  0, 0, 5, 0,  0, 4, 0, 0,  0, 0, 4, 0 },
            { 3, 5, 5, 0,  4, 0, 0, 0,  0, 5, 0, 0,  4, 0, 5, 4 },
            { 4, 0, 0, 0,  5, 0, 0, 0,  4, 3, 0, 0,  5, 0, 0, 5 },
            { 5, 0, 0, 0,  0, 0, 0, 0,  5, 0, 0, 0,  0, 0, 0, 3 },
            { 51, 51, 51, 51,  51, 51, 51, 51,  51, 51, 51, 51,  51, 51, 51, 51 },
            { 52, 52, 52, 52,  52, 52, 52, 52,  52, 52, 52, 52,  52, 52, 52, 52 },
            { 4, 0, 0, 3,  0, 4, 0, 0,  0, 0, 0, 0,  4, 0, 4, 0 },
            { 5, 0, 3, 0,  0, 5, 0, 0,  4, 0, 0, 0,  5, 0, 5, 4 },
            { 0, 0, 0, 4,  3, 0, 0, 0,  5, 0, 4, 0,  0, 4, 0, 5 },
            { 0, 0, 4, 5,  0, 0, 0, 0,  0, 4, 5, 0,  0, 5, 0, 0 },
            { 3, 0, 5, 0,  0, 4, 0, 0,  0, 5, 0, 4,  0, 0, 0, 0 },
            { 0, 0, 0, 0,  0, 5, 0, 3,  3, 0, 0, 5,  0, 0, 0, 4 },
            { 0, 4, 0, 0,  4, 0, 0, 0,  0, 3, 0, 0,  0, 4, 0, 5 }
        };

        public int MaxSize
        {
            get
            {
                return GridSizeX * GridSizeY;
            }
        }

        public Vector3 RandomPosition()
        {
            int randomXIndex = Random.Range(0, GridSizeX);
            int randomYIndex = Random.Range(0, GridSizeY);
            return new Vector3((randomXIndex * TileDiameter) - Offset, (randomYIndex * TileDiameter) - Offset, 0f);
        }

        public List<Node> GetUnblockedBorderNodes()
        {
            List<Node> unblockedBorderNodes = new List<Node>();
            for (int x = 0; x < GridSizeX; x++)
            {
                for (int y = 0; y < GridSizeY; y++)
                {
                    if (grid[x, y].Walkable)
                    {
                        if (y == 0 || x == 0 || x == GridSizeX - 1 || y == GridSizeY - 1)
                        {
                            unblockedBorderNodes.Add(grid[x, y]);
                        }
                    }
                }
            }

            return unblockedBorderNodes;
        }

        public List<Node> GetUnblockedLeftBorderNodes()
        {
            var result = new List<Node>();
            for (int x = 0; x < GridSizeX; x++)
            {
                for (int y = 0; y < GridSizeY; y++)
                {
                    if (grid[x, y].Walkable)
                    {
                        if (x == 0)
                        {
                            result.Add(grid[x, y]);
                        }
                    }
                }
            }

            return result;
        }

        public List<Node> GetUnblockedRightBorderNodes()
        {
            var result = new List<Node>();
            for (int x = 0; x < GridSizeX; x++)
            {
                for (int y = 0; y < GridSizeY; y++)
                {
                    if (grid[x, y].Walkable)
                    {
                        if (x == GridSizeX - 1)
                        {
                            result.Add(grid[x, y]);
                        }
                    }
                }
            }

            return result;
        }

        public List<Node> GetUnblockedBottomBorderNodes()
        {
            var result = new List<Node>();
            for (int x = 0; x < GridSizeX; x++)
            {
                for (int y = 0; y < GridSizeY; y++)
                {
                    if (grid[x, y].Walkable)
                    {
                        if (y == 0)
                        {
                            result.Add(grid[x, y]);
                        }
                    }
                }
            }

            return result;
        }

        public List<Node> GetUnblockedTopBorderNodes()
        {
            var result = new List<Node>();
            for (int x = 0; x < GridSizeX; x++)
            {
                for (int y = 0; y < GridSizeY; y++)
                {
                    if (grid[x, y].Walkable)
                    {
                        if (y == GridSizeY - 1)
                        {
                            result.Add(grid[x, y]);
                        }
                    }
                }
            }

            return result;
        }

        public List<Node> GetUnblockedTopAndBottomBorderNodes()
        {
            List<Node> unblockedBorderNodes = new List<Node>();
            for (int x = 0; x < GridSizeX; x++)
            {
                for (int y = 0; y < GridSizeY; y++)
                {
                    if (grid[x, y].Walkable)
                    {
                        if (y == 0 || y == GridSizeY - 1)
                        {
                            unblockedBorderNodes.Add(grid[x, y]);
                        }
                    }
                }
            }

            return unblockedBorderNodes;
        }

        public List<Node> GetUnblockedLeftAndRightBorderNodes()
        {
            List<Node> unblockedBorderNodes = new List<Node>();
            for (int x = 0; x < GridSizeX; x++)
            {
                for (int y = 0; y < GridSizeY; y++)
                {
                    if (grid[x, y].Walkable)
                    {
                        if (x == 0 || x == GridSizeX - 1)
                        {
                            unblockedBorderNodes.Add(grid[x, y]);
                        }
                    }
                }
            }

            return unblockedBorderNodes;
        }

        public List<Node> GetAllBorderNodes()
        {
            List<Node> allBorderNodes = new List<Node>();
            for (int x = 0; x < GridSizeX; x++)
            {
                for (int y = 0; y < GridSizeY; y++)
                {
                    if (y == 0 || x == 0 || x == GridSizeX - 1 || y == GridSizeY - 1)
                    {
                        allBorderNodes.Add(grid[x, y]);
                    }
                }
            }

            return allBorderNodes;
        }

        public Vector2 GetWorldCoordinatesFromNodeCoordinates(Vector2 nodeCoordinates)
        {
            return grid[(int)nodeCoordinates.x, GridSizeY - (int)nodeCoordinates.y - 1].WorldPosition;
        }

        public List<Node> GetUnblockedCenterNodes()
        {
            List<Node> unblockedCenterNodes = new List<Node>();
            for (int x = 0; x < GridSizeX; x++)
            {
                for (int y = 0; y < GridSizeY; y++)
                {
                    if (grid[x, y].Walkable)
                    {
                        if (y > 3 && y < GridSizeY - 3 && x > 3 && x < GridSizeX - 3)
                        {
                            unblockedCenterNodes.Add(grid[x, y]);
                        }
                    }
                }
            }

            return unblockedCenterNodes;
        }

        public void RandomChancePowerupSpawnAtWorldCoordinate(Vector2 worldCoordinate)
        {
            int random = Random.Range(0, 1000);

            // Don't spawn anything if less than this number
            if (random > 810) 
            {
                ObjectPooler.Instance.GetPooledObjectAndMoveTo(GetRandomPowerupBasedOnDropChance(random), worldCoordinate);
            }
        }        

        public List<Node> GetNeighbours(Node node)
        {
            List<Node> neighbours = new List<Node>();

            for (int x = -1; x <= 1; x++)
            {
                for (int y = -1; y <= 1; y++)
                {
                    if (x == 0 && y == 0)
                    {
                        continue;
                    }

                    int checkX = node.GridX + x;
                    int checkY = node.GridY + y;

                    if (checkX >= 0 && checkX < GridSizeX && checkY >= 0 && checkY < GridSizeY)
                    {
                        neighbours.Add(grid[checkX, checkY]);
                    }
                }
            }

            return neighbours;
        }

        public Node NodeFromWorldPoint(Vector2 worldPosition)
        {
            Vector2 offsetWorldPosition = new Vector2(
                worldPosition.x - GameManager.GameAreaOffset,
                worldPosition.y - GameManager.GameAreaOffset);
            float percentX = (offsetWorldPosition.x + (gridWorldSize.x / 2)) / gridWorldSize.x;
            float percentY = (offsetWorldPosition.y + (gridWorldSize.y / 2)) / gridWorldSize.y;
            percentX = Mathf.Clamp01(percentX);
            percentY = Mathf.Clamp01(percentY);

            int x = Mathf.RoundToInt((GridSizeX - 1) * percentX);
            int y = Mathf.RoundToInt((GridSizeY - 1) * percentY);
            return grid[x, y];
        }

        public void SetWalkablity(Vector2 gridCoordinate, bool newWalkability)
        {
            grid[(int)gridCoordinate.x, GridSizeY - (int)gridCoordinate.y - 1].Walkable = newWalkability;
        }

        public void CreateBridgeTile(Vector2 gridCoord)
        {
            // Get the tile object for the grid coord
            Vector2 tilePositionInWorldCoords = GetWorldCoordinatesFromNodeCoordinates(gridCoord);
            
            // Create the new tile and set walkability
            GameObject bridge = tilePooler.GetPooledTileAndMoveTo(TilePooler.TileType.Bridge, new Vector3(tilePositionInWorldCoords.x, tilePositionInWorldCoords.y, 0f), mostRecentBoardHolder.transform);
            SetWalkablity(gridCoord, true);

            foreach (Transform t in mostRecentBoardHolder.GetComponentInChildren<Transform>())
            {
                // Disable colliders for tiles close to the new bridge
                if (Vector3.Distance(t.position, bridge.transform.position) < TileRadius)
                {
                    foreach (EdgeCollider2D e in t.gameObject.GetComponents<EdgeCollider2D>())
                    {
                        // Disable all edge colliders to allow the player to walk over it
                        e.enabled = false;
                    }

                    break;
                }
            }
        }

        public GameObject BoardSetup(int mapNumber, bool appendToBottomOfCurrentBoard = false)
        {
            float verticalOffset = Offset;
            if (appendToBottomOfCurrentBoard)
            {
                verticalOffset = ((Offset + TileDiameter) * 3f) + TileRadius; // 487? Not sure why this is
            }

            var boardName = "Board" + mapNumber.ToString();
            var map = MapDictionary[mapNumber];
            CreatePathfindingGrid(map);
            mostRecentBoardHolder = new GameObject(boardName);
            for (int x = 0; x < GridSizeX; x++)
            {
                for (int y = 0; y < GridSizeY; y++)
                {
                    int tempTileType = map[GridSizeY - y - 1, x];
                    tilePooler.GetPooledTileAndMoveTo(
                        ConvertMapIntegerToTileType(tempTileType),
                        new Vector3((x * TileDiameter) - Offset, (y * TileDiameter) - verticalOffset, 0f), 
                        mostRecentBoardHolder.transform);
                }
            }

            return mostRecentBoardHolder;
        }

        public Vector3 RandomPosition(float offset)
        {
            int randomXIndex = Random.Range(0, GridSizeX);
            int randomYIndex = Random.Range(0, GridSizeY);
            return new Vector3((randomXIndex * TileDiameter) - offset, (randomYIndex * TileDiameter) - offset, 0f);
        }

        private bool GetDefaultTileWalkability(int tileNumber)
        {
            switch (tileNumber)
            {
                case 0:
                case 3:
                case 19: // Concrete tiles
                case 20:
                case 21:
                case 22:
                case 23:
                case 24:
                case 25:
                case 26:
                case 27:
                case 29:
                case 30:
                case 31:
                case 32:
                case 33:
                case 34:
                case 35:
                case 36:
                case 37:
                case 38:
                case 39:
                case 43:
                case 44:
                case 51:
                case 52:
                    return true;
                default:
                    return false;
            }
        }

        private ObjectPooler.ObjectType GetRandomPowerupBasedOnDropChance(int random)
        {
            ObjectPooler.ObjectType[] droppablePowerups = ObjectPooler.Instance.GetDroppablePowerupsArray();
            if (random > 900)
            {
                return droppablePowerups.Where(x => x == ObjectPooler.ObjectType.Powerup_Dollar).First();
            }
            else if (random > 885)
            {
                return droppablePowerups.Where(x => x == ObjectPooler.ObjectType.Powerup_Shotgun).First();
            }
            else if (random > 870)
            {
                return droppablePowerups.Where(x => x == ObjectPooler.ObjectType.Powerup_MachineGun).First();
            }
            else if (random > 855)
            {
                return droppablePowerups.Where(x => x == ObjectPooler.ObjectType.Powerup_Grenade).First();
            }
            else if (random > 840)
            {
                return droppablePowerups.Where(x => x == ObjectPooler.ObjectType.Powerup_Circle).First();
            }
            else if (random > 825)
            {
                return droppablePowerups.Where(x => x == ObjectPooler.ObjectType.Powerup_Medal).First();
            }
            else
            {
                return droppablePowerups.Where(x => x == ObjectPooler.ObjectType.Powerup_Helmet).First();
            }
        }

        private void CreatePathfindingGrid(int[,] map)
        {
            gridWorldSize = new Vector2(GridSizeX * TileDiameter, GridSizeY * TileDiameter);
            grid = new Node[GridSizeX, GridSizeY];
            Vector2 worldBottomLeft = new Vector2(transform.position.x + GameManager.GameAreaOffset, transform.position.y + GameManager.GameAreaOffset) - Vector2.right * gridWorldSize.x / 2 - Vector2.up * gridWorldSize.y / 2;
            bool walkable;

            for (int x = 0; x < GridSizeX; x++)
            {
                for (int y = 0; y < GridSizeY; y++)
                {
                    // Set the location of each pathfinding node in world coordinates
                    Vector2 worldPoint = worldBottomLeft + Vector2.right * (x * TileDiameter + TileRadius) + Vector2.up * (y * TileDiameter + TileRadius);

                    // Set walkable based on map number
                    walkable = GetDefaultTileWalkability(map[GridSizeY - 1 - y, x]);
                    int movementPenalty = 0;

                    if (walkable)
                    {
                        Ray ray = new Ray(new Vector3(worldPoint.x, worldPoint.y, 0) + Vector3.back * 50, Vector3.forward);
                        RaycastHit hit;
                        if (Physics.Raycast(ray, out hit, 100))
                        {
                            walkableRegionsDictionary.TryGetValue(hit.collider.gameObject.layer, out movementPenalty);
                        }
                    }

                    grid[x, y] = new Node(walkable, worldPoint, x, y, movementPenalty);
                }
            }
        }

        private void OnDrawGizmos()
        {
            // Gizmos.DrawWireCube(transform.position, new Vector2(gridWorldSize.x, gridWorldSize.y));
            if (grid != null && DisplayGridGizmos)
            {
                foreach (Node n in grid)
                {
                    if (!n.Walkable)
                    {
                        Gizmos.color = Color.red;
                    }
                    else
                    {
                        Gizmos.color = Color.white;
                    }

                    Gizmos.DrawCube(n.WorldPosition, Vector3.one * (TileDiameter - (TileDiameter / 10)));
                }
            }
        }

        private void Awake()
        {
            tilePooler = GameObject.FindObjectOfType<TilePooler>();
            MapDictionary = new Dictionary<int, int[,]>();
            MapDictionary.Add(1, map1);
            MapDictionary.Add(2, map2);
            MapDictionary.Add(3, map3);
            MapDictionary.Add(4, map4);
            MapDictionary.Add(5, map5);
            MapDictionary.Add(6, map6);
            MapDictionary.Add(7, map7);
            MapDictionary.Add(8, map8);
            MapDictionary.Add(9, map9);
            MapDictionary.Add(10, map10);
            MapDictionary.Add(11, map11);
            MapDictionary.Add(12, map12);
            MapDictionary.Add(13, map13);
            MapDictionary.Add(14, map14);
            MapDictionary.Add(15, map15);
            MapDictionary.Add(16, map16);
        }

        private TilePooler.TileType ConvertMapIntegerToTileType(int mapInteger)
        {
            switch (mapInteger)
            {
                case 0:
                    return TilePooler.TileType.Grass;
                case 1:
                    return TilePooler.TileType.Wall;
                case 2:
                    return TilePooler.TileType.River;
                case 3:
                    return TilePooler.TileType.Grass3;
                case 4:
                    return TilePooler.TileType.TreeTop;
                case 5:
                    return TilePooler.TileType.TreeBottom;
                case 6:
                    return TilePooler.TileType.TreeOverlap;
                case 7:
                    return TilePooler.TileType.BurningTree;
                case 8:
                    return TilePooler.TileType.CarTopLeft;
                case 9:
                    return TilePooler.TileType.CarTop;
                case 10:
                    return TilePooler.TileType.CarTopRight;
                case 11:
                    return TilePooler.TileType.CarBottomLeft;
                case 12:
                    return TilePooler.TileType.CarBottom;
                case 13:
                    return TilePooler.TileType.CarBottomRight;
                case 14:
                    return TilePooler.TileType.CarBottomRight;
                case 15:
                    return TilePooler.TileType.Bridge;
                case 16:
                    return TilePooler.TileType.Manhole;
                case 17:
                    return TilePooler.TileType.SewerPipe;
                case 18:
                    return TilePooler.TileType.Brick;
                case 19:
                    return TilePooler.TileType.Concrete;
                case 20:
                    return TilePooler.TileType.ConcreteTopBorder;
                case 21:
                    return TilePooler.TileType.ConcreteBottomBorder;
                case 22:
                    return TilePooler.TileType.ConcreteLeftBorder;
                case 23:
                    return TilePooler.TileType.ConcreteRightBorder;
                case 24:
                    return TilePooler.TileType.ConcreteTopLeftBorder;
                case 25:
                    return TilePooler.TileType.ConcreteTopRightBorder;
                case 26:
                    return TilePooler.TileType.ConcreteBottomRightBorder;
                case 27:
                    return TilePooler.TileType.ConcreteBottomLeftBorder;
                case 28:
                    return TilePooler.TileType.SewerWater;
                case 29:
                    return TilePooler.TileType.Metal1;
                case 30:
                    return TilePooler.TileType.Metal2;
                case 31:
                    return TilePooler.TileType.Metal3;
                case 32:
                    return TilePooler.TileType.Metal4;
                case 33:
                    return TilePooler.TileType.Metal5;
                case 34:
                    return TilePooler.TileType.Metal6;
                case 35:
                    return TilePooler.TileType.Metal7;
                case 36:
                    return TilePooler.TileType.Metal8;
                case 37:
                    return TilePooler.TileType.Metal9;
                case 38:
                    return TilePooler.TileType.Metal10;
                case 39:
                    return TilePooler.TileType.Metal11;
                case 40:
                    return TilePooler.TileType.MetalHole;
                case 41:
                    return TilePooler.TileType.MetalWall;
                case 42:
                    return TilePooler.TileType.RedEyes;
                case 43:
                    return TilePooler.TileType.RedLight;
                case 44:
                    return TilePooler.TileType.Data;
                case 45:
                    return TilePooler.TileType.SewerLadder;
                case 46:
                    return TilePooler.TileType.LabLadder;
                case 47:
                    return TilePooler.TileType.Chasm;
                case 48:
                    return TilePooler.TileType.RedEyesChasm;
                case 49:
                    return TilePooler.TileType.BrainJar;
                case 50:
                    return TilePooler.TileType.InactiveRobot;
                case 51:
                    return TilePooler.TileType.RoadTop;
                case 52:
                    return TilePooler.TileType.RoadBottom;
                default:
                    throw new System.Exception("Tile map integer " + mapInteger + " was not defined.");
            }
        }
    }
}