﻿using System;
using UnityEngine;

namespace Assets.Game.Scripts.Map
{
    public class Helper
    {
        public static Vector2 ConvertWorldCoordToGridCoord(Vector2 worldCoord)
        {
            Vector2 gridCoord = new Vector2((float)Math.Round((worldCoord.x - GameManager.GameAreaOffset) / GridManager.TileDiameter, 0), (float)Math.Round(((worldCoord.y * -1) - GameManager.GameAreaOffset) / GridManager.TileDiameter, 0));
            if (gridCoord.x < 0 || gridCoord.y < 0 || gridCoord.x > GridManager.GridSizeX - 1 || gridCoord.y > GridManager.GridSizeY - 1)
            {
                throw new Exception("Helper.ConvertWorldCoordToGridCoord world coord parameter is out of bounds");
            }

            return gridCoord;
        }
    }
}