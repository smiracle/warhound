﻿using Assets.Game.Scripts.Map;
using Assets.Game.Scripts.PlayerScripts;
using UnityEngine;

public class Manhole : MonoBehaviour
{
    private bool doOnce;

    private void OnEnable()
    {
        doOnce = false;
    }

    private void OnTriggerEnter2D(Collider2D other)
    {        
        if (other.GetComponent<Player>() != null && !doOnce)
        {
            doOnce = true;
            GameObject.FindObjectOfType<GameManager>().Progression++;            
        }
    }
}