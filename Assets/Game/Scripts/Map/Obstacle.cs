﻿using Assets.Game.Scripts.EnemyScripts;
using Assets.Game.Scripts.PlayerScripts;
using UnityEngine;

namespace Assets.Game.Scripts.Map
{
    public class Obstacle : MonoBehaviour
    {
        private void OnTriggerEnter2D(Collider2D collision)
        {
            Bullet bullet = collision.gameObject.GetComponent<Bullet>();            
            if (bullet != null)
            {                
                bullet.DestroyBullet();
                return;
            }

            EnemyBullet enemyBullet = collision.gameObject.GetComponent<EnemyBullet>();
            if (enemyBullet != null)
            {
                enemyBullet.DestroyBullet();
            }
        }
    }
}