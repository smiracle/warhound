﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Assets.Game.Scripts.Map
{
    public class PathRequestManager : MonoBehaviour
    {
        private static PathRequestManager instance;
        private Queue<PathRequest> pathRequestQueue = new Queue<PathRequest>();
        private PathRequest currentPathRequest;
        private Pathfinding pathfinding;
        private bool isProcessingPath;

        public static void RequestPathAndMove(Vector2 pathStart, Vector2 pathEnd, Action<Vector2[], bool> callback)
        {
            PathRequest newRequest = new PathRequest(pathStart, pathEnd, callback);
            instance.pathRequestQueue.Enqueue(newRequest);            
            instance.TryProcessNext();
        }

        public void FinishedProcessingPath(Vector2[] path, bool success)
        {
            currentPathRequest.Callback(path, success);
            isProcessingPath = false;
            TryProcessNext();
        }

        private void Awake()
        {
            instance = this;
            pathfinding = GetComponent<Pathfinding>();
        }

        private void TryProcessNext()
        {
            if (!isProcessingPath && pathRequestQueue.Count > 0)
            {
                currentPathRequest = pathRequestQueue.Dequeue();
                isProcessingPath = true;
                pathfinding.StartFindPath(currentPathRequest.PathStart, currentPathRequest.PathEnd);
            }
        }

        private struct PathRequest
        {
            public Vector2 PathStart;
            public Vector2 PathEnd;
            public Action<Vector2[], bool> Callback;

            public PathRequest(Vector2 start, Vector2 end, Action<Vector2[], bool> callback)
            {
                PathStart = start;
                PathEnd = end;
                this.Callback = callback;
            }
        }
    }
}
