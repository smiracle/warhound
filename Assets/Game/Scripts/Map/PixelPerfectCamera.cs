﻿using UnityEngine;

public class PixelPerfectCamera : MonoBehaviour
{
    private void Awake()
    {
        if (Screen.height >= 1080)
        {
            GetComponent<Camera>().orthographicSize = (Screen.height / 2) / 3;
        }
        else
        {
            GetComponent<Camera>().orthographicSize = (Screen.height / 2) / 2;
        }

        transform.position = new Vector3(transform.position.x - 0.1f, transform.position.y - 0.1f, -1f);
    }
}