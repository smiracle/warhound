﻿using System.Collections.Generic;
using Assets.Game.Scripts.EnemyScripts;
using Assets.Game.Scripts.PlayerScripts;
using UnityEngine;

namespace Assets.Game.Scripts.Map
{
    public class ProgressionManager
    {
        [ShowOnly]
        public int ActiveBoardNumber;
        private int memberProgression;
        private AudioManager audioManager;
        private GameManager gameManager;
        private GridManager gridManager;
        private float randomSpawnDelay;
        private float timeOfLastEnemySpawn;
        private float timeOfBrainDeath;

        public ProgressionManager(GameManager gameManager, GridManager gridManager)
        {
            this.gameManager = gameManager;
            this.gridManager = gridManager;
            randomSpawnDelay = 0f;
            timeOfLastEnemySpawn = 0f;
            gameManager.TimeRemainingMax = 90;
            audioManager = GameObject.FindObjectOfType<AudioManager>();
        }

        public int Progression
        {
            get
            {
                return memberProgression;
            }

            set
            {
                memberProgression = value;
            }
        }

        public void CheckProgression()
        {
            switch (Progression)
            {
                case 0:
                    if (gameManager.EndingCarInstance != null)
                    {
                        GameObject.Destroy(gameManager.EndingCarInstance);
                    }

                    gameManager.GameTitleText.SetActive(true);
                    gameManager.GameInterface.SetActive(false);
                    gameManager.PassivePowerupIconParent.SetActive(false);
                    gameManager.ShowTitle();
                    audioManager.Stop(AudioManager.WarhoundSounds.music_end);
                    Progression++;
                    break;
                case 1:
                    WaitForClickToStartGame();
                    break;
                case 2:
                    gameManager.StartGame();
                    gameManager.GameInterface.SetActive(true);
                    gameManager.PassivePowerupIconParent.SetActive(true);
                    gameManager.GetPlayerGameObject().SetActive(true);
                    gameManager.HideTitle();
                    gameManager.CurrentBoard = gridManager.BoardSetup(ActiveBoardNumber);
                    SetTimerToNinetySeconds();
                    break;
                case 3:
                    // Map 1
                    SpawnEnemiesForActiveMap();                    
                    break;
                case 4:
                    BlinkArrowAndAllowBoardScroll();
                    break;
                case 5:
                    // Leave this empty for transition to map 2
                    break;
                case 6:
                    SetTimerToNinetySeconds();
                    break;
                case 7:
                    // Map 2
                    SpawnEnemiesForActiveMap();
                    break;
                case 8:
                    // Map 2 merchant and arrow spawn
                    SpawnMerchantAndBlinkArrow();
                    break;
                case 9:
                    // Leave this empty for transition to map 3
                    break;
                case 10:
                    SetTimerToNinetySeconds();
                    break;
                case 11:
                    // Map3
                    SpawnEnemiesForActiveMap();
                    break;
                case 12:
                    BlinkArrowAndAllowBoardScroll();
                    break;
                case 13:
                    // Leave this empty for transition to map 4
                    break;
                case 14:
                    SetTimerToNinetySeconds();
                    break;
                case 15:
                    // Map 4
                    SpawnEnemiesForActiveMap();
                    break;
                case 16:
                    SpawnMerchantAndBlinkArrow();
                    break;
                case 17:
                    // Leave this empty for transition to map 5
                    break;
                case 18:
                    Progression++;
                    gameManager.IgnoreTimeIncreases = true;
                    gameManager.HeartIcon.SetActive(true);
                    gameManager.ClockIcon.SetActive(false);
                    audioManager.Stop(AudioManager.WarhoundSounds.music_warhound);
                    audioManager.Play(AudioManager.WarhoundSounds.music_boss);
                    ObjectPooler.Instance.GetPooledObjectAndMoveTo(ObjectPooler.ObjectType.Roboboss, gridManager.GetWorldCoordinatesFromNodeCoordinates(new Vector2(7, 13)));
                    break;
                case 19:
                    // Roboboss fight
                    if (gameManager.EnemyKillCount == gameManager.EnemySpawnCount)
                    {
                        gridManager.CreateBridgeTile(new Vector2(8, 8));
                        Progression++;
                    }

                    break;
                case 20:
                    // Ready for manhole entry
                    break;
                case 21:
                    gameManager.IgnoreTimeIncreases = false;
                    gameManager.HeartIcon.SetActive(false);
                    gameManager.ClockIcon.SetActive(true);
                    audioManager.Stop(AudioManager.WarhoundSounds.music_boss);
                    audioManager.Play(AudioManager.WarhoundSounds.music_warhound);
                    gameManager.DeactivateCurrentBoard();
                    ObjectPooler.Instance.DeactivateAllPowerupsAndEnemiesForBoardScroll();
                    ActiveBoardNumber++;
                    gameManager.CurrentBoard = gridManager.BoardSetup(ActiveBoardNumber);
                    gameManager.GetPlayerComponent().transform.position = gridManager.GetWorldCoordinatesFromNodeCoordinates(new Vector2(12, 5));
                    gameManager.GetPlayerComponent().FaceDown();
                    Progression++;
                    break;
                case 22:
                    SetTimerToNinetySeconds();
                    break;
                case 23:
                    // Map 6
                    SpawnEnemiesForActiveMap();
                    break;
                case 24:
                    BlinkArrowAndAllowBoardScroll();
                    break;
                case 25:
                    // Leave this empty for transition to map 7 (sewer 2-2)
                    break;
                case 26:
                    gridManager.CreateBridgeTile(new Vector2(5, 4));
                    gridManager.CreateBridgeTile(new Vector2(5, 11));
                    gridManager.CreateBridgeTile(new Vector2(8, 4));
                    gridManager.CreateBridgeTile(new Vector2(8, 11));
                    gridManager.CreateBridgeTile(new Vector2(11, 4));
                    gridManager.CreateBridgeTile(new Vector2(11, 11));
                    SetTimerToNinetySeconds();
                    break;
                case 27:
                    // Map 7 (sewer 2-2)
                    SpawnEnemiesForActiveMap();
                    break;
                case 28:
                    SpawnMerchantAndBlinkArrow();
                    break;
                case 29:
                    // Leave this empty for transition to map 8 (sewer 2-3)
                    break;
                case 30:
                    SetTimerToNinetySeconds();
                    break;
                case 31:
                    // Map 8 (sewer 2-3)
                    SpawnEnemiesForActiveMap();
                    break;
                case 32:
                    BlinkArrowAndAllowBoardScroll();
                    break;
                case 33:
                    // Leave this empty for transition to map 9 (sewer 2-4)
                    break;
                case 34:
                    SetTimerToNinetySeconds();
                    break;
                case 35:
                    // Map 9 (sewer 2-4)
                    SpawnEnemiesForActiveMap();
                    break;
                case 36:
                    SpawnMerchantAndBlinkArrow();
                    break;
                case 37:
                    // Leave this empty for transition to map 10 (sewer 2-5)
                    break;
                case 38:
                    gameManager.IgnoreTimeIncreases = true;
                    gameManager.HeartIcon.SetActive(true);
                    gameManager.ClockIcon.SetActive(false);
                    audioManager.Stop(AudioManager.WarhoundSounds.music_warhound);
                    audioManager.Play(AudioManager.WarhoundSounds.music_boss);
                    ObjectPooler.Instance.GetPooledObjectAndMoveTo(ObjectPooler.ObjectType.Roboboss2, gridManager.GetWorldCoordinatesFromNodeCoordinates(new Vector2(7, 13)));
                    Progression++;
                    break;
                case 39:
                    // Map 10 - Roboboss 2 fight
                    if (gameManager.EnemyKillCount == gameManager.EnemySpawnCount)
                    {
                        gridManager.CreateBridgeTile(new Vector2(8, 8));
                        Progression++;
                    }

                    break;
                case 40:
                    // Ready for manhole2 entry
                    break;
                case 41:
                    gameManager.IgnoreTimeIncreases = false;
                    gameManager.HeartIcon.SetActive(false);
                    gameManager.ClockIcon.SetActive(true);
                    gameManager.DeactivateCurrentBoard();
                    audioManager.Stop(AudioManager.WarhoundSounds.music_boss);
                    audioManager.Play(AudioManager.WarhoundSounds.music_warhound);
                    ObjectPooler.Instance.DeactivateAllPowerupsAndEnemiesForBoardScroll();
                    ActiveBoardNumber++;
                    gameManager.CurrentBoard = gridManager.BoardSetup(ActiveBoardNumber);
                    gameManager.GetPlayerComponent().transform.position = gridManager.GetWorldCoordinatesFromNodeCoordinates(new Vector2(13, 2));
                    gameManager.GetPlayerComponent().FaceDown();
                    Progression++;
                    break;
                case 42:
                    SetTimerToNinetySeconds();
                    break;
                case 43:
                    // Map 11 (lab 3-1)
                    SpawnEnemiesForActiveMap();
                    break;
                case 44:
                    BlinkArrowAndAllowBoardScroll();
                    break;
                case 45:
                    // Leave this empty for transition to map 12 (lab 3-2)
                    break;
                case 46:
                    SetTimerToNinetySeconds();
                    break;
                case 47:
                    // Map 12 (lab 3-2)
                    SpawnEnemiesForActiveMap();
                    break;
                case 48:
                    SpawnMerchantAndBlinkArrow();
                    break;
                case 49:
                    // Leave this empty for transition to map 13 (lab 3-3)
                    break;
                case 50:
                    SetTimerToNinetySeconds();
                    break;
                case 51:
                    // Map 13 (lab 3-3)
                    SpawnEnemiesForActiveMap();
                    break;
                case 52:
                    BlinkArrowAndAllowBoardScroll();
                    break;
                case 53:
                    // Leave this empty for transition to map 14 (lab 3-4)
                    break;
                case 54:
                    SetTimerToNinetySeconds();
                    break;
                case 55:
                    // Map 14 (lab 3-4)
                    SpawnEnemiesForActiveMap();
                    break;
                case 56:
                    SpawnMerchantAndBlinkArrow();
                    break;
                case 57:
                    // Leave this empty for transition to map 15 (lab 3-5)
                    break;
                case 58:
                    gameManager.IgnoreTimeIncreases = true;
                    gameManager.HeartIcon.SetActive(true);
                    gameManager.ClockIcon.SetActive(false);
                    audioManager.Stop(AudioManager.WarhoundSounds.music_warhound);
                    audioManager.Play(AudioManager.WarhoundSounds.music_boss);
                    ObjectPooler.Instance.GetPooledObjectAndMoveTo(ObjectPooler.ObjectType.Brain, gridManager.GetWorldCoordinatesFromNodeCoordinates(new Vector2(7, 7)));
                    Progression++;
                    break;
                case 59:
                    // Map 15 (lab 3-5) Brain 
                    if (gameManager.TimeRemaining == 0 && gameManager.EnemyKillCount == gameManager.EnemySpawnCount)
                    {
                        Progression++;
                        randomSpawnDelay = Time.time;
                    }

                    break;
                case 60:
                    if (Time.time > randomSpawnDelay + 5f)
                    {
                        // Wait to transition to ending
                        timeOfBrainDeath = Time.time;
                        audioManager.Stop(AudioManager.WarhoundSounds.music_boss);
                        ActiveBoardNumber++;
                        Progression++;
                    }

                    break;
                case 61:
                    gameManager.DeactivateCurrentBoard();
                    gameManager.CurrentBoard = gridManager.BoardSetup(ActiveBoardNumber); // Ending
                    gameManager.EndingCarInstance = GameObject.Instantiate(gameManager.EndingCarPrefab);                    
                    gameManager.EndingCarInstance.transform.position = new Vector2(-200f, 30f);
                    gameManager.GetPlayerGameObject().SetActive(false);
                    audioManager.Play(AudioManager.WarhoundSounds.music_end);
                    Progression++;
                    break;
                case 62:
                    EndingUpdate1();
                    break;
                case 63:
                    EndingUpdate2();
                    break;
                case 64:
                    audioManager.Stop(AudioManager.WarhoundSounds.music_end);
                    gameManager.DeactivateCurrentBoard();
                    ActiveBoardNumber = 1;
                    Progression = 0;
                    break;
                default:
                    throw new System.Exception("Reached end of progression " + Progression);
            }
        }

        private void EndingUpdate2()
        {
            if (Time.time > timeOfBrainDeath + 12f)
            {
                Progression++;
            }
            else if (Time.time > timeOfBrainDeath + 12f)
            {
                if (!gameManager.GameTitleText.activeInHierarchy)
                {
                    gameManager.GameTitleText.SetActive(true);
                }
            }
            else if (Time.time > timeOfBrainDeath + 6f)
            {
                gameManager.EndingCarInstance.transform.position = Vector2.MoveTowards(gameManager.EndingCarInstance.transform.position, new Vector2(300f, 30f), 10f);
            }
            else if (Time.time > timeOfBrainDeath + 5f)
            {
                if (gameManager.EndingDialogInstance != null)
                {
                    GameObject.Destroy(gameManager.EndingDialogInstance);
                }
            }
        }

        private void EndingUpdate1()
        {
            if (Time.time > timeOfBrainDeath + 3f)
            {
                Progression++;
                gameManager.EndingDialogInstance = GameObject.Instantiate(gameManager.EndingDialogPrefab);
                gameManager.EndingDialogInstance.transform.position = new Vector2(-GridManager.TileRadius * 3, GridManager.TileRadius * 7);
            }
            else
            {
                gameManager.EndingCarInstance.transform.position = Vector2.MoveTowards(gameManager.EndingCarInstance.transform.position, new Vector2(-40.6f, 30f), 3f);
            }
        }

        private void Map14EnemySpawning()
        {
            if (gameManager.TimeRemaining > 70 && gameManager.EnemySpawnCount - gameManager.EnemyKillCount < 10)
            {
                var random = Random.Range(0, 10);
                if (random > 10)
                {
                    var nodes = gridManager.GetAllBorderNodes();
                    if (nodes.Count == 0)
                    {
                        return; // All nodes are blocked
                    }

                    ObjectPooler.Instance.GetPooledObjectAndMoveTo(ObjectPooler.ObjectType.Bat, nodes[Random.Range(0, nodes.Count)].WorldPosition);
                }
                else if (random > 10)
                {
                    var nodes = gridManager.GetUnblockedBorderNodes();
                    if (nodes.Count == 0)
                    {
                        return; // All nodes are blocked
                    }

                    ObjectPooler.Instance.GetPooledObjectAndMoveTo(ObjectPooler.ObjectType.Spider, nodes[Random.Range(0, nodes.Count)].WorldPosition);
                }
                else
                {
                    var nodes = gridManager.GetUnblockedBorderNodes();
                    if (nodes.Count == 0)
                    {
                        return; // All nodes are blocked
                    }

                    ObjectPooler.Instance.GetPooledObjectAndMoveTo(ObjectPooler.ObjectType.Roboshield, nodes[Random.Range(0, nodes.Count)].WorldPosition);
                }

                timeOfLastEnemySpawn = Time.time;
                randomSpawnDelay = Random.Range(0.6f, 1.4f);
            }
            else if (gameManager.TimeRemaining > 60 && gameManager.EnemySpawnCount - gameManager.EnemyKillCount < 11)
            {
                var random = Random.Range(0, 10);
                if (random > 8)
                {
                    var nodes = gridManager.GetUnblockedBorderNodes();
                    if (nodes.Count == 0)
                    {
                        return; // All nodes are blocked
                    }

                    ObjectPooler.Instance.GetPooledObjectAndMoveTo(ObjectPooler.ObjectType.Spider, nodes[Random.Range(0, nodes.Count)].WorldPosition);
                }
                else if (random > 6)
                {
                    var nodes = gridManager.GetUnblockedBorderNodes();
                    if (nodes.Count == 0)
                    {
                        return; // All nodes are blocked
                    }

                    ObjectPooler.Instance.GetPooledObjectAndMoveTo(ObjectPooler.ObjectType.MineLayer, nodes[Random.Range(0, nodes.Count)].WorldPosition);
                }
                else
                {
                    var nodes = gridManager.GetUnblockedBorderNodes();
                    if (nodes.Count == 0)
                    {
                        return; // All nodes are blocked
                    }

                    ObjectPooler.Instance.GetPooledObjectAndMoveTo(ObjectPooler.ObjectType.Roboshield, nodes[Random.Range(0, nodes.Count)].WorldPosition);
                }

                timeOfLastEnemySpawn = Time.time;
                randomSpawnDelay = Random.Range(0.6f, 0.9f);
            }
            else if (gameManager.TimeRemaining > 50)
            {
                // Take a break
            }
            else if (gameManager.TimeRemaining > 0 && gameManager.EnemySpawnCount - gameManager.EnemyKillCount < 12)
            {
                var random = Random.Range(0, 10);
                if (random > 8)
                {
                    var nodes = gridManager.GetUnblockedBorderNodes();
                    if (nodes.Count == 0)
                    {
                        return; // All nodes are blocked
                    }

                    ObjectPooler.Instance.GetPooledObjectAndMoveTo(ObjectPooler.ObjectType.Spider, nodes[Random.Range(0, nodes.Count)].WorldPosition);
                }
                else if (random > 6)
                {
                    var nodes = gridManager.GetUnblockedBorderNodes();
                    if (nodes.Count == 0)
                    {
                        return; // All nodes are blocked
                    }

                    ObjectPooler.Instance.GetPooledObjectAndMoveTo(ObjectPooler.ObjectType.MineLayer, nodes[Random.Range(0, nodes.Count)].WorldPosition);
                }
                else
                {
                    var nodes = gridManager.GetUnblockedBorderNodes();
                    if (nodes.Count == 0)
                    {
                        return; // All nodes are blocked
                    }

                    ObjectPooler.Instance.GetPooledObjectAndMoveTo(ObjectPooler.ObjectType.Roboshield, nodes[Random.Range(0, nodes.Count)].WorldPosition);
                }

                timeOfLastEnemySpawn = Time.time;
                randomSpawnDelay = Random.Range(0.6f, 0.9f);
            }
        }

        private void Map13EnemySpawning()
        {
            if (gameManager.TimeRemaining > 70 && gameManager.EnemySpawnCount - gameManager.EnemyKillCount < 12)
            {
                var nodes = gridManager.GetUnblockedLeftAndRightBorderNodes();
                if (nodes.Count == 0)
                {
                    return; // All nodes are blocked
                }

                ObjectPooler.Instance.GetPooledObjectAndMoveTo(ObjectPooler.ObjectType.Robot, nodes[Random.Range(0, nodes.Count)].WorldPosition);
                timeOfLastEnemySpawn = Time.time;
                randomSpawnDelay = Random.Range(2, 3);
            }
            else if (gameManager.TimeRemaining > 60 && gameManager.EnemySpawnCount - gameManager.EnemyKillCount < 8)
            {
                var nodes = gridManager.GetUnblockedBorderNodes();
                if (nodes.Count == 0)
                {
                    return; // All nodes are blocked
                }

                ObjectPooler.Instance.GetPooledObjectAndMoveTo(ObjectPooler.ObjectType.Robot, nodes[Random.Range(0, nodes.Count)].WorldPosition);
                timeOfLastEnemySpawn = Time.time;
                randomSpawnDelay = Random.Range(0.6f, 2);
            }
            else if (gameManager.TimeRemaining > 0 && gameManager.EnemySpawnCount - gameManager.EnemyKillCount < 11)
            {
                var random = Random.Range(0, 10);
                if (random > 10)
                {
                    var nodes = gridManager.GetAllBorderNodes();
                    if (nodes.Count == 0)
                    {
                        return; // All nodes are blocked
                    }

                    ObjectPooler.Instance.GetPooledObjectAndMoveTo(ObjectPooler.ObjectType.Bat, nodes[Random.Range(0, nodes.Count)].WorldPosition);
                }
                else if (random > 6)
                {
                    var nodes = gridManager.GetUnblockedBorderNodes();
                    if (nodes.Count == 0)
                    {
                        return; // All nodes are blocked
                    }

                    ObjectPooler.Instance.GetPooledObjectAndMoveTo(ObjectPooler.ObjectType.MineLayer, nodes[Random.Range(0, nodes.Count)].WorldPosition);
                }
                else
                {
                    var nodes = gridManager.GetUnblockedBorderNodes();
                    if (nodes.Count == 0)
                    {
                        return; // All nodes are blocked
                    }

                    ObjectPooler.Instance.GetPooledObjectAndMoveTo(ObjectPooler.ObjectType.Roboshield, nodes[Random.Range(0, nodes.Count)].WorldPosition);
                }

                timeOfLastEnemySpawn = Time.time;
                randomSpawnDelay = Random.Range(1, 2);
            }
        }

        /// <summary>
        /// Level 3-2
        /// </summary>
        private void Map12EnemySpawning()
        {
            if (gameManager.TimeRemaining > 70 && gameManager.EnemySpawnCount - gameManager.EnemyKillCount < 8)
            {
                var random = Random.Range(0, 10);
                if (random > 10)
                {
                    var nodes = gridManager.GetAllBorderNodes();
                    if (nodes.Count == 0)
                    {
                        return; // All nodes are blocked
                    }

                    ObjectPooler.Instance.GetPooledObjectAndMoveTo(ObjectPooler.ObjectType.Bat, nodes[Random.Range(0, nodes.Count)].WorldPosition);
                }
                else if (random > 7)
                {
                    var nodes = gridManager.GetAllBorderNodes();
                    if (nodes.Count == 0)
                    {
                        return; // All nodes are blocked
                    }

                    ObjectPooler.Instance.GetPooledObjectAndMoveTo(ObjectPooler.ObjectType.Eyeball, nodes[Random.Range(0, nodes.Count)].WorldPosition);
                }
                else
                {
                    var nodes = gridManager.GetUnblockedBorderNodes();
                    if (nodes.Count == 0)
                    {
                        return; // All nodes are blocked
                    }

                    ObjectPooler.Instance.GetPooledObjectAndMoveTo(ObjectPooler.ObjectType.Roboshield, nodes[Random.Range(0, nodes.Count)].WorldPosition);
                }

                timeOfLastEnemySpawn = Time.time;
                randomSpawnDelay = Random.Range(0.4f, 1.2f);
            }
            else if (gameManager.TimeRemaining > 50 && gameManager.EnemySpawnCount - gameManager.EnemyKillCount < 9)
            {
                var random = Random.Range(0, 10);
                if (random > 10)
                {
                    var nodes = gridManager.GetAllBorderNodes();
                    if (nodes.Count == 0)
                    {
                        return; // All nodes are blocked
                    }

                    ObjectPooler.Instance.GetPooledObjectAndMoveTo(ObjectPooler.ObjectType.Bat, nodes[Random.Range(0, nodes.Count)].WorldPosition);
                }
                else if (random > 7)
                {
                    var nodes = gridManager.GetAllBorderNodes();
                    if (nodes.Count == 0)
                    {
                        return; // All nodes are blocked
                    }

                    ObjectPooler.Instance.GetPooledObjectAndMoveTo(ObjectPooler.ObjectType.Eyeball, nodes[Random.Range(0, nodes.Count)].WorldPosition);
                }
                else
                {
                    var nodes = gridManager.GetUnblockedBorderNodes();
                    if (nodes.Count == 0)
                    {
                        return; // All nodes are blocked
                    }

                    ObjectPooler.Instance.GetPooledObjectAndMoveTo(ObjectPooler.ObjectType.Roboshield, nodes[Random.Range(0, nodes.Count)].WorldPosition);
                }

                timeOfLastEnemySpawn = Time.time;
                randomSpawnDelay = Random.Range(0.4f, 1f);
            }
            else if (gameManager.TimeRemaining > 40)
            {
                // Take a break
            }
            else if (gameManager.TimeRemaining > 0 && gameManager.EnemySpawnCount - gameManager.EnemyKillCount < 11)
            {
                var random = Random.Range(0, 10);
                if (random > 8)
                {
                    var nodes = gridManager.GetAllBorderNodes();
                    if (nodes.Count == 0)
                    {
                        return; // All nodes are blocked
                    }

                    ObjectPooler.Instance.GetPooledObjectAndMoveTo(ObjectPooler.ObjectType.Eyeball, nodes[Random.Range(0, nodes.Count)].WorldPosition);
                }
                else
                {
                    var nodes = gridManager.GetUnblockedBorderNodes();
                    if (nodes.Count == 0)
                    {
                        return; // All nodes are blocked
                    }

                    ObjectPooler.Instance.GetPooledObjectAndMoveTo(ObjectPooler.ObjectType.Roboshield, nodes[Random.Range(0, nodes.Count)].WorldPosition);
                }

                timeOfLastEnemySpawn = Time.time;
                randomSpawnDelay = Random.Range(0.6f, 1.4f);
            }
        }

        /// <summary>
        /// Level 3-1
        /// </summary>
        private void Map11EnemySpawning()
        {
            if (gameManager.TimeRemaining > 70 && gameManager.EnemySpawnCount - gameManager.EnemyKillCount < 7)
            {
                var random = Random.Range(0, 10);
                if (random > 10)
                {
                    var nodes = gridManager.GetAllBorderNodes();
                    if (nodes.Count == 0)
                    {
                        return; // All nodes are blocked
                    }

                    ObjectPooler.Instance.GetPooledObjectAndMoveTo(ObjectPooler.ObjectType.Bat, nodes[Random.Range(0, nodes.Count)].WorldPosition);
                }
                else if (random > 10)
                {
                    var nodes = gridManager.GetUnblockedBorderNodes();
                    if (nodes.Count == 0)
                    {
                        return; // All nodes are blocked
                    }

                    ObjectPooler.Instance.GetPooledObjectAndMoveTo(ObjectPooler.ObjectType.Spider, nodes[Random.Range(0, nodes.Count)].WorldPosition);
                }
                else
                {
                    var nodes = gridManager.GetUnblockedBorderNodes();
                    if (nodes.Count == 0)
                    {
                        return; // All nodes are blocked
                    }

                    ObjectPooler.Instance.GetPooledObjectAndMoveTo(ObjectPooler.ObjectType.Roboshield, nodes[Random.Range(0, nodes.Count)].WorldPosition);
                }

                timeOfLastEnemySpawn = Time.time;
                randomSpawnDelay = Random.Range(0.6f, 1.4f);
            }
            else if (gameManager.TimeRemaining > 65)
            {
                // Take a break
            }
            else if (gameManager.TimeRemaining > 50 && gameManager.EnemySpawnCount - gameManager.EnemyKillCount < 9)
            {
                var random = Random.Range(0, 10);
                if (random > 10)
                {
                    var nodes = gridManager.GetAllBorderNodes();
                    if (nodes.Count == 0)
                    {
                        return; // All nodes are blocked
                    }

                    ObjectPooler.Instance.GetPooledObjectAndMoveTo(ObjectPooler.ObjectType.Bat, nodes[Random.Range(0, nodes.Count)].WorldPosition);
                }
                else if (random > 10)
                {
                    var nodes = gridManager.GetUnblockedBorderNodes();
                    if (nodes.Count == 0)
                    {
                        return; // All nodes are blocked
                    }

                    ObjectPooler.Instance.GetPooledObjectAndMoveTo(ObjectPooler.ObjectType.Spider, nodes[Random.Range(0, nodes.Count)].WorldPosition);
                }
                else
                {
                    var nodes = gridManager.GetUnblockedBorderNodes();
                    if (nodes.Count == 0)
                    {
                        return; // All nodes are blocked
                    }

                    ObjectPooler.Instance.GetPooledObjectAndMoveTo(ObjectPooler.ObjectType.Roboshield, nodes[Random.Range(0, nodes.Count)].WorldPosition);
                }

                timeOfLastEnemySpawn = Time.time;
                randomSpawnDelay = Random.Range(0.6f, 1f);
            }
            else if (gameManager.TimeRemaining > 40)
            {
                // Take a break
            }
            else if (gameManager.TimeRemaining > 0 && gameManager.EnemySpawnCount - gameManager.EnemyKillCount < 10)
            {
                var random = Random.Range(0, 10);
                if (random > 10)
                {
                    var nodes = gridManager.GetAllBorderNodes();
                    if (nodes.Count == 0)
                    {
                        return; // All nodes are blocked
                    }

                    ObjectPooler.Instance.GetPooledObjectAndMoveTo(ObjectPooler.ObjectType.Bat, nodes[Random.Range(0, nodes.Count)].WorldPosition);
                }
                else if (random > 10)
                {
                    var nodes = gridManager.GetUnblockedBorderNodes();
                    if (nodes.Count == 0)
                    {
                        return; // All nodes are blocked
                    }

                    ObjectPooler.Instance.GetPooledObjectAndMoveTo(ObjectPooler.ObjectType.Spider, nodes[Random.Range(0, nodes.Count)].WorldPosition);
                }
                else
                {
                    var nodes = gridManager.GetUnblockedBorderNodes();
                    if (nodes.Count == 0)
                    {
                        return; // All nodes are blocked
                    }

                    ObjectPooler.Instance.GetPooledObjectAndMoveTo(ObjectPooler.ObjectType.Roboshield, nodes[Random.Range(0, nodes.Count)].WorldPosition);
                }

                timeOfLastEnemySpawn = Time.time;
                randomSpawnDelay = Random.Range(0.6f, 0.9f);
            }
        }

        /// <summary>
        /// Level 2-4
        /// </summary>
        private void Map9EnemySpawning()
        {
            if (gameManager.TimeRemaining > 70 && gameManager.EnemySpawnCount - gameManager.EnemyKillCount < 6)
            {
                var random = Random.Range(0, 10);
                if (random > 8)
                {
                    var nodes = gridManager.GetAllBorderNodes();
                    if (nodes.Count == 0)
                    {
                        return; // All nodes are blocked
                    }

                    ObjectPooler.Instance.GetPooledObjectAndMoveTo(ObjectPooler.ObjectType.Bat, nodes[Random.Range(0, nodes.Count)].WorldPosition);
                }
                else if (random > 6)
                {
                    var nodes = gridManager.GetUnblockedBorderNodes();
                    if (nodes.Count == 0)
                    {
                        return; // All nodes are blocked
                    }

                    ObjectPooler.Instance.GetPooledObjectAndMoveTo(ObjectPooler.ObjectType.Spider, nodes[Random.Range(0, nodes.Count)].WorldPosition);
                }
                else
                {
                    var nodes = gridManager.GetUnblockedBorderNodes();
                    if (nodes.Count == 0)
                    {
                        return; // All nodes are blocked
                    }

                    ObjectPooler.Instance.GetPooledObjectAndMoveTo(ObjectPooler.ObjectType.Robodog, nodes[Random.Range(0, nodes.Count)].WorldPosition);
                }

                timeOfLastEnemySpawn = Time.time;
                randomSpawnDelay = Random.Range(1, 3);
            }
            else if (gameManager.TimeRemaining > 50 && gameManager.EnemySpawnCount - gameManager.EnemyKillCount < 9)
            {
                var random = Random.Range(0, 10);
                if (random > 8)
                {
                    var nodes = gridManager.GetUnblockedTopAndBottomBorderNodes();
                    if (nodes.Count == 0)
                    {
                        return; // All nodes are blocked
                    }

                    ObjectPooler.Instance.GetPooledObjectAndMoveTo(ObjectPooler.ObjectType.MineLayer, nodes[Random.Range(0, nodes.Count)].WorldPosition);
                }
                else if (random > 6)
                {
                    var nodes = gridManager.GetUnblockedBorderNodes();
                    if (nodes.Count == 0)
                    {
                        return; // All nodes are blocked
                    }

                    ObjectPooler.Instance.GetPooledObjectAndMoveTo(ObjectPooler.ObjectType.Spider, nodes[Random.Range(0, nodes.Count)].WorldPosition);
                }
                else
                {
                    var nodes = gridManager.GetUnblockedBorderNodes();
                    if (nodes.Count == 0)
                    {
                        return; // All nodes are blocked
                    }

                    ObjectPooler.Instance.GetPooledObjectAndMoveTo(ObjectPooler.ObjectType.Robodog, nodes[Random.Range(0, nodes.Count)].WorldPosition);
                }

                timeOfLastEnemySpawn = Time.time;
                randomSpawnDelay = Random.Range(1, 2.5f);
            }
            else if (gameManager.TimeRemaining > 40)
            {
                // Take a break
            }
            else if (gameManager.TimeRemaining > 30 && gameManager.EnemySpawnCount - gameManager.EnemyKillCount < 9)
            {
                var random = Random.Range(0, 10);
                if (random > 8)
                {
                    var nodes = gridManager.GetUnblockedTopAndBottomBorderNodes();
                    if (nodes.Count == 0)
                    {
                        return; // All nodes are blocked
                    }

                    ObjectPooler.Instance.GetPooledObjectAndMoveTo(ObjectPooler.ObjectType.MineLayer, nodes[Random.Range(0, nodes.Count)].WorldPosition);
                }
                else
                {
                    var nodes = gridManager.GetUnblockedBorderNodes();
                    if (nodes.Count == 0)
                    {
                        return; // All nodes are blocked
                    }

                    ObjectPooler.Instance.GetPooledObjectAndMoveTo(ObjectPooler.ObjectType.Robodog, nodes[Random.Range(0, nodes.Count)].WorldPosition);
                }

                timeOfLastEnemySpawn = Time.time;
                randomSpawnDelay = Random.Range(1, 2);
            }
            else if (gameManager.TimeRemaining > 0 && gameManager.EnemySpawnCount - gameManager.EnemyKillCount < 10)
            {
                var random = Random.Range(0, 10);
                if (random > 9)
                {
                    var nodes = gridManager.GetAllBorderNodes();
                    if (nodes.Count == 0)
                    {
                        return; // All nodes are blocked
                    }

                    ObjectPooler.Instance.GetPooledObjectAndMoveTo(ObjectPooler.ObjectType.Bat, nodes[Random.Range(0, nodes.Count)].WorldPosition);
                }
                else if (random > 8)
                {
                    var nodes = gridManager.GetUnblockedBorderNodes();
                    if (nodes.Count == 0)
                    {
                        return; // All nodes are blocked
                    }

                    ObjectPooler.Instance.GetPooledObjectAndMoveTo(ObjectPooler.ObjectType.Spider, nodes[Random.Range(0, nodes.Count)].WorldPosition);
                }
                else
                {
                    var nodes = gridManager.GetUnblockedBorderNodes();
                    if (nodes.Count == 0)
                    {
                        return; // All nodes are blocked
                    }

                    ObjectPooler.Instance.GetPooledObjectAndMoveTo(ObjectPooler.ObjectType.Robodog, nodes[Random.Range(0, nodes.Count)].WorldPosition);
                }

                timeOfLastEnemySpawn = Time.time;
                randomSpawnDelay = Random.Range(0.5f, 1.5f);
            }
        }

        /// <summary>
        /// Level 2-3
        /// </summary>
        private void Map8EnemySpawning()
        {
            if (gameManager.TimeRemaining > 60 && gameManager.EnemySpawnCount - gameManager.EnemyKillCount < 7)
            {
                var random = Random.Range(0, 10);
                if (random > 6)
                {
                    var nodes = gridManager.GetAllBorderNodes();
                    if (nodes.Count == 0)
                    {
                        return; // All nodes are blocked
                    }

                    ObjectPooler.Instance.GetPooledObjectAndMoveTo(ObjectPooler.ObjectType.Bat, nodes[Random.Range(0, nodes.Count)].WorldPosition);
                    timeOfLastEnemySpawn = Time.time;
                }
                else
                {
                    var nodes = gridManager.GetUnblockedBorderNodes();
                    if (nodes.Count == 0)
                    {
                        return; // All nodes are blocked
                    }

                    ObjectPooler.Instance.GetPooledObjectAndMoveTo(ObjectPooler.ObjectType.Spider, nodes[Random.Range(0, nodes.Count)].WorldPosition);
                    timeOfLastEnemySpawn = Time.time;
                }

                randomSpawnDelay = Random.Range(2f, 3f);
            }
            else if (gameManager.TimeRemaining > 45 && gameManager.EnemySpawnCount - gameManager.EnemyKillCount < 7)
            {
                var random = Random.Range(0, 10);
                if (random > 8)
                {
                    var nodes = gridManager.GetAllBorderNodes();
                    if (nodes.Count == 0)
                    {
                        return; // All nodes are blocked
                    }

                    ObjectPooler.Instance.GetPooledObjectAndMoveTo(ObjectPooler.ObjectType.Bat, nodes[Random.Range(0, nodes.Count)].WorldPosition);
                    randomSpawnDelay = Random.Range(2f, 3f);
                    timeOfLastEnemySpawn = Time.time;
                }
                else if (random > 2)
                {
                    var nodes = gridManager.GetUnblockedBorderNodes();
                    if (nodes.Count == 0)
                    {
                        return; // All nodes are blocked
                    }

                    ObjectPooler.Instance.GetPooledObjectAndMoveTo(ObjectPooler.ObjectType.Spider, nodes[Random.Range(0, nodes.Count)].WorldPosition);
                    randomSpawnDelay = Random.Range(2f, 3f);
                    timeOfLastEnemySpawn = Time.time;
                }
                else
                {
                    var nodes = gridManager.GetUnblockedBorderNodes();
                    if (nodes.Count == 0)
                    {
                        return; // All nodes are blocked
                    }

                    ObjectPooler.Instance.GetPooledObjectAndMoveTo(ObjectPooler.ObjectType.Robodog, nodes[Random.Range(0, nodes.Count)].WorldPosition);
                    randomSpawnDelay = Random.Range(2f, 3f);
                    timeOfLastEnemySpawn = Time.time;
                }
            }
            else if (gameManager.TimeRemaining > 35)
            {
                // Take a break
            }
            else if (gameManager.TimeRemaining > 20 && gameManager.EnemySpawnCount - gameManager.EnemyKillCount < 7)
            {
                var random = Random.Range(0, 10);
                if (random > 8)
                {
                    var nodes = gridManager.GetAllBorderNodes();
                    if (nodes.Count == 0)
                    {
                        return; // All nodes are blocked
                    }

                    ObjectPooler.Instance.GetPooledObjectAndMoveTo(ObjectPooler.ObjectType.Bat, nodes[Random.Range(0, nodes.Count)].WorldPosition);
                }
                else if (random > 2)
                {
                    var nodes = gridManager.GetUnblockedBorderNodes();
                    if (nodes.Count == 0)
                    {
                        return; // All nodes are blocked
                    }

                    ObjectPooler.Instance.GetPooledObjectAndMoveTo(ObjectPooler.ObjectType.Spider, nodes[Random.Range(0, nodes.Count)].WorldPosition);
                }
                else
                {
                    var nodes = gridManager.GetUnblockedBorderNodes();
                    if (nodes.Count == 0)
                    {
                        return; // All nodes are blocked
                    }

                    ObjectPooler.Instance.GetPooledObjectAndMoveTo(ObjectPooler.ObjectType.Robodog, nodes[Random.Range(0, nodes.Count)].WorldPosition);
                }

                randomSpawnDelay = Random.Range(1f, 2f);
                timeOfLastEnemySpawn = Time.time;
            }
            else if (gameManager.TimeRemaining > 0 && gameManager.EnemySpawnCount - gameManager.EnemyKillCount < 8)
            {
                var random = Random.Range(0, 10);
                if (random > 8)
                {
                    var nodes = gridManager.GetAllBorderNodes();
                    if (nodes.Count == 0)
                    {
                        return; // All nodes are blocked
                    }

                    ObjectPooler.Instance.GetPooledObjectAndMoveTo(ObjectPooler.ObjectType.Bat, nodes[Random.Range(0, nodes.Count)].WorldPosition);
                    randomSpawnDelay = Random.Range(1f, 1.5f);
                    timeOfLastEnemySpawn = Time.time;
                }
                else if (random > 2)
                {
                    var nodes = gridManager.GetUnblockedBorderNodes();
                    if (nodes.Count == 0)
                    {
                        return; // All nodes are blocked
                    }

                    ObjectPooler.Instance.GetPooledObjectAndMoveTo(ObjectPooler.ObjectType.Spider, nodes[Random.Range(0, nodes.Count)].WorldPosition);
                    randomSpawnDelay = Random.Range(1f, 1.5f);
                    timeOfLastEnemySpawn = Time.time;
                }
                else
                {
                    var nodes = gridManager.GetUnblockedBorderNodes();
                    if (nodes.Count == 0)
                    {
                        return; // All nodes are blocked
                    }

                    ObjectPooler.Instance.GetPooledObjectAndMoveTo(ObjectPooler.ObjectType.Robodog, nodes[Random.Range(0, nodes.Count)].WorldPosition);
                    randomSpawnDelay = Random.Range(1f, 1.5f);
                    timeOfLastEnemySpawn = Time.time;
                }
            }
        }

        /// <summary>
        /// Level 2-2
        /// </summary>
        private void Map7EnemySpawning()
        {
            if (gameManager.TimeRemaining > 60 && gameManager.EnemySpawnCount - gameManager.EnemyKillCount < 7)
            {
                var nodes = gridManager.GetUnblockedLeftAndRightBorderNodes();
                if (nodes.Count == 0)
                {
                    return; // All nodes are blocked
                }

                var spawnPoint = nodes[Random.Range(0, nodes.Count)].WorldPosition;

                if (Random.Range(1, 10) == 9)
                {
                    ObjectPooler.Instance.GetPooledObjectAndMoveTo(ObjectPooler.ObjectType.MineLayer, spawnPoint);
                }
                else
                {
                    ObjectPooler.Instance.GetPooledObjectAndMoveTo(ObjectPooler.ObjectType.Robot, spawnPoint);
                }

                randomSpawnDelay = Random.Range(0.6f, 1.8f);
                timeOfLastEnemySpawn = Time.time;
            }
            else if (gameManager.TimeRemaining > 45 && gameManager.EnemySpawnCount - gameManager.EnemyKillCount < 7)
            {
                var nodes = gridManager.GetUnblockedBorderNodes();
                if (nodes.Count == 0)
                {
                    return; // All nodes are blocked
                }

                var spawnPoint = nodes[Random.Range(0, nodes.Count)].WorldPosition;

                if (Random.Range(1, 10) == 9)
                {
                    ObjectPooler.Instance.GetPooledObjectAndMoveTo(ObjectPooler.ObjectType.Spider, spawnPoint);
                }
                else
                {
                    ObjectPooler.Instance.GetPooledObjectAndMoveTo(ObjectPooler.ObjectType.Robot, spawnPoint);
                }

                randomSpawnDelay = Random.Range(0.6f, 1.2f);
                timeOfLastEnemySpawn = Time.time;
            }
            else if (gameManager.TimeRemaining > 35)
            {
                // Take a break
            }
            else if (gameManager.TimeRemaining > 20 && gameManager.EnemySpawnCount - gameManager.EnemyKillCount < 8)
            {
                var random = Random.Range(0, 10);
                if (random > 9)
                {
                    var nodes = gridManager.GetAllBorderNodes();
                    if (nodes.Count == 0)
                    {
                        return; // All nodes are blocked
                    }

                    ObjectPooler.Instance.GetPooledObjectAndMoveTo(ObjectPooler.ObjectType.Bat, nodes[Random.Range(0, nodes.Count)].WorldPosition);
                }
                else if (random > 2)
                {
                    var nodes = gridManager.GetUnblockedBorderNodes();
                    if (nodes.Count == 0)
                    {
                        return; // All nodes are blocked
                    }

                    ObjectPooler.Instance.GetPooledObjectAndMoveTo(ObjectPooler.ObjectType.Spider, nodes[Random.Range(0, nodes.Count)].WorldPosition);
                }
                else
                {
                    var nodes = gridManager.GetUnblockedBorderNodes();
                    if (nodes.Count == 0)
                    {
                        return; // All nodes are blocked
                    }

                    ObjectPooler.Instance.GetPooledObjectAndMoveTo(ObjectPooler.ObjectType.Robodog, nodes[Random.Range(0, nodes.Count)].WorldPosition);
                }

                randomSpawnDelay = Random.Range(0.9f, 1.2f);
                timeOfLastEnemySpawn = Time.time;
            }
            else if (gameManager.TimeRemaining > 0 && gameManager.EnemySpawnCount - gameManager.EnemyKillCount < 11)
            {
                var random = Random.Range(0, 10);
                if (random > 8)
                {
                    var nodes = gridManager.GetAllBorderNodes();
                    if (nodes.Count == 0)
                    {
                        return; // All nodes are blocked
                    }

                    ObjectPooler.Instance.GetPooledObjectAndMoveTo(ObjectPooler.ObjectType.Bat, nodes[Random.Range(0, nodes.Count)].WorldPosition);
                }
                else if (random > 2)
                {
                    var nodes = gridManager.GetUnblockedBorderNodes();
                    if (nodes.Count == 0)
                    {
                        return; // All nodes are blocked
                    }

                    ObjectPooler.Instance.GetPooledObjectAndMoveTo(ObjectPooler.ObjectType.Spider, nodes[Random.Range(0, nodes.Count)].WorldPosition);
                }
                else
                {
                    var nodes = gridManager.GetUnblockedBorderNodes();
                    if (nodes.Count == 0)
                    {
                        return; // All nodes are blocked
                    }

                    ObjectPooler.Instance.GetPooledObjectAndMoveTo(ObjectPooler.ObjectType.Robodog, nodes[Random.Range(0, nodes.Count)].WorldPosition);
                }

                randomSpawnDelay = Random.Range(0.9f, 1.2f);
                timeOfLastEnemySpawn = Time.time;
            }
        }

        /// <summary>
        /// Level 2-1
        /// </summary>
        private void Map6EnemySpawning()
        {
            if (gameManager.TimeRemaining > 85 && gameManager.EnemySpawnCount - gameManager.EnemyKillCount < 5)
            {
                var nodes = gridManager.GetUnblockedBottomBorderNodes();
                if (nodes.Count == 0)
                {
                    return; // All nodes are blocked
                }

                var spawnPoint = nodes[Random.Range(0, nodes.Count)].WorldPosition;
                ObjectPooler.Instance.GetPooledObjectAndMoveTo(ObjectPooler.ObjectType.Spider, spawnPoint);
                randomSpawnDelay = Random.Range(0.8f, 1.5f);
                timeOfLastEnemySpawn = Time.time;
            }
            else if (gameManager.TimeRemaining > 70 && gameManager.EnemySpawnCount - gameManager.EnemyKillCount < 7)
            {
                var random = Random.Range(1, 10);
                if (random > 4)
                {
                    var nodes = gridManager.GetUnblockedLeftBorderNodes();
                    if (nodes.Count == 0)
                    {
                        return; // All nodes are blocked
                    }

                    var spawnPoint = nodes[Random.Range(0, nodes.Count)].WorldPosition;
                    ObjectPooler.Instance.GetPooledObjectAndMoveTo(ObjectPooler.ObjectType.Spider, spawnPoint);
                    randomSpawnDelay = Random.Range(2f, 4f);
                    timeOfLastEnemySpawn = Time.time;
                }
                else if (random > 1)
                {
                    var nodes = gridManager.GetAllBorderNodes();
                    if (nodes.Count == 0)
                    {
                        return; // All nodes are blocked
                    }

                    var spawnPoint = nodes[Random.Range(0, nodes.Count)].WorldPosition;
                    ObjectPooler.Instance.GetPooledObjectAndMoveTo(ObjectPooler.ObjectType.Bat, spawnPoint);
                    randomSpawnDelay = Random.Range(2, 5);
                    timeOfLastEnemySpawn = Time.time;
                }
                else
                {
                    var nodes = gridManager.GetUnblockedBorderNodes();
                    if (nodes.Count == 0)
                    {
                        return; // All nodes are blocked
                    }

                    var spawnPoint = nodes[Random.Range(0, nodes.Count)].WorldPosition;
                    ObjectPooler.Instance.GetPooledObjectAndMoveTo(ObjectPooler.ObjectType.MineLayer, spawnPoint);

                    randomSpawnDelay = Random.Range(2, 5f);
                    timeOfLastEnemySpawn = Time.time;
                }
            }
            else if (gameManager.TimeRemaining > 60 && gameManager.EnemySpawnCount - gameManager.EnemyKillCount < 7)
            {
                var random = Random.Range(1, 10);
                if (random > 3)
                {
                    var nodes = gridManager.GetUnblockedRightBorderNodes();
                    if (nodes.Count == 0)
                    {
                        return; // All nodes are blocked
                    }

                    var spawnPoint = nodes[Random.Range(0, nodes.Count)].WorldPosition;
                    ObjectPooler.Instance.GetPooledObjectAndMoveTo(ObjectPooler.ObjectType.Spider, spawnPoint);
                    randomSpawnDelay = Random.Range(2, 3);
                    timeOfLastEnemySpawn = Time.time;
                }
                else if(random > 2)
                {
                    var nodes = gridManager.GetAllBorderNodes();
                    if (nodes.Count == 0)
                    {
                        return; // All nodes are blocked
                    }

                    var spawnPoint = nodes[Random.Range(0, nodes.Count)].WorldPosition;
                    ObjectPooler.Instance.GetPooledObjectAndMoveTo(ObjectPooler.ObjectType.Bat, spawnPoint);
                    randomSpawnDelay = Random.Range(2, 3);
                    timeOfLastEnemySpawn = Time.time;
                }
                else
                {
                    var nodes = gridManager.GetUnblockedLeftAndRightBorderNodes();
                    if (nodes.Count == 0)
                    {
                        return; // All nodes are blocked
                    }

                    var spawnPoint = nodes[Random.Range(0, nodes.Count)].WorldPosition;
                    ObjectPooler.Instance.GetPooledObjectAndMoveTo(ObjectPooler.ObjectType.MineLayer, spawnPoint);
                    
                    randomSpawnDelay = Random.Range(2, 3);
                    timeOfLastEnemySpawn = Time.time;
                }
            }
            else if (gameManager.TimeRemaining > 30 && gameManager.EnemySpawnCount - gameManager.EnemyKillCount < 7)
            {
                if (Random.Range(1, 10) > 2)
                {
                    var nodes = gridManager.GetUnblockedBorderNodes();
                    if (nodes.Count == 0)
                    {
                        return; // All nodes are blocked
                    }

                    var spawnPoint = nodes[Random.Range(0, nodes.Count)].WorldPosition;
                    ObjectPooler.Instance.GetPooledObjectAndMoveTo(ObjectPooler.ObjectType.Spider, spawnPoint);
                    randomSpawnDelay = Random.Range(2, 3);
                    timeOfLastEnemySpawn = Time.time;
                }
                else
                {
                    var nodes = gridManager.GetAllBorderNodes();
                    if (nodes.Count == 0)
                    {
                        return; // All nodes are blocked
                    }

                    var spawnPoint = nodes[Random.Range(0, nodes.Count)].WorldPosition;
                    ObjectPooler.Instance.GetPooledObjectAndMoveTo(ObjectPooler.ObjectType.Bat, spawnPoint);
                    randomSpawnDelay = Random.Range(2, 3);
                    timeOfLastEnemySpawn = Time.time;
                }
            }
            else if (gameManager.TimeRemaining > 0 && gameManager.EnemySpawnCount - gameManager.EnemyKillCount < 8)
            {
                if (Random.Range(1, 10) > 2)
                {
                    var nodes = gridManager.GetUnblockedBorderNodes();
                    if (nodes.Count == 0)
                    {
                        return; // All nodes are blocked
                    }

                    var spawnPoint = nodes[Random.Range(0, nodes.Count)].WorldPosition;
                    ObjectPooler.Instance.GetPooledObjectAndMoveTo(ObjectPooler.ObjectType.Spider, spawnPoint);
                    randomSpawnDelay = Random.Range(0.7f, 2);
                    timeOfLastEnemySpawn = Time.time;
                }
                else
                {
                    var nodes = gridManager.GetAllBorderNodes();
                    if (nodes.Count == 0)
                    {
                        return; // All nodes are blocked
                    }

                    var spawnPoint = nodes[Random.Range(0, nodes.Count)].WorldPosition;
                    ObjectPooler.Instance.GetPooledObjectAndMoveTo(ObjectPooler.ObjectType.Bat, spawnPoint);
                    randomSpawnDelay = Random.Range(1f, 1.5f);
                    timeOfLastEnemySpawn = Time.time;
                }
            }
        }

        /// <summary>
        /// Level 1-4
        /// </summary>
        private void Map4EnemySpawning()
        {
            if (gameManager.TimeRemaining > 45 && gameManager.EnemySpawnCount - gameManager.EnemyKillCount < 6)
            {
                var nodes = gridManager.GetUnblockedLeftAndRightBorderNodes();
                if (nodes.Count == 0)
                {
                    return; // All nodes are blocked
                }

                var spawnPoint = nodes[Random.Range(0, nodes.Count)].WorldPosition;

                if (Random.Range(1, 10) == 9)
                {
                    ObjectPooler.Instance.GetPooledObjectAndMoveTo(ObjectPooler.ObjectType.MineLayer, spawnPoint);
                }
                else
                {
                    ObjectPooler.Instance.GetPooledObjectAndMoveTo(ObjectPooler.ObjectType.Robot, spawnPoint);
                }

                randomSpawnDelay = Random.Range(0.6f, 1.8f);
                timeOfLastEnemySpawn = Time.time;
            }
            else if (gameManager.TimeRemaining > 30 && gameManager.EnemySpawnCount - gameManager.EnemyKillCount < 7)
            {
                var nodes = gridManager.GetUnblockedBorderNodes();
                if (nodes.Count == 0)
                {
                    return; // All nodes are blocked
                }

                var spawnPoint = nodes[Random.Range(0, nodes.Count)].WorldPosition;

                if (Random.Range(1, 10) == 9)
                {
                    ObjectPooler.Instance.GetPooledObjectAndMoveTo(ObjectPooler.ObjectType.Spider, spawnPoint);
                }
                else
                {
                    ObjectPooler.Instance.GetPooledObjectAndMoveTo(ObjectPooler.ObjectType.Robot, spawnPoint);
                }

                randomSpawnDelay = Random.Range(0.6f, 0.9f);
                timeOfLastEnemySpawn = Time.time;
            }
            else if (gameManager.TimeRemaining > 10 && gameManager.EnemySpawnCount - gameManager.EnemyKillCount < 8)
            {
                var nodes = gridManager.GetUnblockedBorderNodes();
                if (nodes.Count == 0)
                {
                    return; // All nodes are blocked
                }

                var spawnPoint = nodes[Random.Range(0, nodes.Count)].WorldPosition;

                if (Random.Range(1, 10) == 9)
                {
                    ObjectPooler.Instance.GetPooledObjectAndMoveTo(ObjectPooler.ObjectType.Spider, spawnPoint);
                }
                else
                {
                    ObjectPooler.Instance.GetPooledObjectAndMoveTo(ObjectPooler.ObjectType.Robot, spawnPoint);
                }

                randomSpawnDelay = Random.Range(0.4f, 0.8f);
                timeOfLastEnemySpawn = Time.time;
            }
            else if (gameManager.TimeRemaining > 0 && gameManager.EnemySpawnCount - gameManager.EnemyKillCount < 9)
            {
                var nodes = gridManager.GetUnblockedBorderNodes();
                if (nodes.Count == 0)
                {
                    return; // All nodes are blocked
                }

                var spawnPoint = nodes[Random.Range(0, nodes.Count)].WorldPosition;

                if (Random.Range(1, 10) == 9)
                {
                    ObjectPooler.Instance.GetPooledObjectAndMoveTo(ObjectPooler.ObjectType.Spider, spawnPoint);
                }
                else
                {
                    ObjectPooler.Instance.GetPooledObjectAndMoveTo(ObjectPooler.ObjectType.Robot, spawnPoint);
                }

                randomSpawnDelay = Random.Range(0.4f, 0.6f);
                timeOfLastEnemySpawn = Time.time;
            }
        }

        /// <summary>
        /// Level 1-3
        /// </summary>
        private void Map3EnemySpawning()
        {
            if (gameManager.TimeRemaining > 86 && gameManager.EnemySpawnCount - gameManager.EnemyKillCount < 7)
            {
                var nodes = gridManager.GetUnblockedLeftAndRightBorderNodes();
                if (nodes.Count == 0)
                {
                    return; // All nodes are blocked
                }

                var spawnPoint = nodes[Random.Range(0, nodes.Count)].WorldPosition;

                if (Random.Range(1, 10) == 9)
                {
                    ObjectPooler.Instance.GetPooledObjectAndMoveTo(ObjectPooler.ObjectType.MineLayer, spawnPoint);
                }
                else
                {
                    ObjectPooler.Instance.GetPooledObjectAndMoveTo(ObjectPooler.ObjectType.Robot, spawnPoint);
                }

                randomSpawnDelay = Random.Range(0.2f, 2f);
                timeOfLastEnemySpawn = Time.time;
            }
            else if (gameManager.TimeRemaining > 83 && gameManager.EnemySpawnCount - gameManager.EnemyKillCount < 7)
            {
                var nodes = gridManager.GetUnblockedBottomBorderNodes();
                if (nodes.Count == 0)
                {
                    return; // All nodes are blocked
                }

                var spawnPoint = nodes[Random.Range(0, nodes.Count)].WorldPosition;

                if (Random.Range(1, 10) == 9)
                {
                    ObjectPooler.Instance.GetPooledObjectAndMoveTo(ObjectPooler.ObjectType.Spider, spawnPoint);
                }
                else
                {
                    ObjectPooler.Instance.GetPooledObjectAndMoveTo(ObjectPooler.ObjectType.Robot, spawnPoint);
                }

                randomSpawnDelay = Random.Range(1.0f, 3f);
                timeOfLastEnemySpawn = Time.time;
            }
            else if (gameManager.TimeRemaining > 75 && gameManager.EnemySpawnCount - gameManager.EnemyKillCount < 7)
            {
                var nodes = gridManager.GetUnblockedBorderNodes();
                if (nodes.Count == 0)
                {
                    return; // All nodes are blocked
                }

                var spawnPoint = nodes[Random.Range(0, nodes.Count)].WorldPosition;

                if (Random.Range(1, 10) == 9)
                {
                    ObjectPooler.Instance.GetPooledObjectAndMoveTo(ObjectPooler.ObjectType.Spider, spawnPoint);
                }
                else
                {
                    ObjectPooler.Instance.GetPooledObjectAndMoveTo(ObjectPooler.ObjectType.Robot, spawnPoint);
                }

                randomSpawnDelay = Random.Range(1.4f, 3f);
                timeOfLastEnemySpawn = Time.time;
            }
            else if (gameManager.TimeRemaining > 60 && gameManager.EnemySpawnCount - gameManager.EnemyKillCount < 8)
            {
                var nodes = gridManager.GetUnblockedLeftAndRightBorderNodes();
                if (nodes.Count == 0)
                {
                    return; // All nodes are blocked
                }

                var spawnPoint = nodes[Random.Range(0, nodes.Count)].WorldPosition;
                ObjectPooler.Instance.GetPooledObjectAndMoveTo(ObjectPooler.ObjectType.Robot, spawnPoint);

                randomSpawnDelay = Random.Range(1.0f, 2f);
                timeOfLastEnemySpawn = Time.time;
            }
            else if (gameManager.TimeRemaining > 50 && gameManager.EnemySpawnCount - gameManager.EnemyKillCount < 7)
            {
                var nodes = gridManager.GetUnblockedBorderNodes();
                if (nodes.Count == 0)
                {
                    return; // All nodes are blocked
                }

                var spawnPoint = nodes[Random.Range(0, nodes.Count)].WorldPosition;
                ObjectPooler.Instance.GetPooledObjectAndMoveTo(ObjectPooler.ObjectType.Robot, spawnPoint);
                randomSpawnDelay = Random.Range(1.4f, 3f);
                timeOfLastEnemySpawn = Time.time;
            }
            else if (gameManager.TimeRemaining > 40 && gameManager.EnemySpawnCount - gameManager.EnemyKillCount < 7)
            {
                var nodes = gridManager.GetUnblockedTopAndBottomBorderNodes();
                if (nodes.Count == 0)
                {
                    return; // All nodes are blocked
                }

                var spawnPoint = nodes[Random.Range(0, nodes.Count)].WorldPosition;

                if (Random.Range(1, 8) == 7)
                {
                    ObjectPooler.Instance.GetPooledObjectAndMoveTo(ObjectPooler.ObjectType.MineLayer, spawnPoint);
                }
                else
                {
                    ObjectPooler.Instance.GetPooledObjectAndMoveTo(ObjectPooler.ObjectType.Robot, spawnPoint);
                }

                randomSpawnDelay = Random.Range(1.4f, 3f);
                timeOfLastEnemySpawn = Time.time;
            }
            else if (gameManager.TimeRemaining > 30 && gameManager.EnemySpawnCount - gameManager.EnemyKillCount < 7)
            {
                var nodes = gridManager.GetUnblockedBorderNodes();
                if (nodes.Count == 0)
                {
                    return; // All nodes are blocked
                }

                var spawnPoint = nodes[Random.Range(0, nodes.Count)].WorldPosition;

                ObjectPooler.Instance.GetPooledObjectAndMoveTo(ObjectPooler.ObjectType.Robot, spawnPoint);
                randomSpawnDelay = Random.Range(1, 2.5f);
                timeOfLastEnemySpawn = Time.time;
            }
            else if (gameManager.TimeRemaining > 20 && gameManager.EnemySpawnCount - gameManager.EnemyKillCount < 8)
            {
                var nodes = gridManager.GetUnblockedBorderNodes();
                if (nodes.Count == 0)
                {
                    return; // All nodes are blocked
                }

                var spawnPoint = nodes[Random.Range(0, nodes.Count)].WorldPosition;

                ObjectPooler.Instance.GetPooledObjectAndMoveTo(ObjectPooler.ObjectType.Robot, spawnPoint);
                randomSpawnDelay = Random.Range(1f, 2f);
                timeOfLastEnemySpawn = Time.time;
            }
            else if (gameManager.TimeRemaining > 10 && gameManager.EnemySpawnCount - gameManager.EnemyKillCount < 9)
            {
                var nodes = gridManager.GetUnblockedBorderNodes();
                if (nodes.Count == 0)
                {
                    return; // All nodes are blocked
                }

                var spawnPoint = nodes[Random.Range(0, nodes.Count)].WorldPosition;

                if (Random.Range(1, 10) == 9)
                {
                    ObjectPooler.Instance.GetPooledObjectAndMoveTo(ObjectPooler.ObjectType.Spider, spawnPoint);
                }
                else
                {
                    ObjectPooler.Instance.GetPooledObjectAndMoveTo(ObjectPooler.ObjectType.Robot, spawnPoint);
                }

                randomSpawnDelay = Random.Range(0.4f, 2f);
                timeOfLastEnemySpawn = Time.time;
            }
            else if (gameManager.TimeRemaining > 0 && gameManager.EnemySpawnCount - gameManager.EnemyKillCount < 9)
            {
                var nodes = gridManager.GetUnblockedBorderNodes();
                if (nodes.Count == 0)
                {
                    return; // All nodes are blocked
                }

                var spawnPoint = nodes[Random.Range(0, nodes.Count)].WorldPosition;

                if (Random.Range(1, 10) == 9)
                {
                    ObjectPooler.Instance.GetPooledObjectAndMoveTo(ObjectPooler.ObjectType.Spider, spawnPoint);
                }
                else
                {
                    ObjectPooler.Instance.GetPooledObjectAndMoveTo(ObjectPooler.ObjectType.Robot, spawnPoint);
                }

                randomSpawnDelay = Random.Range(0.4f, 0.6f);
                timeOfLastEnemySpawn = Time.time;
            }
        }

        /// <summary>
        /// Level 1-2
        /// </summary>
        private void Map2EnemySpawning()
        {
            if (gameManager.TimeRemaining > 86 && gameManager.EnemySpawnCount - gameManager.EnemyKillCount < 5)
            {
                var nodes = gridManager.GetUnblockedBottomBorderNodes();
                if (nodes.Count == 0)
                {
                    return; // All nodes are blocked
                }

                var spawnPoint = nodes[Random.Range(0, nodes.Count)].WorldPosition;
                ObjectPooler.Instance.GetPooledObjectAndMoveTo(ObjectPooler.ObjectType.MineLayer, spawnPoint);
                randomSpawnDelay = Random.Range(0.8f, 1.5f);
                timeOfLastEnemySpawn = Time.time;
            }
            else if (gameManager.TimeRemaining > 83 && gameManager.EnemySpawnCount - gameManager.EnemyKillCount < 6)
            {
                var nodes = gridManager.GetUnblockedTopBorderNodes();
                if (nodes.Count == 0)
                {
                    return; // All nodes are blocked
                }

                var spawnPoint = nodes[Random.Range(0, nodes.Count)].WorldPosition;

                if (Random.Range(1, 10) == 9)
                {
                    ObjectPooler.Instance.GetPooledObjectAndMoveTo(ObjectPooler.ObjectType.MineLayer, spawnPoint);
                }
                else
                {
                    ObjectPooler.Instance.GetPooledObjectAndMoveTo(ObjectPooler.ObjectType.Robot, spawnPoint);
                }

                randomSpawnDelay = Random.Range(1.0f, 2f);
                timeOfLastEnemySpawn = Time.time;
            }
            else if (gameManager.TimeRemaining > 75 && gameManager.EnemySpawnCount - gameManager.EnemyKillCount < 6)
            {
                var nodes = gridManager.GetUnblockedBorderNodes();
                if (nodes.Count == 0)
                {
                    return; // All nodes are blocked
                }

                var spawnPoint = nodes[Random.Range(0, nodes.Count)].WorldPosition;

                if (Random.Range(1, 10) == 9)
                {
                    ObjectPooler.Instance.GetPooledObjectAndMoveTo(ObjectPooler.ObjectType.MineLayer, spawnPoint);
                }
                else
                {
                    ObjectPooler.Instance.GetPooledObjectAndMoveTo(ObjectPooler.ObjectType.Robot, spawnPoint);
                }

                randomSpawnDelay = Random.Range(0.8f, 3f);
                timeOfLastEnemySpawn = Time.time;
            }
            else if (gameManager.TimeRemaining > 60 && gameManager.EnemySpawnCount - gameManager.EnemyKillCount < 7)
            {
                var nodes = gridManager.GetUnblockedBorderNodes();
                if (nodes.Count == 0)
                {
                    return; // All nodes are blocked
                }

                var spawnPoint = nodes[Random.Range(0, nodes.Count)].WorldPosition;
                ObjectPooler.Instance.GetPooledObjectAndMoveTo(ObjectPooler.ObjectType.Robot, spawnPoint);

                randomSpawnDelay = Random.Range(1.0f, 4f);
                timeOfLastEnemySpawn = Time.time;
            }
            else if (gameManager.TimeRemaining > 50 && gameManager.EnemySpawnCount - gameManager.EnemyKillCount < 7)
            {
                var nodes = gridManager.GetUnblockedBorderNodes();
                if (nodes.Count == 0)
                {
                    return; // All nodes are blocked
                }

                var spawnPoint = nodes[Random.Range(0, nodes.Count)].WorldPosition;

                if (Random.Range(1, 8) == 7)
                {
                    ObjectPooler.Instance.GetPooledObjectAndMoveTo(ObjectPooler.ObjectType.MineLayer, spawnPoint);
                }
                else
                {
                    ObjectPooler.Instance.GetPooledObjectAndMoveTo(ObjectPooler.ObjectType.Robot, spawnPoint);
                }

                randomSpawnDelay = Random.Range(0.4f, 4f);
                timeOfLastEnemySpawn = Time.time;
            }
            else if (gameManager.TimeRemaining > 40 && gameManager.EnemySpawnCount - gameManager.EnemyKillCount < 7)
            {
                var nodes = gridManager.GetUnblockedBorderNodes();
                if (nodes.Count == 0)
                {
                    return; // All nodes are blocked
                }

                var spawnPoint = nodes[Random.Range(0, nodes.Count)].WorldPosition;

                if (Random.Range(1, 8) == 7)
                {
                    ObjectPooler.Instance.GetPooledObjectAndMoveTo(ObjectPooler.ObjectType.MineLayer, spawnPoint);
                }
                else
                {
                    ObjectPooler.Instance.GetPooledObjectAndMoveTo(ObjectPooler.ObjectType.Robot, spawnPoint);
                }

                randomSpawnDelay = Random.Range(0.4f, 3.5f);
                timeOfLastEnemySpawn = Time.time;
            }
            else if (gameManager.TimeRemaining > 30 && gameManager.EnemySpawnCount - gameManager.EnemyKillCount < 7)
            {
                var nodes = gridManager.GetUnblockedBorderNodes();
                if (nodes.Count == 0)
                {
                    return; // All nodes are blocked
                }

                var spawnPoint = nodes[Random.Range(0, nodes.Count)].WorldPosition;

                if (Random.Range(1, 8) == 7)
                {
                    ObjectPooler.Instance.GetPooledObjectAndMoveTo(ObjectPooler.ObjectType.MineLayer, spawnPoint);
                }
                else
                {
                    ObjectPooler.Instance.GetPooledObjectAndMoveTo(ObjectPooler.ObjectType.Robot, spawnPoint);
                }

                randomSpawnDelay = Random.Range(0.4f, 3.5f);
                timeOfLastEnemySpawn = Time.time;
            }
            else if (gameManager.TimeRemaining > 20 && gameManager.EnemySpawnCount - gameManager.EnemyKillCount < 7)
            {
                var nodes = gridManager.GetUnblockedBorderNodes();
                if (nodes.Count == 0)
                {
                    return; // All nodes are blocked
                }

                var spawnPoint = nodes[Random.Range(0, nodes.Count)].WorldPosition;

                if (Random.Range(1, 8) == 7)
                {
                    ObjectPooler.Instance.GetPooledObjectAndMoveTo(ObjectPooler.ObjectType.MineLayer, spawnPoint);
                }
                else
                {
                    ObjectPooler.Instance.GetPooledObjectAndMoveTo(ObjectPooler.ObjectType.Robot, spawnPoint);
                }

                randomSpawnDelay = Random.Range(0.4f, 3f);
                timeOfLastEnemySpawn = Time.time;
            }
            else if (gameManager.TimeRemaining > 10 && gameManager.EnemySpawnCount - gameManager.EnemyKillCount < 7)
            {
                var nodes = gridManager.GetUnblockedBorderNodes();
                if (nodes.Count == 0)
                {
                    return; // All nodes are blocked
                }

                var spawnPoint = nodes[Random.Range(0, nodes.Count)].WorldPosition;

                if (Random.Range(1, 8) == 7)
                {
                    ObjectPooler.Instance.GetPooledObjectAndMoveTo(ObjectPooler.ObjectType.MineLayer, spawnPoint);
                }
                else
                {
                    ObjectPooler.Instance.GetPooledObjectAndMoveTo(ObjectPooler.ObjectType.Robot, spawnPoint);
                }

                randomSpawnDelay = Random.Range(0.4f, 3f);
                timeOfLastEnemySpawn = Time.time;
            }
            else if (gameManager.TimeRemaining > 0 && gameManager.EnemySpawnCount - gameManager.EnemyKillCount < 8)
            {
                var nodes = gridManager.GetUnblockedBorderNodes();
                if (nodes.Count == 0)
                {
                    return; // All nodes are blocked
                }

                var spawnPoint = nodes[Random.Range(0, nodes.Count)].WorldPosition;

                if (Random.Range(1, 8) == 7)
                {
                    ObjectPooler.Instance.GetPooledObjectAndMoveTo(ObjectPooler.ObjectType.MineLayer, spawnPoint);
                }
                else
                {
                    ObjectPooler.Instance.GetPooledObjectAndMoveTo(ObjectPooler.ObjectType.Robot, spawnPoint);
                }

                randomSpawnDelay = Random.Range(0.4f, 1f);
                timeOfLastEnemySpawn = Time.time;
            }
        }

        /// <summary>
        /// Level 1-1
        /// </summary>
        private void Map1EnemySpawning()
        {
            if (gameManager.TimeRemaining > 80 && gameManager.EnemySpawnCount - gameManager.EnemyKillCount < 4)
            {
                var nodes = gridManager.GetUnblockedBottomBorderNodes();
                if (nodes.Count > 0)
                {
                    ObjectPooler.Instance.GetPooledObjectAndMoveTo(ObjectPooler.ObjectType.Robot, nodes[Random.Range(0, nodes.Count)].WorldPosition);
                    randomSpawnDelay = Random.Range(1.5f, 2f);
                    timeOfLastEnemySpawn = Time.time;
                }
            }
            else if (gameManager.TimeRemaining > 78 && gameManager.EnemySpawnCount - gameManager.EnemyKillCount < 6)
            {
                var nodes = gridManager.GetUnblockedRightBorderNodes();
                if (nodes.Count > 0)
                {
                    ObjectPooler.Instance.GetPooledObjectAndMoveTo(ObjectPooler.ObjectType.Robot, nodes[Random.Range(0, nodes.Count)].WorldPosition);
                    randomSpawnDelay = Random.Range(0.7f, 1.5f);
                    timeOfLastEnemySpawn = Time.time;
                }
            }
            else if (gameManager.TimeRemaining > 76 && gameManager.EnemySpawnCount - gameManager.EnemyKillCount < 6)
            {
                var nodes = gridManager.GetUnblockedLeftBorderNodes();
                if (nodes.Count > 0)
                {
                    ObjectPooler.Instance.GetPooledObjectAndMoveTo(ObjectPooler.ObjectType.Robot, nodes[Random.Range(0, nodes.Count)].WorldPosition);
                    randomSpawnDelay = Random.Range(0.7f, 0.9f);
                    timeOfLastEnemySpawn = Time.time;
                }
            }
            else if (gameManager.TimeRemaining > 74 && gameManager.EnemySpawnCount - gameManager.EnemyKillCount < 6)
            {
                var nodes = gridManager.GetUnblockedTopBorderNodes();
                if (nodes.Count > 0)
                {
                    ObjectPooler.Instance.GetPooledObjectAndMoveTo(ObjectPooler.ObjectType.Robot, nodes[Random.Range(0, nodes.Count)].WorldPosition);
                    randomSpawnDelay = Random.Range(0.9f, 1.2f);
                    timeOfLastEnemySpawn = Time.time;
                }
            }
            else if (gameManager.TimeRemaining > 69)
            {
                // Wait 5 seconds
            }
            else if (gameManager.TimeRemaining > 45 && gameManager.EnemySpawnCount - gameManager.EnemyKillCount < 6)
            {
                var nodes = gridManager.GetUnblockedBorderNodes();
                if (nodes.Count > 0)
                {
                    ObjectPooler.Instance.GetPooledObjectAndMoveTo(ObjectPooler.ObjectType.Robot, nodes[Random.Range(0, nodes.Count)].WorldPosition);
                    randomSpawnDelay = Random.Range(0.8f, 1.0f);
                    timeOfLastEnemySpawn = Time.time;
                }
            }
            else if (gameManager.TimeRemaining > 40)
            {
                // Wait 5 seconds
            }
            else if (gameManager.TimeRemaining > 30 && gameManager.EnemySpawnCount - gameManager.EnemyKillCount < 6)
            {
                var nodes = gridManager.GetUnblockedBorderNodes();
                if (nodes.Count > 0)
                {
                    ObjectPooler.Instance.GetPooledObjectAndMoveTo(ObjectPooler.ObjectType.Robot, GetRandomUnblockedEdgeWorldCoordinate());
                    randomSpawnDelay = Random.Range(0.4f, 0.9f);
                    timeOfLastEnemySpawn = Time.time;
                }
            }
            else if (gameManager.TimeRemaining > 25)
            {
                // Wait 5 seconds
            }
            else if (gameManager.TimeRemaining > 13 && gameManager.EnemySpawnCount - gameManager.EnemyKillCount < 6)
            {
                var nodes = gridManager.GetUnblockedBorderNodes();
                if (nodes.Count > 0)
                {
                    ObjectPooler.Instance.GetPooledObjectAndMoveTo(ObjectPooler.ObjectType.Robot, GetRandomUnblockedEdgeWorldCoordinate());
                    randomSpawnDelay = Random.Range(0.4f, 0.9f);
                    timeOfLastEnemySpawn = Time.time;
                }
            }
            else if (gameManager.TimeRemaining > 10)
            {
                // Wait
            }
            else if (gameManager.TimeRemaining > 0 && gameManager.EnemySpawnCount - gameManager.EnemyKillCount < 7)
            {
                var nodes = gridManager.GetUnblockedBorderNodes();
                if (nodes.Count > 0)
                {
                    ObjectPooler.Instance.GetPooledObjectAndMoveTo(ObjectPooler.ObjectType.Robot, GetRandomUnblockedEdgeWorldCoordinate());
                    randomSpawnDelay = Random.Range(0.4f, 0.7f);
                    timeOfLastEnemySpawn = Time.time;
                }
            }
        }

        private void SpawnEnemiesForActiveMap()
        {
            if (gameManager.TimeRemaining == 0 && gameManager.EnemyKillCount == gameManager.EnemySpawnCount)
            {
                Progression++;
            }
            else if (Time.time > timeOfLastEnemySpawn + randomSpawnDelay && Time.time > gameManager.TimeOfLastDeathOrBoardSetup + GameManager.SpawnDelayAfterDeathAndScroll && gameManager.GetPlayerComponent().NumLives > 0)
            {
                switch (ActiveBoardNumber)
                {
                    case 1:
                        Map1EnemySpawning();
                        break;
                    case 2:
                        Map2EnemySpawning();
                        break;
                    case 3:
                        Map3EnemySpawning();
                        break;
                    case 4:
                        Map4EnemySpawning();
                        break;
                    case 6:
                        Map6EnemySpawning();
                        break;
                    case 7:
                        Map7EnemySpawning();
                        break;
                    case 8:
                        Map8EnemySpawning();
                        break;
                    case 9:
                        Map9EnemySpawning();
                        break;
                    case 11:
                        Map11EnemySpawning();
                        break;
                    case 12:
                        Map12EnemySpawning();
                        break;
                    case 13:
                        Map13EnemySpawning();
                        break;
                    case 14:
                        Map14EnemySpawning();
                        break;
                    default:
                        throw new System.Exception("Active map number " + ActiveBoardNumber + " not found");
                }
            }
        }
        
        private void SpawnMerchantAndBlinkArrow()
        {
            gameManager.AllowBoardScroll = true;
            gameManager.BlinkingArrowInstance = GameObject.Instantiate(gameManager.BlinkingArrowPrefab, gridManager.GetWorldCoordinatesFromNodeCoordinates(new Vector2(8, 15)), Quaternion.identity);
            var merchant = gameManager.MerchantInstance = GameObject.Instantiate(gameManager.MerchantPrefab, gridManager.GetWorldCoordinatesFromNodeCoordinates(new Vector2(8, 1)), Quaternion.identity);
            merchant.GetComponent<Merchant>().MoveToGridCord = new Vector2(8, 7);
            Progression++;
        }

        private void BlinkArrowAndAllowBoardScroll()
        {
            gameManager.AllowBoardScroll = true;
            gameManager.BlinkingArrowInstance = GameObject.Instantiate(gameManager.BlinkingArrowPrefab, gridManager.GetWorldCoordinatesFromNodeCoordinates(new Vector2(8, 15)), Quaternion.identity);
            Progression++;
        }

        private Vector2 GetRandomUnblockedEdgeWorldCoordinate()
        {
            List<Node> unblockedNodes = gridManager.GetUnblockedBorderNodes();
            return unblockedNodes[Random.Range(0, unblockedNodes.Count)].WorldPosition;
        }

        private Vector2 GetRandomEdgeWorldCoordinateForFlying()
        {
            List<Node> allBorderNodes = gridManager.GetAllBorderNodes();
            return allBorderNodes[Random.Range(0, allBorderNodes.Count)].WorldPosition;
        }

        private void WaitForClickToStartGame()
        {
            if (Input.GetMouseButtonDown(0) || Input.anyKey)
            {
                Progression++;
                audioManager.Play(AudioManager.WarhoundSounds.blip);
                audioManager.Play(AudioManager.WarhoundSounds.music_warhound);
            }
        }

        private void SetTimerToNinetySeconds()
        {
            gameManager.TimeRemaining = 90;
            Progression++;
        }
    }
}