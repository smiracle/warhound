﻿using System.Collections.Generic;
using UnityEngine;

namespace Assets.Game.Scripts.EnemyScripts
{
    public class TilePooler : MonoBehaviour
    {
        public GameObject WalkableTilePrefab;
        public GameObject UnwalkableTilePrefab;
        public GameObject BlockerTilePrefab;
        
        public Dictionary<string, Sprite> SpritesDictionary = new Dictionary<string, Sprite>();
        public Dictionary<string, RuntimeAnimatorController> RuntimeAnimatorControllerDictionary = new Dictionary<string, RuntimeAnimatorController>();

        [HideInInspector]
        public GameObject PooledTileParent;

        [ShowOnly]
        private Dictionary<TileType, List<GameObject>> pooledTilesByType = new Dictionary<TileType, List<GameObject>>();

        public enum TileType
        {
            Grass,
            Grass2,
            Grass3,
            River,
            Wall,
            TreeTop,
            TreeBottom,
            TreeOverlap,
            BurningTree,
            CarTopLeft,
            CarTop,
            CarTopRight,
            CarBottomRight,
            CarBottom,
            CarBottomLeft,
            Bridge,
            Manhole,
            SewerPipe,
            Brick,
            ConcreteTopBorder,
            ConcreteBottomBorder,
            ConcreteLeftBorder,
            ConcreteRightBorder,
            ConcreteTopRightBorder,
            ConcreteTopLeftBorder,
            ConcreteBottomRightBorder,
            ConcreteBottomLeftBorder,
            Concrete,
            SewerWater,
            Metal1,
            Metal2,
            Metal3,
            Metal4,
            Metal5,
            Metal6,
            Metal7,
            Metal8,
            Metal9,
            Metal10,
            Metal11,
            MetalHole,
            MetalWall,
            RedEyes,
            RedLight,
            Data,
            SewerLadder,
            LabLadder,
            Chasm,
            RedEyesChasm,
            BrainJar,
            InactiveRobot,
            RoadTop,
            RoadBottom
        }

        public GameObject GetPooledTileAndMoveTo(TileType tileType, Vector2 moveTo, Transform boardParent)
        {
            if (!pooledTilesByType.ContainsKey(tileType))
            {
                throw new System.Exception("Could not find tile type " + tileType + " in pooledTilesByType dictionary");
            }

            for (int i = 0; i < pooledTilesByType[tileType].Count; i++)
            {
                if (!pooledTilesByType[tileType][i].activeInHierarchy)
                {
                    pooledTilesByType[tileType][i].SetActive(true);
                    pooledTilesByType[tileType][i].transform.position = moveTo;
                    pooledTilesByType[tileType][i].transform.SetParent(boardParent);
                    return pooledTilesByType[tileType][i];
                }
            }

            GameObject obj = CreateAndReturnNewObject(tileType, boardParent);
            obj.transform.position = moveTo;
            obj.SetActive(true);
            return obj;
        }

        public void DeactivateAllTilesForBoard(Transform boardTransform)
        {
            while (boardTransform.childCount > 0)
            {
                var child = boardTransform.GetChild(0);
                child.SetParent(PooledTileParent.transform);
                
                // Reset any deactivated edge colliders
                foreach (EdgeCollider2D e in child.GetComponents<EdgeCollider2D>())
                {
                    e.enabled = true;
                }

                child.gameObject.SetActive(false);
            }
        }

        private void Awake()
        {
            Sprite[] sprites = Resources.LoadAll<Sprite>("Sprites");
            foreach (Sprite sprite in sprites)
            {
                SpritesDictionary.Add(sprite.name, sprite);
            }

            RuntimeAnimatorController[] animatorControllers = Resources.LoadAll<RuntimeAnimatorController>("AnimatorControllers");
            foreach (RuntimeAnimatorController animator in animatorControllers)
            {
                RuntimeAnimatorControllerDictionary.Add(animator.name, animator);
            }

            PooledTileParent = new GameObject("PooledTileParent");
            pooledTilesByType.Add(TileType.Grass, new List<GameObject>());
            pooledTilesByType.Add(TileType.Grass2, new List<GameObject>());
            pooledTilesByType.Add(TileType.Grass3, new List<GameObject>());
            pooledTilesByType.Add(TileType.River, new List<GameObject>());
            pooledTilesByType.Add(TileType.Wall, new List<GameObject>());
            pooledTilesByType.Add(TileType.TreeTop, new List<GameObject>());
            pooledTilesByType.Add(TileType.TreeBottom, new List<GameObject>());
            pooledTilesByType.Add(TileType.TreeOverlap, new List<GameObject>());
            pooledTilesByType.Add(TileType.BurningTree, new List<GameObject>());
            pooledTilesByType.Add(TileType.CarTopLeft, new List<GameObject>());
            pooledTilesByType.Add(TileType.CarTop, new List<GameObject>());
            pooledTilesByType.Add(TileType.CarTopRight, new List<GameObject>());
            pooledTilesByType.Add(TileType.CarBottomRight, new List<GameObject>());
            pooledTilesByType.Add(TileType.CarBottom, new List<GameObject>());
            pooledTilesByType.Add(TileType.CarBottomLeft, new List<GameObject>());
            pooledTilesByType.Add(TileType.Bridge, new List<GameObject>());
            pooledTilesByType.Add(TileType.Manhole, new List<GameObject>());
            pooledTilesByType.Add(TileType.SewerPipe, new List<GameObject>());
            pooledTilesByType.Add(TileType.Brick, new List<GameObject>());
            pooledTilesByType.Add(TileType.Concrete, new List<GameObject>());
            pooledTilesByType.Add(TileType.ConcreteTopBorder, new List<GameObject>());
            pooledTilesByType.Add(TileType.ConcreteBottomBorder, new List<GameObject>());
            pooledTilesByType.Add(TileType.ConcreteLeftBorder, new List<GameObject>());
            pooledTilesByType.Add(TileType.ConcreteRightBorder, new List<GameObject>());
            pooledTilesByType.Add(TileType.ConcreteTopRightBorder, new List<GameObject>());
            pooledTilesByType.Add(TileType.ConcreteTopLeftBorder, new List<GameObject>());
            pooledTilesByType.Add(TileType.ConcreteBottomRightBorder, new List<GameObject>());
            pooledTilesByType.Add(TileType.ConcreteBottomLeftBorder, new List<GameObject>());
            pooledTilesByType.Add(TileType.SewerWater, new List<GameObject>());

            pooledTilesByType.Add(TileType.Metal1, new List<GameObject>());
            pooledTilesByType.Add(TileType.Metal2, new List<GameObject>());
            pooledTilesByType.Add(TileType.Metal3, new List<GameObject>());
            pooledTilesByType.Add(TileType.Metal4, new List<GameObject>());
            pooledTilesByType.Add(TileType.Metal5, new List<GameObject>());
            pooledTilesByType.Add(TileType.Metal6, new List<GameObject>());
            pooledTilesByType.Add(TileType.Metal7, new List<GameObject>());
            pooledTilesByType.Add(TileType.Metal8, new List<GameObject>());
            pooledTilesByType.Add(TileType.Metal9, new List<GameObject>());
            pooledTilesByType.Add(TileType.Metal10, new List<GameObject>());
            pooledTilesByType.Add(TileType.Metal11, new List<GameObject>());
            pooledTilesByType.Add(TileType.MetalHole, new List<GameObject>());
            pooledTilesByType.Add(TileType.MetalWall, new List<GameObject>());
            pooledTilesByType.Add(TileType.RedEyes, new List<GameObject>());
            pooledTilesByType.Add(TileType.RedLight, new List<GameObject>());
            pooledTilesByType.Add(TileType.Data, new List<GameObject>());
            pooledTilesByType.Add(TileType.SewerLadder, new List<GameObject>());
            pooledTilesByType.Add(TileType.LabLadder, new List<GameObject>());
            pooledTilesByType.Add(TileType.Chasm, new List<GameObject>());
            pooledTilesByType.Add(TileType.RedEyesChasm, new List<GameObject>());
            pooledTilesByType.Add(TileType.BrainJar, new List<GameObject>());
            pooledTilesByType.Add(TileType.InactiveRobot, new List<GameObject>());

            pooledTilesByType.Add(TileType.RoadTop, new List<GameObject>());
            pooledTilesByType.Add(TileType.RoadBottom, new List<GameObject>());

            // Set initial pooling amounts here
            PoolObjectMultipleTimes(TileType.Grass, 10, PooledTileParent.transform);
            PoolObjectMultipleTimes(TileType.Grass2, 10, PooledTileParent.transform);
            PoolObjectMultipleTimes(TileType.Grass3, 10, PooledTileParent.transform);
            PoolObjectMultipleTimes(TileType.TreeTop, 10, PooledTileParent.transform);
            PoolObjectMultipleTimes(TileType.TreeBottom, 10, PooledTileParent.transform);
            PoolObjectMultipleTimes(TileType.TreeOverlap, 10, PooledTileParent.transform);
        }

        private void PoolObjectMultipleTimes(TileType tileType, int count, Transform boardParent)
        {
            for (int i = 0; i < count; i++)
            {
                var newObject = CreateAndReturnNewObject(tileType, boardParent);
                newObject.SetActive(false);
            }
        }

        private GameObject CreateAndReturnNewObject(TileType tileType, Transform boardParent)
        {
            switch (tileType)
            {
                // Walkable
                case TileType.Grass:
                case TileType.Grass2:
                case TileType.Grass3:
                case TileType.Bridge:
                case TileType.Manhole:
                case TileType.Concrete:
                case TileType.ConcreteTopBorder:
                case TileType.ConcreteBottomBorder:
                case TileType.ConcreteLeftBorder:
                case TileType.ConcreteRightBorder:
                case TileType.ConcreteTopRightBorder:
                case TileType.ConcreteTopLeftBorder:
                case TileType.ConcreteBottomRightBorder:
                case TileType.ConcreteBottomLeftBorder:
                case TileType.Metal1:
                case TileType.Metal2:
                case TileType.Metal3:
                case TileType.Metal4:
                case TileType.Metal5:
                case TileType.Metal6:
                case TileType.Metal7:
                case TileType.Metal8:
                case TileType.Metal9:
                case TileType.Metal10:
                case TileType.Metal11:                
                case TileType.RedLight:
                case TileType.Data:
                case TileType.RoadTop:
                case TileType.RoadBottom:
                    GameObject walkableObj = (GameObject)Instantiate(WalkableTilePrefab);
                    SetTileSpriteAndAnimation(walkableObj, tileType);
                    walkableObj.transform.SetParent(boardParent);
                    pooledTilesByType[tileType].Add(walkableObj);
                    return walkableObj;

                // Unwalkable (but not a blocker)
                case TileType.MetalHole:
                case TileType.River:
                case TileType.SewerWater:
                case TileType.RedEyes:
                case TileType.Chasm:
                case TileType.RedEyesChasm:
                    GameObject unwalkableObj = (GameObject)Instantiate(UnwalkableTilePrefab);
                    SetTileSpriteAndAnimation(unwalkableObj, tileType);
                    unwalkableObj.transform.SetParent(boardParent);
                    pooledTilesByType[tileType].Add(unwalkableObj);
                    return unwalkableObj;

                // Blocker
                case TileType.Wall:
                case TileType.TreeTop:
                case TileType.TreeBottom:
                case TileType.TreeOverlap:
                case TileType.BurningTree:
                case TileType.CarTopLeft:
                case TileType.CarTop:
                case TileType.CarTopRight:
                case TileType.CarBottomRight:
                case TileType.CarBottom:
                case TileType.CarBottomLeft:
                case TileType.SewerPipe:
                case TileType.Brick:
                case TileType.MetalWall:
                case TileType.SewerLadder:
                case TileType.LabLadder:
                case TileType.BrainJar:
                case TileType.InactiveRobot:
                    GameObject blockerObj = (GameObject)Instantiate(BlockerTilePrefab);
                    SetTileSpriteAndAnimation(blockerObj, tileType);
                    blockerObj.transform.SetParent(boardParent);
                    pooledTilesByType[tileType].Add(blockerObj);
                    return blockerObj;
                default:
                    throw new System.Exception("CreateAndReturnNewObject invalid tile type");
            }
        }

        private void SetTileSpriteAndAnimation(GameObject obj, TileType type)
        {
            obj.name = type.ToString();
            if (SpritesDictionary.ContainsKey(type.ToString()))
            {
                obj.GetComponent<SpriteRenderer>().sprite = SpritesDictionary[type.ToString()];
            }

            if (RuntimeAnimatorControllerDictionary.ContainsKey(type.ToString()))
            {
                obj.AddComponent<Animator>();
                obj.GetComponent<Animator>().runtimeAnimatorController = RuntimeAnimatorControllerDictionary[type.ToString()];
            }
            else
            {
                // Debug.Log(type + " animator controller not found in resources");
            }

            if (type == TileType.Manhole)
            {
                obj.AddComponent<Manhole>();
                obj.AddComponent<BoxCollider2D>();
                obj.GetComponent<BoxCollider2D>().isTrigger = true;
                obj.GetComponent<BoxCollider2D>().size = new Vector2(21f, 21f);
            }

            if (type == TileType.Bridge)
            {
                obj.GetComponent<SpriteRenderer>().sortingOrder = 1;
            }

            return;
        }
    }
}