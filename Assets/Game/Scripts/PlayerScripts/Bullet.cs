﻿using Assets.Game.Scripts.Map;
using UnityEngine;

namespace Assets.Game.Scripts.PlayerScripts
{
    public class Bullet : MonoBehaviour
    {
        [ShowOnly]
        public int Hp;
        protected Vector3 moveDirection;
        private Player player;

        public void SetMoveDirection(Vector3 moveDir)
        {
            moveDirection = moveDir;
        }

        public void DestroyBullet()
        {            
            gameObject.SetActive(false);
        }

        protected void Update()
        {
            if (player.FreezeMotion)
            {
                gameObject.SetActive(false);
            }

            if (gameObject.transform.position.x > GameManager.GameAreaSize + GameManager.GameAreaOffset 
                || gameObject.transform.position.x < -GameManager.GameAreaSize + GameManager.GameAreaOffset 
                || gameObject.transform.position.y > GameManager.GameAreaSize + GameManager.GameAreaOffset 
                || gameObject.transform.position.y < -GameManager.GameAreaSize + GameManager.GameAreaOffset)
            {
                // Remove bullet if out of bounds
                DestroyBullet();
            }

            transform.Translate(moveDirection.x * Time.deltaTime, moveDirection.y * Time.deltaTime, 0);            
        }

        protected void OnEnable()
        {
            if (player == null)
            {
                player = GameObject.FindObjectOfType<GameManager>().GetPlayerComponent();
            }

            Hp = player.GunLevel + 1;
            Invoke("DestroyBullet", 3f);
        }

        protected void Awake()
        {
            player = GameObject.FindObjectOfType<GameManager>().GetPlayerComponent();
        }

        protected void OnDisable()
        {
            Hp = 0;
            CancelInvoke();
        }
    }
}