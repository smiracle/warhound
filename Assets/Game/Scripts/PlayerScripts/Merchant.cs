﻿using Assets.Game.Scripts.Map;
using UnityEngine;

namespace Assets.Game.Scripts.PlayerScripts
{
    public class Merchant : MonoBehaviour
    {
        public Vector2 MoveToGridCord;
        private Animator animator;
        private GameManager gameManager;
        private GridManager gridManager;
        private Player player;
        private AudioManager audioManager;

        private void Awake()
        {
            gridManager = GameObject.FindObjectOfType<GridManager>();
        }

        private void Start()
        {
            animator = GetComponent<Animator>();
            gameManager = GameObject.FindObjectOfType<GameManager>();
            player = GameObject.FindObjectOfType<Player>();
            audioManager = GameObject.FindObjectOfType<AudioManager>();
        }

        private void Update()
        {
            if (MoveToGridCord != Vector2.zero)
            {
                if (Vector2.Distance(transform.position, gridManager.GetWorldCoordinatesFromNodeCoordinates(MoveToGridCord)) < 0.1f)
                {
                    animator.SetBool("IsMoving", false);
                    MoveToGridCord = Vector2.zero;
                    RevealMat();
                }
                else
                {
                    animator.SetBool("IsMoving", true);
                    transform.position = Vector3.MoveTowards(transform.position, gridManager.GetWorldCoordinatesFromNodeCoordinates(MoveToGridCord), 70f * Time.deltaTime);
                }
            }
        }

        private void RevealMat()
        {
            audioManager.Play(AudioManager.WarhoundSounds.blip);
            gameManager.MerchantMatInstance = GameObject.Instantiate(gameManager.MerchantMatPrefab);
            gameManager.MerchantMatInstance.transform.position = new Vector2(transform.position.x, transform.position.y - 27f);

            if (player.BootLevel < 3)
            {
                gameManager.BootPowerupInstance = GameObject.Instantiate(gameManager.BootPowerupPrefabArray[player.BootLevel]);
                gameManager.BootPowerupInstance.transform.position = new Vector2(transform.position.x - 25f, transform.position.y - 23f);
                gameManager.BootCostText.text = gameManager.BootCosts[player.BootLevel].ToString().Trim();
            }
            else
            {
                gameManager.BootCostText.text = string.Empty;
            }

            if (player.GunLevel < 3)
            {
                gameManager.GunPowerupInstance = GameObject.Instantiate(gameManager.GunPowerupPrefabArray[player.GunLevel]);
                gameManager.GunPowerupInstance.transform.position = new Vector2(transform.position.x, transform.position.y - 23f);
                gameManager.GunCostText.text = gameManager.GunCosts[player.GunLevel].ToString().Trim();
            }
            else
            {
                gameManager.GunCostText.text = string.Empty;
            }

            if (player.AmmoLevel < 3)
            {
                gameManager.AmmoPowerupInstance = GameObject.Instantiate(gameManager.AmmoPowerupPrefabArray[player.AmmoLevel]);
                gameManager.AmmoPowerupInstance.transform.position = new Vector2(transform.position.x + 25f, transform.position.y - 23f);
                gameManager.AmmoCostText.text = gameManager.AmmoCosts[player.AmmoLevel].ToString().Trim();                
            }
            else
            {
                gameManager.AmmoCostText.text = string.Empty;
            }
                       
            gameManager.PowerupCostParent.SetActive(true);
        }
    }
}
