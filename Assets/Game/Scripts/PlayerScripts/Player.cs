﻿using System.Collections;
using System.Collections.Generic;
using Assets.Game.Scripts.EnemyScripts;
using Assets.Game.Scripts.Map;
using UnityEngine;

namespace Assets.Game.Scripts.PlayerScripts
{
    public class Player : MonoBehaviour
    {
        [ShowOnly]
        public const float BaseShootingRate = 0.65f; // Part of calculations
        [ShowOnly]
        public float ShootingRate; // Current real-time firing rate, higher number means slower shooting rate                
        [ShowOnly]
        public float MoveSpeed;
        [ShowOnly]
        public int BootLevel;
        [ShowOnly]
        public int GunLevel;
        [ShowOnly]
        public int AmmoLevel;
        [ShowOnly]
        public bool HasEnemyDestinationChanged;
        public bool FreezeMotion;
        [HideInInspector]
        public StoredPowerups StoredPowerup;
        [HideInInspector]
        public ProgressionManager ProgressionManager;
        [HideInInspector]
        public float LastShot;
        [ShowOnly]
        public Node PreviousNode;
        [ShowOnly]
        public int NumEnemiesThatStillNeedToRecalculatePaths;
        [ShowOnly]
        public bool DoneRecalculatingAllPaths = true;
        private const float BulletMoveSpeed = 17.6f; // Constant movement speed of bullets
        private float machineGunFireRateBoost = 0.225f;
        private float passiveFireRateBoost = 0.1f; // Passive shooting speed increase gained from each gun powerup
        private float baseMoveSpeed = 1.7f;
        private float passiveMoveSpeedBoost = 0.30f; // Boost from boots
        private Animator animator;
        private List<PlayerState> activeStates;
        private int previousStateCount;        
        private int numberLives;
        private int numberDollars;
        private GameManager gameManager;
        private PlayerKeyboardControl keyboardControl;
        private GridManager gridManager;
        private AudioManager audioManager;

        public int NumDollars
        {
            get
            {
                return numberDollars;
            }

            set
            {
                numberDollars = value;
                gameManager.NumDollarsText.text = numberDollars.ToString("D2");
            }
        }

        public int NumLives
        {
            get
            {
                return numberLives;
            }

            set
            {
                numberLives = value;
                gameManager.NumLivesText.text = numberLives.ToString("D2");
            }
        }

        public void Update()
        {
            if (FreezeMotion)
            {
                return;
            }

            UpdateMoveDirection();
            UpdatePlayerState();
            animator.SetBool("IsMoving", keyboardControl.IsMoving());

            if (transform.position.y <= -GameManager.GameAreaSize + GameManager.GameAreaOffset && gameManager.AllowBoardScroll && !gameManager.HasPlayerReachedBoundary)
            {
                gameManager.HasPlayerReachedBoundary = true;
            }
            else
            {
                // Prevent player from leaving the boundary area
                transform.position = new Vector3(
                    Mathf.Clamp(transform.position.x, -GameManager.GameAreaSize + GameManager.GameAreaOffset, GameManager.GameAreaSize + GameManager.GameAreaOffset),
                    Mathf.Clamp(transform.position.y, -GameManager.GameAreaSize + GameManager.GameAreaOffset, GameManager.GameAreaSize + GameManager.GameAreaOffset),
                    transform.position.z);
            }

            var currentNode = gridManager.NodeFromWorldPoint(transform.position);
            NumEnemiesThatStillNeedToRecalculatePaths = gameManager.EnemySpawnCount - gameManager.EnemyKillCount;
            if (currentNode != PreviousNode)
            {
                PreviousNode = currentNode;
                HasEnemyDestinationChanged = true;
                DoneRecalculatingAllPaths = false;
            }
            else if (gameManager.EnemySpawnCount - gameManager.EnemyKillCount == NumEnemiesThatStillNeedToRecalculatePaths)
            {
                // If pathing has been recalculated for every active enemy
                HasEnemyDestinationChanged = false;
                DoneRecalculatingAllPaths = true;
            }
        }

        public void FaceDown()
        {
            keyboardControl.FaceDown();
        }

        public void ConsumePowerup(StoredPowerups powerupToConsume, bool isStored)
        {
            if (powerupToConsume == StoredPowerups.NONE && isStored)
            {
                return;
            }

            switch (powerupToConsume)
            {
                case StoredPowerups.MACHINE_GUN:
                    activeStates.Add(new PlayerState(StateType.MACHINE_GUN, Time.time));
                    audioManager.Play(AudioManager.WarhoundSounds.powerupcollect3);
                    break;
                case StoredPowerups.SHOTGUN:
                    activeStates.Add(new PlayerState(StateType.SPREAD_SHOT, Time.time));
                    audioManager.Play(AudioManager.WarhoundSounds.powerupcollect3);
                    break;
                case StoredPowerups.CIRCLE_SHOT:
                    activeStates.Add(new PlayerState(StateType.CIRCLE_SHOT, Time.time));
                    audioManager.Play(AudioManager.WarhoundSounds.powerupcollect3);
                    break;
                case StoredPowerups.GRENADE:
                    for (int i = 0; i < 15; i++)
                    {
                        ObjectPooler.Instance.GetPooledObjectAndMoveTo(ObjectPooler.ObjectType.Explosion, gridManager.RandomPosition());
                        audioManager.Play(AudioManager.WarhoundSounds.grenade);

                        // Kill eligible enemies
                        ObjectPooler.Instance.DeactivateAllEnemiesAffectedByGrenadeFromBoard();
                    }

                    break;
                case StoredPowerups.MEDAL:
                    activeStates.Add(new PlayerState(StateType.BERSERK, Time.time));
                    audioManager.Play(AudioManager.WarhoundSounds.powerupcollect3);
                    break;
            }

            if (isStored)
            {
                StoredPowerup = StoredPowerups.NONE;
                gameManager.SetStoredPowerupIcon(StoredPowerups.NONE);
            }
        }

        public bool HasActiveState(StateType state)
        {
            if (activeStates != null)
            {
                foreach (PlayerState activeState in activeStates)
                {
                    if (activeState.GetStateType() == state)
                    {
                        return true;
                    }
                }
            }

            return false;
        }
        
        public void HandlePlayerHit()
        {
            audioManager.Play(AudioManager.WarhoundSounds.grenade2);
            FreezeMotion = true;
            gameManager.TimeOfLastDeathOrBoardSetup = Time.time;
            if (!gameManager.IgnoreTimeIncreases)
            {
                gameManager.TimeRemaining += 10;
            }

            ObjectPooler.Instance.DeactivateAllEnemiesAndPowerupsForPlayerHitEvent();
            StoredPowerup = StoredPowerups.NONE;
            gameManager.SetStoredPowerupIcon(StoredPowerups.NONE);
            gameManager.GetPlayerComponent().NumLives--;
            animator.SetBool("IsDead", true);
            StartCoroutine("FinishDeathAnimation");
            if (gameManager.IsBossActive)
            {
                audioManager.Stop(AudioManager.WarhoundSounds.music_boss);
            }
            else
            {
                audioManager.Stop(AudioManager.WarhoundSounds.music_warhound);
            }
            audioManager.Play(AudioManager.WarhoundSounds.music_gameover);
        }

        public IEnumerator FinishDeathAnimation()
        {
            yield return new WaitForSeconds(1.5f);
            GetComponent<SpriteRenderer>().enabled = false;
            animator.SetBool("IsDead", false);
            FaceDown();
            yield return new WaitForSeconds(1f);
            if (NumLives <= 0)
            {
                audioManager.Stop(AudioManager.WarhoundSounds.music_boss);
                audioManager.Stop(AudioManager.WarhoundSounds.music_warhound);
                gameManager.GameOver();
                yield break;
            }

            GetComponent<SpriteRenderer>().enabled = true;
            activeStates.Clear();
            yield return new WaitForSeconds(0.2f);
            FreezeMotion = false;
            if (gameManager.IsBossActive)
            {
                audioManager.Play(AudioManager.WarhoundSounds.music_boss);
            }
            else
            {
                audioManager.Play(AudioManager.WarhoundSounds.music_warhound);
            }
            yield break;
        }

        public void ShootCircle()
        {
            ShootRight();
            ShootUp();
            ShootLeft();
            ShootDown();
            ShootUpRight();
            ShootUpLeft();
            ShootDownLeft();
            ShootDownRight();
        }

        public void ShootShotgunCircle()
        {
            ShotgunShootRight();
            ShotgunShootUp();
            ShotgunShootLeft();
            ShotgunShootDown();
            ShotgunShootUpRight();
            ShotgunShootUpLeft();
            ShotgunShootDownLeft();
            ShotgunShootDownRight();
        }

        public void ShootUp()
        {
            ShootBullet(new Vector2(0.0f, 1.0f), new Vector3(0f, 10f, 0f) * BulletMoveSpeed);
        }

        public void ShootLeft()
        {
            ShootBullet(new Vector2(-1.0f, 0.0f), new Vector3(-10f, 0.0f, 0f) * BulletMoveSpeed);
        }

        public void ShootRight()
        {
            ShootBullet(new Vector2(1.0f, 0.0f), new Vector3(10f, 0.0f, 0f) * BulletMoveSpeed);
        }

        public void ShootDown()
        {
            ShootBullet(new Vector2(0.0f, -1.0f), new Vector3(0.0f, -10f, 0f) * BulletMoveSpeed);
        }

        public void ShootDownLeft()
        {
            ShootBullet(new Vector2(0.0f, -1.0f), new Vector3(-5f, -5f, 0f) * BulletMoveSpeed);
        }

        public void ShootDownRight()
        {
            ShootBullet(new Vector2(0.0f, -1.0f), new Vector3(5f, -5f, 0f) * BulletMoveSpeed);
        }

        public void ShootUpLeft()
        {
            ShootBullet(new Vector2(0.0f, 1.0f), new Vector3(-5f, 5f, 0f) * BulletMoveSpeed);
        }

        public void ShootUpRight()
        {
            ShootBullet(new Vector2(0.0f, 1.0f), new Vector3(5f, 5f, 0f) * BulletMoveSpeed);
        }

        public void ShotgunShootUp()
        {
            ShootBullet(new Vector2(0.0f, 1.0f), new Vector3(0f, 10f, 0f) * BulletMoveSpeed);
            ShootBullet(new Vector2(0.0f, 1.0f), new Vector3(-2.5f, 10f, 0f) * BulletMoveSpeed);
            ShootBullet(new Vector2(0.0f, 1.0f), new Vector3(2.5f, 10f, 0f) * BulletMoveSpeed);
        }

        public void ShotgunShootRight()
        {
            ShootBullet(new Vector2(1.0f, 0.0f), new Vector3(10f, 0.0f, 0f) * BulletMoveSpeed);
            ShootBullet(new Vector2(1.0f, 0.0f), new Vector3(10f, 2.5f, 0f) * BulletMoveSpeed);
            ShootBullet(new Vector2(1.0f, 0.0f), new Vector3(10f, -2.5f, 0f) * BulletMoveSpeed);
        }

        public void ShotgunShootLeft()
        {
            ShootBullet(new Vector2(-1.0f, 0.0f), new Vector3(-10f, 0.0f, 0f) * BulletMoveSpeed);
            ShootBullet(new Vector2(-1.0f, 0.0f), new Vector3(-10f, 2.5f, 0f) * BulletMoveSpeed);
            ShootBullet(new Vector2(-1.0f, 0.0f), new Vector3(-10f, -2.5f, 0f) * BulletMoveSpeed);
        }

        public void ShotgunShootDown()
        {
            ShootBullet(new Vector2(0.0f, -1.0f), new Vector3(0.0f, -10f, 0f) * BulletMoveSpeed);
            ShootBullet(new Vector2(0.0f, -1.0f), new Vector3(-2.5f, -10f, 0f) * BulletMoveSpeed);
            ShootBullet(new Vector2(0.0f, -1.0f), new Vector3(2.5f, -10f, 0f) * BulletMoveSpeed);
        }

        public void ShotgunShootDownLeft()
        {
            ShootBullet(new Vector2(0.0f, -1.0f), new Vector3(-5f, -5f, 0f) * BulletMoveSpeed);
            ShootBullet(new Vector2(0.0f, -1.0f), new Vector3(-5f, -2.5f, 0f) * BulletMoveSpeed);
            ShootBullet(new Vector2(0.0f, -1.0f), new Vector3(-2.5f, -5f, 0f) * BulletMoveSpeed);
        }

        public void ShotgunShootDownRight()
        {
            ShootBullet(new Vector2(0.0f, -1.0f), new Vector3(5f, -5f, 0f) * BulletMoveSpeed);
            ShootBullet(new Vector2(0.0f, -1.0f), new Vector3(5f, -2.5f, 0f) * BulletMoveSpeed);
            ShootBullet(new Vector2(0.0f, -1.0f), new Vector3(2.5f, -5f, 0f) * BulletMoveSpeed);
        }

        public void ShotgunShootUpLeft()
        {
            ShootBullet(new Vector2(0.0f, 1.0f), new Vector3(-5f, 5f, 0f) * BulletMoveSpeed);
            ShootBullet(new Vector2(0.0f, 1.0f), new Vector3(-2.5f, 5f, 0f) * BulletMoveSpeed);
            ShootBullet(new Vector2(0.0f, 1.0f), new Vector3(-5f, 2.5f, 0f) * BulletMoveSpeed);
        }

        public void ShotgunShootUpRight()
        {
            ShootBullet(new Vector2(0.0f, 1.0f), new Vector3(5f, 5f, 0f) * BulletMoveSpeed);
            ShootBullet(new Vector2(0.0f, 1.0f), new Vector3(5f, 2.5f, 0f) * BulletMoveSpeed);
            ShootBullet(new Vector2(0.0f, 1.0f), new Vector3(2.5f, 5f, 0f) * BulletMoveSpeed);
        }

        private void UpdatePlayerState()
        {
            if (activeStates == null)
            {
                return;
            }

            for (int i = activeStates.Count - 1; i >= 0; i--)
            {
                if (activeStates[i].HasStateDurationExpired(Time.time))
                {
                    if (activeStates[i].GetStateType() == StateType.BERSERK)
                    {
                        animator.SetBool("IsBerserk", false);
                    }

                    activeStates.Remove(activeStates[i]);
                }
            }

            if (previousStateCount == activeStates.Count)
            {
                return; // Don't process state changes if active states were not changed
            }

            previousStateCount = activeStates.Count;

            if (HasActiveState(StateType.BERSERK))
            {
                animator.SetBool("IsBerserk", true);
            }

            RecalculateShootingRate();
        }

        private void Awake()
        {
            animator = GetComponent<Animator>();
            activeStates = new List<PlayerState>();
            gridManager = GameObject.FindObjectOfType<GridManager>();
            keyboardControl = GetComponent<PlayerKeyboardControl>();
            gameManager = GameObject.FindObjectOfType<GameManager>();
            ProgressionManager = gameManager.ProgressionManager;
            audioManager = GameObject.FindObjectOfType<AudioManager>();
        }

        private void Start()
        {
            MoveSpeed = baseMoveSpeed;
            ShootingRate = 1 - BaseShootingRate;
        }

        private void RecalculateShootingRate()
        {
            var baseRate = 1 - BaseShootingRate;
            var ammoBoost = AmmoLevel * passiveFireRateBoost;
            var machineGunBoost = HasActiveState(StateType.MACHINE_GUN) ? machineGunFireRateBoost : 0;
            ShootingRate = Mathf.Clamp(baseRate - ammoBoost - machineGunBoost, 0.05f, 1f);
        }

        private void ShootBullet(Vector2 animatorDirection, Vector3 moveDirection)
        {
            animator.SetBool("IsAttacking", true);
            animator.SetFloat("DirectionX", animatorDirection.x);
            animator.SetFloat("DirectionY", animatorDirection.y);

            GameObject bullet = ObjectPooler.Instance.GetPooledObjectAndMoveTo(ObjectPooler.ObjectType.Bullet, transform.position);
            bullet.GetComponent<Bullet>().SetMoveDirection(moveDirection);
            LastShot = Time.time;
            audioManager.Play(AudioManager.WarhoundSounds.bullet);
        }

        private void UpdateMoveDirection()
        {
            Vector3 direction = keyboardControl.GetFacingDirection();
            if (direction != Vector3.zero)
            {
                if (!Input.GetKey(KeyCode.LeftArrow)
                && !Input.GetKey(KeyCode.RightArrow)
                && !Input.GetKey(KeyCode.UpArrow)
                && !Input.GetKey(KeyCode.DownArrow))
                {
                    if (direction.x != 1 || direction.y != 1)
                    {
                        animator.SetFloat("DirectionX", direction.x);
                        animator.SetFloat("DirectionY", direction.y);
                    }
                }
            }
        }

        private void OnTriggerEnter2D(Collider2D other)
        {
            if (other.tag == "Powerup_MachineGun")
            {
                StoredPowerup = StoredPowerups.MACHINE_GUN;
                gameManager.SetStoredPowerupIcon(StoredPowerups.MACHINE_GUN);
                other.gameObject.SetActive(false);
                audioManager.Play(AudioManager.WarhoundSounds.powerupcollect);
            }
            else if (other.tag == "Powerup_Shotgun")
            {
                StoredPowerup = StoredPowerups.SHOTGUN;
                gameManager.SetStoredPowerupIcon(StoredPowerups.SHOTGUN);
                other.gameObject.SetActive(false);
                audioManager.Play(AudioManager.WarhoundSounds.powerupcollect);
            }
            else if (other.tag == "Powerup_Circle")
            {
                StoredPowerup = StoredPowerups.CIRCLE_SHOT;
                gameManager.SetStoredPowerupIcon(StoredPowerups.CIRCLE_SHOT);
                other.gameObject.SetActive(false);
                audioManager.Play(AudioManager.WarhoundSounds.powerupcollect);
            }
            else if (other.tag == "Powerup_Helmet")
            {
                gameManager.GetPlayerComponent().NumLives++;
                other.gameObject.SetActive(false);
                audioManager.Play(AudioManager.WarhoundSounds.powerupcollect);
            }
            else if (other.tag == "Powerup_Dollar")
            {
                gameManager.GetPlayerComponent().NumDollars++;
                other.gameObject.SetActive(false);
                audioManager.Play(AudioManager.WarhoundSounds.powerupcollect);
            }
            else if (other.tag == "Powerup_Boots" && numberDollars >= gameManager.BootCosts[BootLevel])
            {
                gameManager.PassiveBootImage.enabled = true;
                gameManager.PassiveBootImage.sprite = gameManager.BootPowerupPrefabArray[BootLevel].GetComponent<SpriteRenderer>().sprite;
                NumDollars -= gameManager.BootCosts[BootLevel];
                BootLevel += 1;
                MoveSpeed = baseMoveSpeed + (BootLevel * passiveMoveSpeedBoost);
                gameManager.DestroyMerchantAndMatAndPassivePowerups();
                audioManager.Play(AudioManager.WarhoundSounds.powerupcollect2);
            }
            else if (other.tag == "Powerup_Gun" && numberDollars >= gameManager.GunCosts[GunLevel])
            {
                gameManager.PassiveGunImage.enabled = true;
                gameManager.PassiveGunImage.sprite = gameManager.GunPowerupPrefabArray[GunLevel].GetComponent<SpriteRenderer>().sprite;
                NumDollars -= gameManager.GunCosts[GunLevel];
                GunLevel += 1;
                gameManager.DestroyMerchantAndMatAndPassivePowerups();
                audioManager.Play(AudioManager.WarhoundSounds.powerupcollect2);
            }
            else if (other.tag == "Powerup_Ammo" && numberDollars >= gameManager.AmmoCosts[AmmoLevel])
            {
                gameManager.PassiveAmmoImage.enabled = true;
                gameManager.PassiveAmmoImage.sprite = gameManager.AmmoPowerupPrefabArray[AmmoLevel].GetComponent<SpriteRenderer>().sprite;
                NumDollars -= gameManager.AmmoCosts[AmmoLevel];
                AmmoLevel += 1;
                RecalculateShootingRate();
                gameManager.DestroyMerchantAndMatAndPassivePowerups();
                audioManager.Play(AudioManager.WarhoundSounds.powerupcollect2);
            }
            else if (other.tag == "Powerup_Grenade")
            {
                if (StoredPowerup == StoredPowerups.NONE)
                {
                    StoredPowerup = StoredPowerups.GRENADE;
                    gameManager.SetStoredPowerupIcon(StoredPowerups.GRENADE);
                }
                else
                {
                    ConsumePowerup(StoredPowerups.GRENADE, false);
                }

                other.gameObject.SetActive(false);
                audioManager.Play(AudioManager.WarhoundSounds.powerupcollect);
            }
            else if (other.tag == "Powerup_Medal")
            {
                StoredPowerup = StoredPowerups.MEDAL;
                gameManager.SetStoredPowerupIcon(StoredPowerups.MEDAL);
                other.gameObject.SetActive(false);
                audioManager.Play(AudioManager.WarhoundSounds.powerupcollect);
            }
            else if (other.tag == "Enemy")
            {
                if (HasActiveState(StateType.BERSERK))
                {
                    // Don't destroy the final boss                    
                    if (!other.GetComponent<Brain>()) 
                    {
                        other.GetComponent<Enemy>().Destroy();
                    }
                }
                else if (!FreezeMotion)
                {
                    HandlePlayerHit();
                }
            }
        }
    }
}