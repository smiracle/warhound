﻿using Assets.Game.Scripts.Map;
using UnityEngine;

namespace Assets.Game.Scripts.PlayerScripts
{
    public class PlayerKeyboardControl : MonoBehaviour
    {
        private Player player;
        private Animator playerAnimator;
        private GameManager gameManager;
        private Vector3 leftJoystickDirection;
        private Vector3 rightJoystickDirection;
        private Vector3 movementDirection;
        private Vector3 facingDirection;
        private Rigidbody2D body;
        private Vector2 receivedDirection;

        public void FaceDown()
        {
            facingDirection = new Vector2(0f, -1f);
        }

        public void SetReceivedDirection(Vector2 direction)
        {
            if (direction == Vector2.zero || player.FreezeMotion)
            {
                return;
            }

            receivedDirection = direction;
        }

        public Vector3 GetFacingDirection()
        {
            return facingDirection;
        }

        public bool IsMoving()
        {
            return movementDirection != Vector3.zero;
        }

        public void CanvasUseButtonPress()
        {
            GameObject.FindObjectOfType<Player>().ConsumePowerup(GameObject.FindObjectOfType<Player>().StoredPowerup, true);
        }

        private void ResetReceivedDirection()
        {
            receivedDirection = Vector2.zero;
        }

        private void UpdateDirection()
        {
            movementDirection = new Vector3(receivedDirection.x, receivedDirection.y, 0);

            if (receivedDirection != Vector2.zero)
            {
                Vector3 facingDirection = movementDirection;
                if (facingDirection.x != 0 && facingDirection.y != 0)
                {
                    if (this.facingDirection.x == facingDirection.x)
                    {
                        facingDirection.y = 0;
                    }
                    else if (this.facingDirection.y == facingDirection.y)
                    {
                        facingDirection.x = 0;
                    }
                    else
                    {
                        facingDirection.x = 0;
                    }
                }

                this.facingDirection = facingDirection;

                receivedDirection = Vector2.zero;
            }
        }

        private void Start()
        {
            body = GetComponent<Rigidbody2D>();
            FaceDown();
            gameManager = GameObject.FindObjectOfType<GameManager>();
            player = GetComponent<Player>();
            playerAnimator = GetComponent<Animator>();
        }

        private void Update()
        {
            rightJoystickDirection = gameManager.GetRightJoystick().InputDirection;
            leftJoystickDirection = gameManager.GetLeftJoystick().InputDirection;

            UpdateMoveDirection();
            UpdateConsumePowerup();

            if (gameManager.GetPlayerComponent().FreezeMotion)
            {
                return;
            }

            if (rightJoystickDirection == Vector3.zero && (!Input.GetKey(KeyCode.UpArrow) && !Input.GetKey(KeyCode.RightArrow) && !Input.GetKey(KeyCode.DownArrow) && !Input.GetKey(KeyCode.LeftArrow)))
            {
                playerAnimator.SetBool("IsAttacking", false);
            }
            else
            {
                playerAnimator.SetBool("IsAttacking", true);
            }

            if (Time.time > player.ShootingRate + player.LastShot)
            {
                if (rightJoystickDirection == Vector3.zero)
                {
                    UpdateKeyboardShooting();
                }
                else
                {
                    UpdateRightJoystickShoot();
                }
            }

            UpdateDirection();
            ResetReceivedDirection();
        }

        private void FixedUpdate()
        {
            if (player.FreezeMotion)
            {
                body.velocity = Vector3.zero;
                return;
            }

            UpdateMovement();
        }

        private void UpdateMovement()
        {
            if (movementDirection != Vector3.zero)
            {
                movementDirection.Normalize();
            }

            // Move the character
            body.velocity = movementDirection * player.MoveSpeed * 60;
        }

        private void ShootCircle()
        {
            player.ShootRight();
            player.ShootUp();
            player.ShootLeft();
            player.ShootDown();
            player.ShootUpRight();
            player.ShootUpLeft();
            player.ShootDownLeft();
            player.ShootDownRight();
        }

        private void UpdateRightJoystickShoot()
        {
            float angle = rightJoystickDirection.x < 0 ? (180 - Vector3.Angle(rightJoystickDirection, Vector3.up) + 180) : Vector3.Angle(rightJoystickDirection, Vector3.up);
            bool circleShotActive = player.HasActiveState(StateType.CIRCLE_SHOT);
            bool spreadShotActive = player.HasActiveState(StateType.SPREAD_SHOT);

            if (circleShotActive)
            {
                ShootCircle();
                return;
            }
            
            if (angle > 337.5 || angle <= 22.5)
            {
                // Up
                if (spreadShotActive)
                {
                    player.ShotgunShootUp();
                }
                else
                {
                    player.ShootUp();
                }
            }
            else if (angle > 22.5 && angle <= 67.5) 
            {
                // UpRight
                if (spreadShotActive)
                {
                    player.ShotgunShootUpRight();
                }
                else
                {
                    player.ShootUpRight();
                }
            }
            else if (angle > 67.5 && angle <= 112.5)
            {
                // Right
                if (spreadShotActive)
                {
                    player.ShotgunShootRight();
                }
                else
                {
                    player.ShootRight();
                }
            }
            else if (angle > 112.5 && angle <= 157.5)
            {
                // DownRight
                if (spreadShotActive)
                {
                    player.ShotgunShootDownRight();
                }
                else
                {
                    player.ShootDownRight();
                }
            }
            else if (angle > 157.5 && angle <= 202.5)
            {
                // Down
                if (spreadShotActive)
                {
                    player.ShotgunShootDown();
                }
                else
                {
                    player.ShootDown();
                }
            }
            else if (angle > 202.5 && angle <= 247.5)
            {
                // DownLeft
                if (spreadShotActive)
                {
                    player.ShotgunShootDownLeft();
                }
                else
                {
                    player.ShootDownLeft();
                }
            }
            else if (angle > 247.5 && angle <= 292.5)
            {
                // Left
                if (spreadShotActive)
                {
                    player.ShotgunShootLeft();
                }
                else
                {
                    player.ShootLeft();
                }
            }
            else if (angle > 292.5 && angle <= 337.5)
            {
                // UpLeft
                if (spreadShotActive)
                {
                    player.ShotgunShootUpLeft();
                }
                else
                {
                    player.ShootUpLeft();
                }
            }
        }        

        private void UpdateConsumePowerup()
        {
            if (Input.GetKeyDown(KeyCode.E) || Input.GetKeyDown(KeyCode.Space))
            {
                player.ConsumePowerup(player.StoredPowerup, true);
            }
        }

        private void UpdateKeyboardShooting()
        {
            bool circleShotActive = player.HasActiveState(StateType.CIRCLE_SHOT);
            bool spreadShotActive = player.HasActiveState(StateType.SPREAD_SHOT);

            if (spreadShotActive && circleShotActive)
            {
                UpdateCircleShotgunShooting();
            }
            else if (spreadShotActive)
            {
                UpdateShotgunShooting();
            }
            else if (circleShotActive)
            {
                UpdateKeyboardCircleShooting();
            }
            else
            {
                UpdateRegularShooting();
            }
        }

        private void UpdateKeyboardCircleShooting()
        {
            if (Input.GetKey(KeyCode.UpArrow) || Input.GetKey(KeyCode.RightArrow) || Input.GetKey(KeyCode.DownArrow) || Input.GetKey(KeyCode.LeftArrow))
            {
                player.ShootCircle();
            }
        }

        private void UpdateCircleShotgunShooting()
        {
            if (Input.GetKey(KeyCode.UpArrow) || Input.GetKey(KeyCode.RightArrow) || Input.GetKey(KeyCode.DownArrow) || Input.GetKey(KeyCode.LeftArrow))
            {
                player.ShootShotgunCircle();
            }
            else
            {
                playerAnimator.SetBool("IsAttacking", false);
            }
        }

        private void UpdateShotgunShooting()
        {
            if (Input.GetKey(KeyCode.UpArrow))
            {
                if (Input.GetKey(KeyCode.RightArrow))
                {
                    player.ShotgunShootUpRight();
                }
                else if (Input.GetKey(KeyCode.LeftArrow))
                {
                    player.ShotgunShootUpLeft();
                }
                else
                {
                    player.ShotgunShootUp();
                }
            }
            else if (Input.GetKey(KeyCode.DownArrow))
            {
                if (Input.GetKey(KeyCode.RightArrow))
                {
                    player.ShotgunShootDownRight();
                }
                else if (Input.GetKey(KeyCode.LeftArrow))
                {
                    player.ShotgunShootDownLeft();
                }
                else
                {
                    player.ShotgunShootDown();
                }
            }
            else if (Input.GetKey(KeyCode.LeftArrow))
            {
                player.ShotgunShootLeft();
            }
            else if (Input.GetKey(KeyCode.RightArrow))
            {
                player.ShotgunShootRight();
            }
            else
            {
                playerAnimator.SetBool("IsAttacking", false);
            }
        }

        private void UpdateRegularShooting()
        {
            if (Input.GetKey(KeyCode.UpArrow))
            {
                if (Input.GetKey(KeyCode.RightArrow))
                {
                    player.ShootUpRight();
                }
                else if (Input.GetKey(KeyCode.LeftArrow))
                {
                    player.ShootUpLeft();
                }
                else
                {
                    player.ShootUp();
                }
            }
            else if (Input.GetKey(KeyCode.DownArrow))
            {
                if (Input.GetKey(KeyCode.RightArrow))
                {
                    player.ShootDownRight();
                }
                else if (Input.GetKey(KeyCode.LeftArrow))
                {
                    player.ShootDownLeft();
                }
                else
                {
                    player.ShootDown();
                }
            }
            else if (Input.GetKey(KeyCode.LeftArrow))
            {
                player.ShootLeft();
            }
            else if (Input.GetKey(KeyCode.RightArrow))
            {
                player.ShootRight();
            }
            else
            {
                playerAnimator.SetBool("IsAttacking", false);
            }
        }

        private void UpdateMoveDirection()
        {
            if (leftJoystickDirection == Vector3.zero)
            {
                Vector2 newDirection = Vector2.zero;
                if (Input.GetKey(KeyCode.W))
                {
                    newDirection.y = 1;
                }

                if (Input.GetKey(KeyCode.S))
                {
                    newDirection.y = -1;
                }

                if (Input.GetKey(KeyCode.A))
                {
                    newDirection.x = -1;
                }

                if (Input.GetKey(KeyCode.D))
                {
                    newDirection.x = 1;
                }

                SetReceivedDirection(newDirection);
            }
            else
            {
                SetReceivedDirection(leftJoystickDirection);
            }
        }
    }
}