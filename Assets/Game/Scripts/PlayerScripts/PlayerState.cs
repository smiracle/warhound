﻿using System;

namespace Assets.Game.Scripts.PlayerScripts
{
    public enum StateType
    {
        NORMAL,
        SPREAD_SHOT,
        CIRCLE_SHOT,
        MACHINE_GUN,
        BERSERK
    }

    public class PlayerState
    {
        private StateType stateType;
        private float startTime;
        private float stateDuration;

        public PlayerState(StateType stateType, float startTime)
        {            
            this.stateType = stateType;
            this.startTime = startTime;
            switch (stateType)
            {
                case StateType.MACHINE_GUN:
                case StateType.SPREAD_SHOT:
                case StateType.CIRCLE_SHOT:                
                    stateDuration = 15f; // SET THE DURATION OF EACH STATE
                    break;
                case StateType.BERSERK:
                    stateDuration = 8f;
                    break;
            }
        }

        public StateType GetStateType()
        {
            return stateType;
        }

        public bool HasStateDurationExpired(float currentTime)
        {
            return Math.Abs(startTime - currentTime) > stateDuration;
        }
    }
}