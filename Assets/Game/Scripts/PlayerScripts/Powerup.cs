﻿namespace Assets.Game.Scripts.PlayerScripts
{
    public enum StoredPowerups
    {
         NONE,
         MACHINE_GUN,
         SHOTGUN,
         CIRCLE_SHOT,
         GRENADE,
         MEDAL,
         DOLLAR
    }
}
