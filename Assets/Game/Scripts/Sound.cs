﻿using UnityEngine;

[System.Serializable]
public class Sound
{
    [HideInInspector]
    public string Name;
    public AudioClip Clip;
    [Range(0f, 2f)]
    public float Volume = 1f;
    [Range(.1f, 3f)]
    public float Pitch = 1f;
    public bool Loop;
    [HideInInspector]
    public AudioSource Source;
}