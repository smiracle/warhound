# WARHOUND README #

### What is this repository for? ###
This is a mobile game that I created using Unity on 12/4/2017. It is a top-down 2D shooter designed for modern android devices.

Version 1.0

### How do I get set up? ###

Download the repo and open the scene file in Unity, then generate an android apk or run it in Windows.

### Who do I talk to? ###

This repo is owned by Steven Miracle. My email address is stvnmiracle@gmail.com.